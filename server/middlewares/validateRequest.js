var jwt = require('jwt-simple');
var validateUser = require('../routes/auth').validateUser;


var pg = require('pg');
var connectionString = require('../config/database');

var async = require('async');


module.exports = function(req, res, next) {

  // When performing a cross domain request, you will recieve
  // a preflighted request first. This is to check if our the app
  // is safe. 

  // We skip the token outh for [OPTIONS] requests.
  //if(req.method == 'OPTIONS') next();

  var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
  //var key = (req.body && req.body.x_key) || (req.query && req.query.x_key) || req.headers['x-key'];

  if (token || (req.url.indexOf('public') >= 0)) {// || key

    if(token){
      var dbUser;

      var decoded = jwt.decode(token, require('../config/secret.js')());
    }

    async.series([
        //Load user to get `userId` first
        function(callback) {
            //dbUser = validateUser(key); // The key would be the logged in user's username

            if(token){
              var client = new pg.Client(connectionString);
              
              key = decoded.username;
              //console.log("email is: "+key);
              client.connect();
              client.query("SELECT * from users where email=$1", [key], function(err, result){
                  //console.log(result.rows[0]);
                  if(err){
                      return callback(err, null);
                  }
                  
                  //if no rows were returned from query, then new user
                  //console.log(result.rows);
                  if (result.rows.length > 0){
                      //console.log(result.rows[0] + ' is found!');
                     
                     dbUser = { // spoofing a userobject from the DB. 
                            name: result.rows[0]['email'],
                            role: result.rows[0]['role'],
                            username: result.rows[0]['email'],
                          };
                          //console.log(dbUser);
                          
                      
                  }
                  client.end();
                  callback();
              });
            }else{
              callback();
            }

        },
        //Load posts (won't be called before task 1's "task callback" has been called)
        function(callback) {
          if(token){
            //console.log(dbUser);
             try {
              //var decoded = jwt.decode(token, require('../config/secret.js')());

              //console.log(decoded.username);
              if (decoded.exp <= Date.now()) {
                res.status(400);
                res.json({
                  "status": 400,
                  "message": "Token Expired"
                });
                return;
              }

              /*if (decoded.username != key) {
                res.status(401);
                res.json({
                  "status": 401,
                  "message": "Invalid Token or Key"
                });
                return;
              }*/

              // Authorize the user to see if s/he can access our resources

              
              console.log("--validate--");
              console.log(dbUser);
              //dbUser = validateUser(decoded.username);
              //console.log(validateUser(decoded.username));
              console.log("--validate--");

              if (dbUser) {
                if ((req.url.indexOf('admin') >= 0 && dbUser.role == 'admin') || (req.url.indexOf('admin') < 0 && req.url.indexOf('/api/v1/') >= 0)) {
                  next(); // To move to next middleware
                } else {
                  res.status(403);
                  res.json({
                    "status": 403,
                    "message": "Not Authorized"
                  });
                  return;
                }
              } else {
                // No user with this name exists, respond back with a 401
                res.status(401);
                res.json({
                  "status": 401,
                  "message": "Invalid User"
                });
                return;
              }

            } catch (err) {
              res.status(500);
              res.json({
                "status": 500,
                "message": "Oops something went wrong",
                "error": err
              });
            }
          }else{
            next();
          }
        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        res.render('user-profile', locals);
    });


    
  } else {
    res.status(401);
    res.json({
      "status": 401,
      "message": "Invalid Token or Key"
    });
    return;
  }
};
