// config/database.js

var connectionString = process.env.DATABASE_URL || 'postgres://postgres:toor@localhost:5432/monica'

module.exports = connectionString;