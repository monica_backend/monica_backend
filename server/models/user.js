// load the things we need

var pg = require('pg');
var connectionString = require('../config/database');
var bcrypt   = require('bcrypt-nodejs');

var jwt = require('jwt-simple');


var async = require('async');


function User(){
    this.u_id = "";
    //this.name ='';
    //this.photo ='';
    this.email = "";
    this.password= ""; //need to declare the things that i want to be remembered for each user in the database
    this.createdate="";
    this.updatedate="";
    this.status="";
    this.role="";

    
    this.fname="";
    this.lname="";
    this.photo="";

    

    
    this.save = function(callback) {
        //console.log('user:');
        //console.log(this.email);
        myuser = this;
        uid = '';
        async.series([
            function(done){ 
                var client = new pg.Client(connectionString);
                client.connect();
                //console.log(myuser);
                //console.log(this.email +' will be saved');
                client.query('INSERT INTO users(email, password, create_date, update_date, status, role) VALUES($1, $2, $3, $4, $5, $6) RETURNING u_id', [myuser.email, myuser.password, myuser.createdate, myuser.updatedate, myuser.status, myuser.role], function (err, result) {
                    if(err){
                        console.log(err);
                        return console.error('error running query', err);
                    }
                    //console.log(result.rows);
                    //console.log(result.rows[0].u_id);

                    uid = result.rows[0].u_id;
                    client.end();
                    done();
                    //console.log(this.email);
                });
            },
            function(done){
                var client = new pg.Client(connectionString);
                client.connect();
                client.query('INSERT INTO profile(profile_user, profile_fname, profile_lname, profile_photo) VALUES($1, $2, $3, $4)', [uid, myuser.fname, myuser.lname, myuser.photo], function (err, result) {
                    if(err){
                        console.log(err);
                        return console.error('error running query', err);
                    }
                    //console.log(result.rows);
                    client.end();
                    return callback(uid);
                    //done();

                    //console.log(this.email);
                });

                /*client.query('SELECT * FROM users ORDER BY u_id desc limit 1', null, function(err, result){
                    if(err){
                        return callback(null);
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows[0] + ' is found!');
                        var user = new User();
                        user.email= result.rows[0]['email'];
                        user.password = result.rows[0]['password'];
                        user.u_id = result.rows[0]['u_id'];
                        //console.log(user.email);
                        client.end();
                        return callback(user);
                    }
                });*/

            }
        ]);


        

        
        //whenever we call 'save function' to object USER we call the insert query which will save it into the database.
    //});
    };


    // generating a hash
    this.generateHash = function(password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    };

    // checking if password is valid
    this.validPassword = function(password) {
        //console.log(password, this.password);
        return bcrypt.compareSync(password, this.password);
    };

    this.getFromToken = function(token){
        var decoded = jwt.decode(token, require('../config/secret.js')());
        return decoded.username;
    };


}

User.findOne = function(email, callback){
    //console.log("we are in findbyemail");
    var client = new pg.Client(connectionString);
    
    
    //console.log("email is: "+email);
    client.connect();
    client.query("SELECT * from users where email=$1", [email], function(err, result){
        
        if(err){
            return callback(err, null);
        }

        //if no rows were returned from query, then new user
        //console.log(result.rows);
        if (result.rows.length > 0){
            //console.log(result.rows[0] + ' is found!');
            var user = new User();
            user.email= result.rows[0]['email'];
            user.password = result.rows[0]['password'];
            user.u_id = result.rows[0]['u_id'];
            //console.log(user);
            client.end();
            return callback(null, user);
        }else{
            client.end();
            return callback(null, null);
        }
    });
};

User.signUp = function(email, callback){

    var client = new pg.Client(connectionString);

    var isNotAvailable = false; //we are assuming the email is taking
    //var email = this.email;
    //var rowresult = false;
    //console.log(email + ' is in the findOne function test');
    //check if there is a user available for this email;
    client.connect();

    client.query("SELECT * from users where email=$1", [email], function(err, result){
        if(err){
            return callback(err, isNotAvailable, this);
        }
        //if no rows were returned from query, then new user
        if (result.rows.length > 0){
            isNotAvailable = true; // update the user for return in callback
            ///email = email;
            //password = result.rows[0].password;
            //console.log(email + ' is am not available!');
        }
        else{
            isNotAvailable = false;
            //email = email;
            //console.log(email + ' is available');
        }
        //the callback has 3 parameters:
        // parameter err: false if there is no error
        //parameter isNotAvailable: whether the email is available or not
        // parameter this: the User object;

        client.end();
        return callback(false, isNotAvailable, this);
    });

};


/*User.login = function(email, callback){
    console.log("we are in findbyemail");
    var client = new pg.Client(connectionString);
    
    client.connect();
    client.query("SELECT * from users where email=$1", [email], function(err, result){

        if(err){
            return callback(err, null);
        }

        //if no rows were returned from query, then new user
        if (result.rows.length > 0){
            console.log(result.rows[0] + ' is found!');
            var user = new User();
            user.email= result.rows[0]['email'];
            user.password = result.rows[0]['password'];
            user.u_id = result.rows[0]['u_id'];
            console.log(user.email);
            return callback(null, user);
        }
    });
};*/


/*User.logIn = function(email, callback){

    var client = new pg.Client(connectionString);

    var isNotAvailable = false; //we are assuming the email is taking
    //var email = this.email;
    //var rowresult = false;
    console.log(email + ' is in the findOne function test');
    //check if there is a user available for this email;
    client.connect();

    client.query("SELECT * from users where email=$1", [email], function(err, result){
        if(err){
            return callback(err, isNotAvailable, this);
        }
        //if no rows were returned from query, then new user
        if (result.rows.length > 0){
            isNotAvailable = true; // update the user for return in callback
            ///email = email;
            //password = result.rows[0].password;
            console.log(email + ' is am not available!');
        }
        else{
            isNotAvailable = false;
            //email = email;
            console.log(email + ' is available');
        }
        //the callback has 3 parameters:
        // parameter err: false if there is no error
        //parameter isNotAvailable: whether the email is available or not
        // parameter this: the User object;

        client.end();
        return callback(false, isNotAvailable, this);
    });

};


User.logInTwo = function(email, callback) {

    var client = new pg.Client(connectionString);
    //check if there is a user available for this email;
    client.connect();

    client.query("SELECT * from users where email=$1", [email], function(err, result){
        callback(result)
    });
};


User.findByEmail = function(email, callback){
    console.log("we are in findbyemail");
    var client = new pg.Client(connectionString);
    
    client.connect();
    client.query("SELECT * from users where email=$1", [email], function(err, result){

        if(err){
            return callback(err, null);
        }

        //if no rows were returned from query, then new user
        if (result.rows.length > 0){
            console.log(result.rows[0] + ' is found!');
            var user = new User();
            user.email= result.rows[0]['email'];
            user.password = result.rows[0]['password'];
            user.u_id = result.rows[0]['u_id'];
            console.log(user.email);
            return callback(null, user);
        }
    });
};*/




// create the model for users and expose it to our app
//module.exports = mongoose.model('User', userSchema);
module.exports = User;
