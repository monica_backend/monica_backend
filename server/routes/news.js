



var pg = require('pg');
var connectionString = require('../config/database');

var async = require('async');

var User = require('../models/user');
var Notification = require('../models/notification');


var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];



var news = {


  getAll: function(req, res) {

    var allNews = new Array;

    var id = req.params.id;
    console.log(id);

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);

    User.findOne( username , function(err, user) {
        if(user){
          
          async.series([
                  function(callback) {
          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM news WHERE news_event=$1 AND news_user=$2 ORDER BY news_date DESC", [id, user.u_id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(reslen);
                        for(i=0; i<reslen; i++){

                          var newsdate = new Date(result.rows[i].news_date);

                          allNews.push({
                                        'news_id':result.rows[i].news_id, 
                                        'news_text':result.rows[i].news_text, 
                                        'news_icon':result.rows[i].news_icon, 
                                        'news_color':result.rows[i].news_color, 
                                        'news_date': addZero(newsdate.getHours()) + ':' + addZero(newsdate.getMinutes()) + ' - ' + days[newsdate.getDay()] + ', ' + months[newsdate.getMonth()] + ' ' + newsdate.getDate() + ',' +  newsdate.getFullYear(),
                                        'news_details':result.rows[i].news_details 
                                      });
                          //console.log(result.rows[i].topic_branch);
                        }

                        client.end();
                        callback();
                    }else{
                      res.json({
                        "status": 200,
                        "message": "No news were found.",
                        "data" : []
                      });
                    }
                    
                });

        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(allEvents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : allNews
                  });
          


        }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
        }
        
    });
    
  },


  addNews: function(req, res) {
/*
    var pushtoken = req.body.pushtoken || '';

    var tokenfound = 0;
    var status = 0;


    if (pushtoken == ''  ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Push Token is missing",
        "data" : []
      });
      return;
    }
*/
    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);

    User.findOne( username , function(err, user) {
        if(user){
          
            
            
            notification = new Notification();
            notification.newsevent = "12";
            notification.newstext = "test";
            notification.newsicon = "http://image.flaticon.com/icons/png/512/126/126501.png";
            notification.newsuser="123";
            notification.newsdetails="{}";
            notification.save();
            
            
              res.status(200);
              res.json({
                      "status": 200,
                      "message": "",
                      "data" : 1
                      });    
        }
        
    });
    
  },
  
  /* getAll: function(req, res) {
    
    var allNews = new Array;

    var id = req.params.id;
    console.log(id);

    async.series([
        //Load user to get `userId` first
        function(callback) {
          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM news WHERE news_event=$1 ORDER BY news_date DESC", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(reslen);
                        for(i=0; i<reslen; i++){

                          var newsdate = new Date(result.rows[i].news_date);

                          allNews.push({
                                        'news_id':result.rows[i].news_id, 
                                        'news_text':result.rows[i].news_text, 
                                        'news_icon':result.rows[i].news_icon, 
                                        'news_color':result.rows[i].news_color, 
                                        'news_date': addZero(newsdate.getHours()) + ':' + addZero(newsdate.getMinutes()) + ' - ' + days[newsdate.getDay()] + ', ' + months[newsdate.getMonth()] + ' ' + newsdate.getDate() + ',' +  newsdate.getFullYear()
                                      });
                          //console.log(result.rows[i].topic_branch);
                        }

                        client.end();
                        callback();
                    }else{
                      res.json({
                        "status": 200,
                        "message": "No news were found.",
                        "data" : []
                      });
                    }
                    
                });

        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(allEvents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : allNews
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },*/

  

  update: function(req, res) {
    var updateProduct = req.body;
    var id = req.params.id;
    data[id] = updateProduct // Spoof a DB call
    res.json(updateProduct);
  },

  delete: function(req, res) {
    var id = req.params.id;
    data.splice(id, 1) // Spoof a DB call
    res.json(true);
  }
};




function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

var data = [{
  name: 'event 1',
  id: '1'
}, {
  name: 'event 2',
  id: '2'
}, {
  name: 'event 3',
  id: '3'
}];






module.exports = news;
