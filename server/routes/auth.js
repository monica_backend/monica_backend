var jwt = require('jwt-simple');

var async = require('async');

var pg = require('pg');
var connectionString = require('../config/database');

var request = require('request');

var User = require('../models/user');
var Linkedin = require('../models/linkedin');

var auth = {

  login: function(req, res) {

    var username = req.body.username || '';
    var password = req.body.password || '';

    if (username == '' || password == '') {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Invalid credentials",
        "data" : []
      });
      return;
    }

    // Fire a query to your DB and check if the credentials are valid
    //var dbUserObj = auth.validate(username, password);
    var dbUserObj = false; 
  	async.series([
  	    function(done){ 

  	    	var client = new pg.Client(connectionString);
  		    console.log(username + ' is in the findOne function test');
  		    //check if there is a user available for this email;
  		    client.connect();

  		    client.query("SELECT * from users where email=$1", [username], function(err, result){

  		        User.findOne( username , function(err, user) {
  		        	console.log("password: "+password);
  	                //console.log(user.validPassword(password));
                    if(user){
                        if(user.validPassword(password)){
                            name = '';
                            photo = '';
                            //role = user.role;
                            //console.log(user.role);
                            async.series([
                              function(finish){
                                var client = new pg.Client(connectionString);
                                //check if there is a user available for this email;
                                //console.log(user.u_id);
                                
                                client.connect();
                                client.query("SELECT * from profile where profile_user=$1", [user.u_id], function(err, profresult){
                                  //console.log(profresult.rows[0].profile_fname);
                                  if(err){
                                    return;
                                  }
                                  if(profresult.rows.length > 0){
                                      name = profresult.rows[0].profile_fname + ' ' + profresult.rows[0].profile_lname;
                                      photo = profresult.rows[0].profile_photo;
                                  }
                                  
                                  client.end();
                                  finish();
                                });
                                
                              },
                              function(finish){
                                dbUserObj = { // spoofing a userobject from the DB. 
                                  username: username,
                                  name: name,
                                  photo: photo
                                  //role: role,
                                };
                                done(null);
                              },
                            ]);

                            

                            
                        }else{
                              res.status(401);
                              res.json({
                              "status": 401,
                              "message": "Invalid credentials",
                              "data" : []
                              });
                              done(null);
                        }
                    }
  	            });

  		        client.end();
  		        
  		    });
    	},
	    
		function(done){
			if (dbUserObj) {
		      // If authentication is success, we will generate a token
		      // and dispatch it to the client
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "Login successful",
                  "data" : genToken(dbUserObj)
                  });
		      //res.json(genToken(dbUserObj));
		    }
		}
	]);
    

  },

  loginlinkedin: function(req, res) {
    var token = req.body.token;

    var options = {
      url: 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,formatted-name,date-of-birth,industry,email-address,location,headline,summary,picture-urls::(original))?format=json&oauth2_access_token='+token,
      headers: {
        'User-Agent': 'request'
      }
    };
     
    function callback(error, response, body) {
      if (!error && response.statusCode == 200) {
        var info = JSON.parse(body);
        /*console.log(info.id);
        console.log(info.firstName);
        console.log(info.lastName);
        console.log(info.headline);
        console.log(info.industry);
        console.log(info.location.country.code);
        console.log(info.summary);
        console.log(info.pictureUrls.values[0]);*/
        //console.log("--info--");
        //console.log(info);
        //console.log("--info--");
        
        //var email = 'bg.11@hotmail.com';

        Linkedin.findOne( info.id , function(err, linkedin) {
            //console.log(user.validPassword(password));
            if(linkedin){
                //console.log(linkedin);
                console.log("linkedin is found");
                User.findOne( info.emailAddress , function(err, user) {
                      if(user){
                        name = '';
                        photo = '';
                        async.series([
                              function(finish){
                                var client = new pg.Client(connectionString);
                                //check if there is a user available for this email;
                                
                                client.connect();
                                client.query("SELECT * from profile where profile_user=$1", [user.u_id], function(err, profresult){
                                  //console.log(profresult.rows[0].profile_fname);
                                  if(err){
                                    return;
                                  }
                                  if(profresult.rows.length > 0){
                                      name = profresult.rows[0].profile_fname + ' ' + profresult.rows[0].profile_lname;
                                      photo = profresult.rows[0].profile_photo;
                                  }
                                  
                                  client.end();
                                  finish();
                                });
                                
                              },
                              function(finish){
                                dbUserObj = { // spoofing a userobject from the DB. 
                                      username: info.emailAddress,
                                      name: name,
                                      photo: photo
                                      //role: 'user',
                                      
                                    };

                                res.status(200);
                                res.json({
                                    "status": 200,
                                    "message": "Login successful",
                                    "data" : genToken(dbUserObj)
                                    });
                              },
                            ]);

                      }
                      
                  });
            }else{
                console.log("linkedin not found");
                //check if email is found
                User.findOne( info.emailAddress , function(err, user) {
                    if(user){
                          //console.log("email: "+info.emailAddress);
                          console.log("email exists in users table.");
                          console.log(user.u_id);

                          linkedin = new Linkedin();
                          linkedin.user_id = user.u_id;
                          linkedin.linkedin_id = info.id;
                          linkedin.linkedin_fname= info.firstName;
                          linkedin.linkedin_lname=info.lastName;
                          linkedin.linkedin_email=info.emailAddress;
                          linkedin.linkedin_headline=info.headline;
                          linkedin.linkedin_industry=info.industry;
                          linkedin.linkedin_photo=info.pictureUrls.values[0];
                          linkedin.linkedin_summary=info.summary;
                          
                          linkedin.save(function(newLinkedin) {
                              //console.log("userid: "+user.u_id);
                              //console.log("saved linkedin: ", linkedin);
                              
                              name = '';
                              photo = '';
                              async.series([
                                    function(finish){
                                      var client = new pg.Client(connectionString);
                                      //check if there is a user available for this email;
                                      
                                      client.connect();
                                      client.query("SELECT * from profile where profile_user=$1", [user.u_id], function(err, profresult){
                                        //console.log(profresult.rows[0].profile_fname);
                                        if(err){
                                          return;
                                        }
                                        if(profresult.rows.length > 0){
                                            name = profresult.rows[0].profile_fname + ' ' + profresult.rows[0].profile_lname;
                                            photo = profresult.rows[0].profile_photo;
                                        }

                                        client.end();
                                        finish();
                                      });
                                      
                                    },
                                    function(finish){
                                      dbUserObj = { // spoofing a userobject from the DB. 
                                            username: info.emailAddress,
                                            name: name,
                                            photo: photo
                                            //role: 'user',
                                            
                                          };
                                          
                                      res.status(200);
                                      res.json({
                                          "status": 200,
                                          "message": "Login successful",
                                          "data" : genToken(dbUserObj)
                                          });
                                    },
                                  ]);

                          });

                          //lnkdin.signUp(mylnkdin);
                    }else if(user == null && err == null){
                        console.log("email does not exist in users table.");

                        password = Math.random().toString(36).slice(2);
                        //console.log("password: "+password);

                        user = new User();
                        user.email    = info.emailAddress;
                        user.password = user.generateHash(password);
                        
                        user.createdate = new Date().toISOString();
                        user.updatedate = new Date().toISOString();
                        user.status = 1;
                        user.role = 'user';
                        user.fname = info.firstName;
                        user.lname = info.lastName;
                        user.photo = info.pictureUrls.values[0];


                        myuserid = '';

                        async.series([
                                function(done){
                                  user.save(function(newUserId) {
                                    //console.log("user save: ");
                                    //console.log(newUserId);

                                    //lnkdin.signUp(mylnkdin);
                                    myuserid = newUserId;

                                    linkedin = new Linkedin();
                                    linkedin.user_id = newUserId;
                                    linkedin.linkedin_id = info.id;
                                    linkedin.linkedin_fname= info.firstName;
                                    linkedin.linkedin_lname=info.lastName;
                                    linkedin.linkedin_email=info.emailAddress;
                                    linkedin.linkedin_headline=info.headline;
                                    linkedin.linkedin_industry=info.industry;
                                    linkedin.linkedin_photo=info.pictureUrls.values[0];
                                    linkedin.linkedin_summary=info.summary;
                                    
                                    done();
                                    });
                                },
                                function(done){
                                      
                                      //console.log("userid: "+user.u_id);
                                      //console.log("saved linkedin: ", linkedin);
                                      
                                      name = '';
                                      photo = '';
                                      async.series([
                                            function(finish){
                                                //console.log("1st");
                                                linkedin.save(finish);
                                            },
                                            function(finish){
                                                //console.log("2nd");
                                                var client = new pg.Client(connectionString);
                                                //check if there is a user available for this email;
                                                
                                                //console.log("user id: "+myuserid);
                                                client.connect();
                                                client.query("SELECT * from profile where profile_user=$1", [myuserid], function(err, result){
                                                  //console.log(profresult.rows[0].profile_fname);
                                                  //console.log("3rd");
                                                  //console.log(result.rows);
                                                  //console.log(err);
                                                  if(err){
                                                    return;
                                                  }
                                                  if(result.rows.length > 0){
                                                      name = result.rows[0].profile_fname + ' ' + result.rows[0].profile_lname;
                                                      photo = result.rows[0].profile_photo;
                                                  }
                                                  client.end();
                                                  finish();
                                                });
                                            },
                                            function(finish){
                                              dbUserObj = { // spoofing a userobject from the DB. 
                                                    username: info.emailAddress,
                                                    name: name,
                                                    photo: photo
                                                    //role: 'user',
                                                  };
                                              res.status(200);
                                              res.json({
                                                  "status": 200,
                                                  "message": "Login successful",
                                                  "data" : genToken(dbUserObj)
                                                  });
                                            },
                                          ]);


                                  
                                },
                              ]);

                            //console.log(new Date().toISOString());
                            //console.log(username, password, user);

                    }

                });



                

            }
        });


      }
    }
     
    request(options, callback);
  },


   signup: function(req, res) {

    var username = req.body.username || '';
    var password = req.body.password || '';
    var fname = req.body.fname || '';
    var lname = req.body.lname || '';
    var photo = req.body.photo || '';

    var password = req.body.password || '';

    if (username == '' || password == '' || fname == '' || lname == '' || photo == '') {
      res.status(401);
      res.json({
        "status": 401,
        "message": "All fields are required.",
        "data" : []
      });
      return;
    }
	

    User.findOne( username , function(err, user) {
    	
    	if(user){
    		  res.status(401);
		      res.json({
		        "status": 401,
		        "message": "User exists.",
            "data" : []
		      });
    	}else if(user == null && err == null){

    		user = new User();
		    // set the user's local credentials
		    user.email    = username;
		    user.password = user.generateHash(password);
        
        
        user.createdate = new Date().toISOString();
        user.updatedate = new Date().toISOString();
        user.status = 1;
        user.role = 'user';

        user.fname = fname;
        user.lname = lname;
        user.photo = photo;
        
        
        //console.log(new Date().toISOString());
		    //console.log(username, password, user);
		    
		    user.save(function(newUser) {
		        //console.log("userid: "+user.u_id);
		        console.log("saved user: ", user);
		        //passport.authenticate();
		        //return done(null, user);
		        //newUser.password = newUser.generateHash(password);
		        res.status(200);
    				res.json({
    				"status": 200,
    				"message": "User successfully created.",
            "data" : []
    				});
		    });
    	}

    });

    

  },

  validate: function(username, password) {
  	var dbUserObj; 
  	
  },

  validateUser: function(username) {
    // spoofing the DB response for simplicity

    var dbUserObj = false;

	/*async.series([
        //Load user to get userId first
        function(callback) {
            client.query("SELECT * from users where email=$1", [username], function(err, users){
                if (err) return callback(err);
                //Check that a user was found
                if (users.length == 0) {
                    return callback(new Error('No user with name '+name+' found.'));
                }
                var user = users[0];
                userId = user.id; //Set the userId here, so the next tasks can access it
                locals.user = {
                    name: user.name,
                    email: user.email,
                    bio: user.bio
                };

                dbUserObj = { // spoofing a userobject from the DB. 
			      name: 'arvind',
			      role: 'admin',
			      username: 'arvind@myapp.com'
			    };

                callback();
            });
        },
        //Load posts and photos in parallel (won't be called before task 1's "task callback" has been called)
        function(callback) {
            async.parallel([
                //Load posts
                function(callback) {
                    callback();
                        return dbUserObj
                }
            ], callback); //Remember to put in the second series task's "task callback" as the "final callback" for the async.parallel operation
        }
    ]);*/

    
    var client = new pg.Client(connectionString);
    client.connect();
    client.query("SELECT * from users where email=$1", [username], function(err, users){
                if (err) return callback(err);
                //Check that a user was found
                if (users.length == 0) {
                    return callback(new Error('No user with name '+name+' found.'));
                }
                var user = users[0];
                console.log("users: "+users.length);
                /*userId = user.id; //Set the userId here, so the next tasks can access it
                locals.user = {
                    name: user.name,
                    email: user.email,
                    bio: user.bio
                };*/

                client.end();
            });




          dbUserObj = { // spoofing a userobject from the DB. 
                  name: 'arvind',
                  role: 'admin',
                  username: 'arvind@myapp.com'
                };

          return dbUserObj;
		      
    
  },
}

// private method
function genToken(user) {
  var expires = expiresIn(365); // in days
  var token = jwt.encode({
    exp: expires,
    username: user.username
  }, require('../config/secret')());

  return {
    token: token,
    expires: expires,
    user: user
  };
}

function expiresIn(numDays) {
  var dateObj = new Date();
  return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;
