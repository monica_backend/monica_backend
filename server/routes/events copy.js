

var g = require('ger')
var esm = new g.MemESM()
var ger = new g.GER(esm);

var pg = require('pg');
var connectionString = require('../config/database');


var events = {

  getAll: function(req, res) {
    //var allProducts = data; // Spoof a DB call
    //res.json(allProducts);



 
ger.initialize_namespace('events')
.then( function() {

  var client = new pg.Client(connectionString);
  client.connect();
  client.query('SELECT * FROM topics', null, function(err, result){

            if(err){
                return ;
            }
            //if no rows were returned from query, then new user
            if (result.rows.length > 0){
                //console.log(result.rows.length);
                reslen = result.rows.length;
                var topics = new Array;
                for(i=0; i<reslen; i++){
                  
                  var mytopic = {
                    namespace: 'events',
                    person: 'bob-'+result.rows[i].topic_branch,
                    action: 'likes',
                    thing: result.rows[i].topic_related,
                    expires_at: '2020-06-06'
                  };
                  topics.push(mytopic);
                }

                var mytopic = {
                    namespace: 'events',
                    person: 'alice',
                    action: 'likes',
                    thing: 'green_energy',
                    expires_at: '2020-06-06'
                  };
                  topics.push(mytopic);

                console.log(topics);
                return ger.events([
    //Engineer
    {
      namespace: 'events',
      person: 'bob-engineer',
      action: 'likes',
      thing: 'math',
      expires_at: '2020-06-06'
    },
    //Greenenergy
    {
      namespace: 'events',
      person: 'bob-greenenergy',
      action: 'likes',
      thing: 'solar_power',
      expires_at: '2020-06-06'
    },
    {
      namespace: 'events',
      person: 'bob-greenenergy',
      action: 'likes',
      thing: 'green_energy',
      expires_at: '2020-06-06'
    },
    {
      namespace: 'events',
      person: 'bob-greenenergy',
      action: 'likes',
      thing: 'wind_mills',
      expires_at: '2020-06-06'
    },
    {
      namespace: 'events',
      person: 'bob-greenenergy',
      action: 'likes',
      thing: 'alternative_energy',
      expires_at: '2020-06-06'
    },
    //Graphic Web
    {
      namespace: 'events',
      person: 'bob-graphicweb',
      action: 'likes',
      thing: 'photoshop',
      expires_at: '2020-06-06'
    },
    {
      namespace: 'events',
      person: 'bob-graphicweb',
      action: 'likes',
      thing: 'illustrator',
      expires_at: '2020-06-06'
    },
    {
      namespace: 'events',
      person: 'bob-graphicweb',
      action: 'likes',
      thing: 'responsive_design',
      expires_at: '2020-06-06'
    },
    {
      namespace: 'events',
      person: 'bob-graphicweb',
      action: 'likes',
      thing: 'web_design',
      expires_at: '2020-06-06'
    },
    {
      namespace: 'events',
      person: 'bob-graphicweb',
      action: 'likes',
      thing: 'app_design',
      expires_at: '2020-06-06'
    },


    {
      namespace: 'events',
      person: 'alice',
      action: 'likes',
      thing: 'app_design',
      expires_at: '2020-06-06'
    },
  ]);
            }
        });

  
})
.then( function() {
  // What things might alice like? 
  return ger.recommendations_for_person('events', 'alice', {actions: {likes: 1}})
})
.then( function(recommendations) {
  console.log("\nRecommendations For 'alice'")
  console.log(JSON.stringify(recommendations,null,2))
})
.then( function() {
  // What things are similar to xmen? 
  return ger.recommendations_for_thing('events', 'green_energy', {actions: {likes: 1}})
})
.then( function(recommendations) {
  console.log("\nRecommendations Like 'green_energy'")
  console.log(JSON.stringify(recommendations,null,2))
});

  },

  getOne: function(req, res) {
    var id = req.params.id;
    var product = data[0]; // Spoof a DB call
    res.json(product);
  },

  create: function(req, res) {
    var newProduct = req.body;
    data.push(newProduct); // Spoof a DB call
    res.json(newProduct);
  },

  update: function(req, res) {
    var updateProduct = req.body;
    var id = req.params.id;
    data[id] = updateProduct // Spoof a DB call
    res.json(updateProduct);
  },

  delete: function(req, res) {
    var id = req.params.id;
    data.splice(id, 1) // Spoof a DB call
    res.json(true);
  }
};

var data = [{
  name: 'event 1',
  id: '1'
}, {
  name: 'event 2',
  id: '2'
}, {
  name: 'event 3',
  id: '3'
}];

module.exports = events;
