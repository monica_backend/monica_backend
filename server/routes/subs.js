
var pg = require('pg');

var connectionString = require('../config/database');

var async = require('async');

var User = require('../models/user');

var subs = {

  //get all subscriptions of a user
  getAll: function(req, res) {
    //var allProducts = data; // Spoof a DB call
    //res.json(allProducts);
    
    subscriptions = new Array;

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    

    User.findOne( username , function(err, user) {
        if(user){
           //console.log(user);
           user_id = user.u_id;
           //console.log(user_id);
           var client = new pg.Client(connectionString);
            
            //console.log("email is: "+key);
            client.connect();
            client.query("SELECT * from subscription where user_id=$1", [user_id], function(err, result){
                console.log(result.rows[0]);
                if(err){
                    return ;
                }
                if(result.rows.length > 0){
                   //console.log(result.rows);
                   reslen = result.rows.length;
                  
                  for(i=0; i<reslen; i++){
                      subscriptions.push({
                        'subs_id':result.rows[i].subs_id, 
                        'event_id':result.rows[i].event_id, 
                        //'topic_one':result.rows[i].topic_one, 
                        //'topic_two':result.rows[i].topic_two, 
                        //'topic_three':result.rows[i].topic_three
                      });
                    //console.log(result.rows[i].topic_branch);
                  }

                  
                }

                client.end();

                res.status(200);
                res.json({
                        "status": 200,
                        "message": "",
                        "data" : subscriptions
                        });
                
            });

        }
        
    });

    //console.log(subscriptions);
    

  },

  checkSubscribed: function(req, res) {
    var event_id = req.params.id || '';

    if (event_id == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID is missing",
        "data" : []
      });
      return;
    }


    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){

          alreadysub = 0;
          user_id = user.u_id;
          subsid = "";
          msg = '';

          async.series([
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("select * from subscription where user_id=$1 and event_id=$2", [user_id, event_id], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return ;
                    }
                    if(result.rows.length > 0){
                      alreadysub = 1;
                    }

                    client.end();

                    res.status(200);
                    res.json({
                      "status": 200,
                      "message": "",
                      "data" : [{"subscribed":alreadysub}]
                    });
                    
                });
              },
              //Load user to get `userId` first
              
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });




           //console.log(user);
            

        }
        
    });



  },

  create: function(req, res) {
    //var newProduct = req.body;
    //console.log(req.params);

    var event_id = req.body.event_id || '';

    if (event_id == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID is missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);

    //var user_id = req.params.user_id;
    //var event_id = req.body.event_id;
    //var topic_one = req.body.topic_one;
    //var topic_two = req.body.topic_two;
    //var topic_three = req.body.topic_three;
    
    User.findOne( username , function(err, user) {
        if(user){

          alreadysub = 0;
          user_id = user.u_id;
          subsid = "";
          msg = '';

          async.series([
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("select * from subscription where user_id=$1 and event_id=$2", [user_id, event_id], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return ;
                    }
                    if(result.rows.length > 0){
                      alreadysub = 1;
                    }

                    client.end();
                    callback();
                });
              },
              //Load user to get `userId` first
              function(callback) {
                //console.log(topicbranches);

                if(!alreadysub){
                  var client = new pg.Client(connectionString);
                  client.connect();
                  client.query("INSERT INTO subscription(user_id, event_id) VALUES($1, $2) RETURNING subs_id", [user_id, event_id], function(err, result){
                      //console.log(result.rows[0]);
                      if(err){
                          return ;
                      }
                      //console.log(result);

                      client.end();
                      
                      subsid = result.rows[0].subs_id;
                      msg = "Successfully subscribed.";
                      res.status(200);
                      res.json({
                        "status": 200,
                        "message": msg,
                        "data" : ""
                      });
                  });
                }else{
                  msg = "Already subscribed.";
                  res.status(401);
                  res.json({
                    "status": 401,
                    "message": msg,
                    "data" : ""
                  });
                }

                

              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });




           //console.log(user);
            

        }
        
    });
  },

  update: function(req, res) {
    var updateProduct = req.body;
    var id = req.params.id;
    data[id] = updateProduct // Spoof a DB call
    res.json(updateProduct);
  },

  delete: function(req, res) {
    var event_id = req.params.id;

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);

    User.findOne( username , function(err, user) {
        if(user){

          alreadysub = 0;
          user_id = user.u_id;
          
          async.series([
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("select * from subscription where user_id=$1 and event_id=$2", [user_id, event_id], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return ;
                    }
                    if(result.rows.length > 0){
                      alreadysub = 1;
                    }

                    client.end();
                    callback();
                });
              },
              //Load user to get `userId` first
              function(callback) {
                //console.log(topicbranches);

                if(alreadysub){
                  var client = new pg.Client(connectionString);
                  client.connect();
                  client.query("DELETE FROM subscription WHERE user_id=$1 and event_id=$2", [user_id, event_id], function(err, result){
                      //console.log(result.rows[0]);
                      if(err){
                          return ;
                      }

                      client.end();
                      
                      //console.log(result);
                      //subsid = result.rows[0].subs_id;
                      msg = "Successfully unsubscribed.";
                      res.status(200);
                      res.json({
                        "status": 200,
                        "message": msg,
                        "data" : ""
                      });
                  });
                }else{
                  msg = "Already unsubscribed.";
                  res.status(401);
                  res.json({
                    "status": 401,
                    "message": msg,
                    "data" : ""
                  });
                }

                

              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });




           //console.log(user);
            

        }
        
    });

  }
};

var data = [{
  name: 'product 1',
  id: '1'
}, {
  name: 'product 2',
  id: '2'
}, {
  name: 'product 3',
  id: '3'
}];

module.exports = subs;
