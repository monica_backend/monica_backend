
var pg = require('pg');

var connectionString = require('../config/database');

var async = require('async');

var User = require('../models/user');
var Notification = require('../models/notification');



var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];


var users = {

  getAll: function(req, res) {
    var allusers = data; // Spoof a DB call
    res.json(allusers);
  },

  viewOne: function(req, res) {
    var id = req.params.id;
    console.log(id);

    var linkedin_headline;
    var linkedin_industry;
    var linkedin_summary;
    var speaker = new Array;
    var subeventids = new Array;
    var subevents = new Array;


    async.series([
        //Load user to get `userId` first
        
        function(callback){
          var client = new pg.Client(connectionString);
          
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM user_linkedin WHERE user_id = $1", [id], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    linkedin_headline = result.rows[i].linkedin_headline;
                    linkedin_industry = result.rows[i].linkedin_industry;
                    linkedin_summary = result.rows[i].linkedin_summary;
                  }
                  
              }

            client.end();
            callback();
            
          });
        },
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT users.*, profile.* FROM users INNER JOIN profile ON users.u_id = profile.profile_user WHERE profile.profile_user = $1", [id], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    speaker = [{    
                                    'user_id':result.rows[i].profile_user, 
                                    'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                    'photo':result.rows[i].profile_photo,
                                    'email':result.rows[i].email,
                                    'headline': linkedin_headline,
                                    'industry' : linkedin_industry,
                                    'summary' : linkedin_summary,
                                    'subevents': subevents
                                    }];
                  }
                  
              }

            client.end();
            callback();
            
          });
        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(allEvents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : speaker
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },


  sendMessage: function(req, res) {
    var message = req.body.message || '';

    if (message == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Message is missing",
        "data" : []
      });
      return;
    }


    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){

          usremail = user.email;
          msgbody = "email: " + usremail + "\r\n </br> message: " + message;

          var nodemailer = require('nodemailer');

          // create reusable transporter object using the default SMTP transport
          var transporter = nodemailer.createTransport('smtps://monicaevents11%40gmail.com:Monica123@smtp.gmail.com');

          // setup e-mail data with unicode symbols
          var mailOptions = {
              from: '"Monica 👥" <monicaevents11@gmail.com>', // sender address
              to: 'richard@monica.events, bashir@monica.events', // list of receivers
              subject: 'Contact Us ✔', // Subject line
              text: msgbody, // plaintext body
              html: msgbody // html body
          };

          // send mail with defined transport object
          transporter.sendMail(mailOptions, function(error, info){
              if(error){
                  return console.log(error);
              }
              res.status(200);
              res.json({
                      "status": 200,
                      "message": "Message sent.",
                      "data" : 1
                      });
              //console.log('Message sent: ' + info.response);
          });
           //console.log(user);
            

        }
        
    });



  },


  getSuggestedUsers: function(req, res) {

    var event_id = req.params.id || '';

    var topicids = new Array;
    var topics = new Array;
    var userids = new Array;
    var users = new Array;
    var waveduserids = new Array;
    var wavedforuserids = new Array;
    var accepteduserids = new Array;

    if (event_id == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID is missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){

          
          async.series([
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM answer_three WHERE user_id=$1 AND event_id=$2", [user.u_id, event_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              topicids.push(result.rows[i].topic_subsubcat);
                            }

                      }

                      client.end();
                      done();
                    });
                  },
                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM linkedin_industry WHERE industry_id=ANY($1)", [topicids], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              topics.push(result.rows[i].industry_name);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                    
                  },
                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM user_linkedin WHERE linkedin_industry=ANY($1)", [topics], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              userids.push(result.rows[i].user_id);
                            }
                            //console.log(userids);
                      }

                      client.end();
                      done();
                    });
                    
                  },
                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND to_user=ANY($2) AND from_user=$3", [event_id, userids, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              wavedforuserids.push(result.rows[i].to_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                    
                  },

                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND to_user=$3 AND from_user=ANY($2)", [event_id, userids, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              waveduserids.push(result.rows[i].from_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                    
                  },


                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND status=1 AND to_user=$3 AND from_user=ANY($2)", [event_id, userids, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              accepteduserids.push(result.rows[i].from_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                    
                  },
                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND status=1 AND to_user=ANY($2) AND from_user=$3", [event_id, userids, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              accepteduserids.push(result.rows[i].to_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                    
                  },





                  function(callback){
                    var client = new pg.Client(connectionString);
            
                    //console.log("email is: "+key);
                    client.connect();
                    client.query("SELECT users.*, profile.* FROM users INNER JOIN profile ON users.u_id = profile.profile_user WHERE profile.profile_user = ANY($1)", [userids], function(err, result){
                        //console.log(err);
                        if(err){
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              //eventids.push(result.rows[i].event_id);
                              //console.log(result.rows[i].profile_fname);
                              users.push({  'user_id':result.rows[i].profile_user, 
                                                'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                                'photo':result.rows[i].profile_photo,
                                                'email':result.rows[i].email,
                                                'saidhello': 0
                                                });
                            }
                            
                        }

                        client.end();
                        callback();
                      
                    });
                  },
                  
                  function(callback) {
                    reslen = users.length;
                    console.log(waveduserids);
                    for(i=0; i<reslen; i++){
                      if(waveduserids.indexOf(users[i].user_id) != -1)
                      users[i].saidhello = 2;
                    }
                    callback();
                  },

                  function(callback) {
                    reslen = users.length;
                    console.log(waveduserids);
                    for(i=0; i<reslen; i++){
                      if(wavedforuserids.indexOf(users[i].user_id) != -1)
                      users[i].saidhello = 3;
                    }
                    callback();
                  },

                  function(callback) {
                    reslen = users.length;
                    console.log(waveduserids);
                    for(i=0; i<reslen; i++){
                      if(accepteduserids.indexOf(users[i].user_id) != -1)
                      users[i].saidhello = 1;
                    }
                    callback();
                  },
                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "",
                            "data" : users
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
          
        }
        
    });
  },

  sayHello: function(req, res) {

    var event_id = req.params.id || '';
    var to_user_id = req.params.userid || '';

    var alreadysaid = 0;
    var alreadyinvited = 0;
    var status = 1;

    fromprofilename = '';
    toprofilename = '';



    if (event_id == '' || to_user_id == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID and/or User ID is missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){
          
          async.series([
                  function(callback){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM profile WHERE profile_user=$1", [user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                          //console.log(result);
                            fromprofilename = result.rows[0].profile_fname + ' ' + result.rows[0].profile_lname;
                        }

                        client.end();
                        callback();
                      
                    });
                  },
                  function(callback){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM profile WHERE profile_user=$1", [to_user_id], function(err, result){
                        //console.log(err);
                        if(err){
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                          //console.log(result);
                            toprofilename = result.rows[0].profile_fname + ' ' + result.rows[0].profile_lname;
                        }

                        client.end();
                        callback();
                      
                    });
                  },

                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND from_user=$2 AND to_user=$3", [event_id, user.u_id, to_user_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            alreadysaid = 1;
                            if(result.rows[0].status == 1){
                              status = 1;
                            }else{
                              status = 3;
                            }
                        }

                      client.end();
                      done();
                    });
                  },
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND from_user=$2 AND to_user=$3", [event_id, to_user_id, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            //if(result.rows[0].status == 1){
                              alreadyinvited = 1;
                              if(result.rows[0].status == 1){
                                status = 1;
                              }else{
                                status = 1;
                              }
                            //}
                            console.log(to_user_id, user.u_id);
                        }

                      client.end();
                      done();
                    });
                  },
                  function(done){
                    //console.log(user);

                    if(!alreadysaid && !alreadyinvited &&  user.u_id !=to_user_id){
                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query("INSERT INTO waves(event_id, from_user, to_user, status) VALUES($1, $2, $3, 0)", [event_id, user.u_id, to_user_id], function(err, result){
                          //console.log(err);
                          if(err){
                            console.log(err);
                              return ;
                          }
                          status = 3;
                        client.end();

                        msgtext = fromprofilename + " has said hello!";
                        notification = new Notification();
                        notification.newsevent = event_id;
                        notification.newstext = msgtext;
                        notification.newsicon = "http://image.flaticon.com/icons/png/512/126/126501.png";
                        notification.newsdetails= {
                            "fromuser": user.u_id,
                            "page": "attendee"
                            };
                        notification.newsuser= to_user_id;
                        notification.save();

                        notification.sendPush(to_user_id, msgtext, event_id);


                        done();
                      });
                    }else if(alreadyinvited){
                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query("UPDATE waves SET status=1 WHERE event_id=$1 AND from_user=$2 AND to_user=$3", [event_id, to_user_id, user.u_id], function(err, result){
                          //console.log(err);
                          if(err){
                            console.log(err);
                              return ;
                          }

                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                              //console.log(result.rows.length);
                              status = 1;
                          }

                        client.end();

                        msgtext = fromprofilename + " has said hello back!";

                        notification = new Notification();
                        notification.newsevent = event_id;
                        notification.newstext = msgtext;
                        notification.newsicon = "http://image.flaticon.com/icons/png/512/126/126501.png";
                        notification.newsdetails= {
                            "fromuser": user.u_id,
                            "page": "attendee"
                            };
                        notification.newsuser= to_user_id;
                        notification.save();

                        notification.sendPush(to_user_id, msgtext, event_id);

                        done();
                      });
                    }else{
                      done();
                    }
                    
                  },
                  
                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "Success",
                            "data" : status
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
          
        }
        
    });
  },


  getHellos: function(req, res) {

    var event_id = req.params.id || '';

    var userids = new Array;
    var users = new Array;
    var waveduserids = new Array;
    var wavedforuserids = new Array;
    var accepteduserids = new Array;

    if (event_id == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID is missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){
          
          async.series([
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND to_user=$2", [event_id, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              userids.push(result.rows[i].from_user);
                            }
                        }

                      client.end();
                      done();
                    });
                  },

                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND to_user=ANY($2) AND from_user=$3", [event_id, userids, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              wavedforuserids.push(result.rows[i].to_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                    
                  },

                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND to_user=$3 AND from_user=ANY($2)", [event_id, userids, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              waveduserids.push(result.rows[i].from_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                    
                  },

                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND status=1 AND to_user=$3 AND from_user=ANY($2)", [event_id, userids, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              accepteduserids.push(result.rows[i].from_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                    
                  },
                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND status=1 AND to_user=ANY($2) AND from_user=$3", [event_id, userids, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              accepteduserids.push(result.rows[i].to_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                    
                  },

                  function(callback){
                    var client = new pg.Client(connectionString);
                    
                    //console.log("email is: "+key);
                    client.connect();
                    client.query("SELECT users.*, profile.* FROM users INNER JOIN profile ON users.u_id = profile.profile_user WHERE profile.profile_user = ANY($1)", [userids], function(err, result){
                        //console.log(err);
                        if(err){
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              //eventids.push(result.rows[i].event_id);
                              //console.log(result.rows[i].profile_fname);
                              users.push({  'user_id':result.rows[i].profile_user, 
                                                'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                                'photo':result.rows[i].profile_photo,
                                                'email':result.rows[i].email,
                                                'saidhello': 0
                                                });
                            }
                            
                        }

                        client.end();
                        callback();
                      
                    });
                  },
                  function(callback) {
                    reslen = users.length;
                    console.log(waveduserids);
                    for(i=0; i<reslen; i++){
                      if(waveduserids.indexOf(users[i].user_id) != -1)
                      users[i].saidhello = 2;
                    }
                    callback();
                  },

                  function(callback) {
                    reslen = users.length;
                    console.log(waveduserids);
                    for(i=0; i<reslen; i++){
                      if(wavedforuserids.indexOf(users[i].user_id) != -1)
                      users[i].saidhello = 3;
                    }
                    callback();
                  },

                  function(callback) {
                    reslen = users.length;
                    console.log(waveduserids);
                    for(i=0; i<reslen; i++){
                      if(accepteduserids.indexOf(users[i].user_id) != -1)
                      users[i].saidhello = 1;
                    }
                    callback();
                  },
                  
                  
                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "",
                            "data" : users
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
          
        }
        
    });
  },

  checkHello: function(req, res) {

    var user_id = req.params.id || '';
    var status = 0;
   

    if (user_id == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "User ID is missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){
          
          async.series([
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE to_user=$1 AND from_user=$2 AND status=0", [user_id, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            status = 3;
                        }

                      client.end();
                      done();
                    });
                  },
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE to_user=$1 AND from_user=$2 AND status=0", [user.u_id, user_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            status = 2;
                        }

                      client.end();
                      done();
                    });
                  },
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE ((to_user=$1 AND from_user=$2) OR (to_user=$2 AND from_user=$1)) AND status=1", [user.u_id, user_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            status = 1;
                        }

                      client.end();
                      done();
                    });
                  },

                  
                  
                  
                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "",
                            "data" : status
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
          
        }
        
    });
  },


  handshakeDo: function(req, res) {
    var status = 0;

    var event_id = req.body.event_id || '';
    //var from_user = req.body.from_user || '';
    var recipient_type = req.body.recipient_type || '';
    var to_company = req.body.to_company || '';
    var to_user = req.body.to_user || '';
    var note = req.body.note || '';
    var action = req.body.action || 0;
    var date = req.body.date || '';

    fromprofilename = '';
    toprofilename = '';


    if (event_id == '' || recipient_type == '' || !(to_user instanceof Array) || date == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "All fields are required.",
        "data" : []
      });
      return;
    }


    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){

          usremail = user.email;

          async.series([
                function(callback){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM profile WHERE profile_user=$1", [user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                          //console.log(result);
                            fromprofilename = result.rows[0].profile_fname + ' ' + result.rows[0].profile_lname;
                        }

                        client.end();
                        callback();
                      
                    });
                  },
                  /*function(callback){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM profile WHERE profile_user=$1", [to_user_id], function(err, result){
                        //console.log(err);
                        if(err){
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                          //console.log(result);
                            toprofilename = result.rows[0].profile_fname + ' ' + result.rows[0].profile_lname;
                        }

                        client.end();
                        callback();
                      
                    });
                  },*/

                function(callback) {
                    var client = new pg.Client(connectionString);
                    client.connect();

                    sizecat = to_user.length;
                    iter = 0;

                    function insertData(item,callback) {
                      
                      client.query("INSERT INTO handshake (event_id, recipient_type, from_user, to_user, to_company, notes, action, date) VALUES($1,$2,$3,$4,$5,$6,$7,$8)", [event_id, recipient_type, user.u_id, item, to_company, note, action, date], 
                      function(err,result) {
                        // return any err to async.each iterator
                        //callback(err);
                        if (err) {
                          return console.error('error running query', err);
                        }else{
                          status = 1;
                        }
                        
                        iter++;

                        msgtext = fromprofilename + " has handshaked you!";

                        notification = new Notification();
                        notification.newsevent = event_id;
                        notification.newstext = msgtext;
                        notification.newsicon = "http://image.flaticon.com/icons/png/512/126/126501.png";
                        notification.newsdetails= {
                            "fromuser": user.u_id,
                            "page": "handshake"
                            };
                        notification.newsuser= item;
                        notification.save();

                        notification.sendPush(item, msgtext, event_id);

                        
                        /*if(iter > (sizecat-1)){
                        }*/
                        //client.end();
                        callback();

                      })
                    }
                    async.each(to_user,insertData,function(err) {
                      // Release the client to the pg module
                      //done();
                      if (err) {
                        return console.error('error running query', err);
                      }
                      client.end();
                      callback();
                      //console.log(sizecat);

                    })

                   
                  },
                  
                  function(callback) {
                    //console.log(allEvents);
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "",
                            "data" : status
                            });
                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );

        }
        
    });



  },



  handshakeUpdate: function(req, res) {
    var status = 0;
    var rtype;
    var company_id = 0;
    var event_id;
    var compusers = new Array;
    var addusers = new Array;
    var remusers = new Array;

    var handshake_id = req.body.handshake_id || '';
    var to_user = req.body.to_user || '';
    var note = req.body.note || '';
    var action = req.body.action || 0;
    var date = req.body.date || '';


    if (handshake_id == '' || !(to_user instanceof Array) || date == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "All fields are required.",
        "data" : []
      });
      return;
    }


    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){

          usremail = user.email;

          async.series([
                function(callback){
                  var client = new pg.Client(connectionString);
                  client.connect();

                  client.query("SELECT * FROM handshake WHERE handshake_id=$1", [handshake_id], 
                    function(err,result) {
                      // return any err to async.each iterator
                      //callback(err);
                      if (err) {
                        return console.error('error running query', err);
                      }

                      if (result.rows.length > 0){
                        rtype = result.rows[0].recipient_type;
                        company_id = result.rows[0].to_company;
                        event_id = result.rows[0].event_id;
                      }
                      
                      
                      /*if(iter > (sizecat-1)){
                      }*/
                      //client.end();
                      callback();

                    });
                },
                function(callback) {

                  if(rtype == 2){
                    compusers.push(to_user[0]);
                    callback();
                  }else if(rtype == 1){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    //client.query("select distinct on (date) * from handshake order by date, from_user", null, function(err, result){
                    client.query("SELECT * FROM handshake WHERE event_id=$1 AND from_user=$2 AND to_company=$3", [event_id, user.u_id, company_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            reslen = result.rows.length;
                            for(i=0; i<reslen; i++){
                              compusers.push(result.rows[i].to_user);
                            }
                        }
                        console.log('compusrs: '+compusers);

                      client.end();
                      callback();

                    });
                  }

                   
                  },

                 /* */

                  function(callback) {
                    if(rtype == 2){
                      callback();
                    }else if(rtype == 1){

                      tousrs = to_user.length;
                      compusrs = compusers.length;

                      for(i=0; i<tousrs; i++){
                        //for(j=0; j<compusrs; j++){
                          if(compusers.indexOf(to_user[i]) == -1){
                            addusers.push(to_user[i]);
                          }
                        //}
                      }
                      console.log('addusers: '+addusers);
                      callback();
                    }

                    
                      
  
                  },

                  function(callback) {
                    if(rtype == 2){
                      callback();
                    }else if(rtype == 1){

                      tousrs = to_user.length;
                      compusrs = compusers.length;

                      for(i=0; i<compusrs; i++){
                        //for(j=0; j<compusrs; j++){
                          if(to_user.indexOf(compusers[i]) == -1){
                            remusers.push(compusers[i]);
                          }
                        //}
                      }
                      console.log('remusers: '+remusers);
                      callback();
                    }

                    
                      
  
                  },

                  function(callback){
                    var client = new pg.Client(connectionString);
                      client.connect();

                      sizecat = addusers.length;
                      iter = 0;

                      function insertData(item,callback) {
                        
                        client.query("INSERT INTO handshake (event_id, recipient_type, from_user, to_user, to_company, notes, action, date) VALUES($1,$2,$3,$4,$5,$6,$7,$8)", [event_id, rtype, user.u_id, item, company_id, note, action, date], 
                        function(err,result) {
                          // return any err to async.each iterator
                          //callback(err);
                          if (err) {
                            return console.error('error running query', err);
                          }else{
                            status = 1;
                          }
                          
                          iter++;
                          
                          /*if(iter > (sizecat-1)){
                          }*/
                          //client.end();
                          callback();

                        })
                      }
                      async.each(addusers,insertData,function(err) {
                        // Release the client to the pg module
                        //done();
                        if (err) {
                          return console.error('error running query', err);
                        }
                        client.end();
                        callback();
                        //console.log(sizecat);

                      })

                  },

                  function(callback){
                    var client = new pg.Client(connectionString);
                      client.connect();

                      sizecat = remusers.length;
                      iter = 0;

                      function insertData(item,callback) {
                        console.log("delete item: "+item);
                        client.query("DELETE FROM handshake WHERE event_id=$1 AND recipient_type=$2 AND from_user=$3 AND to_user=$4", [event_id, rtype, user.u_id, item], 
                        function(err,result) {
                          // return any err to async.each iterator
                          //callback(err);
                          if (err) {
                            return console.error('error running query', err);
                          }else{
                            status = 1;
                          }
                          
                          iter++;
                          
                          /*if(iter > (sizecat-1)){
                          }*/
                          //client.end();
                          callback();

                        })
                      }
                      async.each(remusers,insertData,function(err) {
                        // Release the client to the pg module
                        //done();
                        if (err) {
                          return console.error('error running query', err);
                        }
                        client.end();
                        callback();
                        //console.log(sizecat);

                      })
                  },

                  function(callback){
                    var client = new pg.Client(connectionString);
                    client.connect();

                    sizecat = compusers.length;
                    iter = 0;

                    function insertData(item,callback) {
                      client.query("UPDATE handshake SET to_user=$2, notes=$3, action=$4, date=$5 WHERE handshake_id=$1", [handshake_id, item, note, action, date], 
                      function(err,result) {
                        // return any err to async.each iterator
                        //callback(err);
                        if (err) {
                          return console.error('error running query', err);
                        }else{
                          status = 1;
                        }
                        
                        iter++;
                        
                        
                        //client.end();
                        callback();

                      })
                    }

                    async.each(compusers,insertData,function(err) {
                      // Release the client to the pg module
                      //done();
                      if (err) {
                        return console.error('error running query', err);
                      }
                      client.end();
                      callback();
                      //console.log(sizecat);

                    })



                  },

                  
                  function(callback) {
                    //console.log(allEvents);
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "",
                            "data" : status
                            });
                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );

        }
        
    });



  },


  getUserHandshakes: function(req, res) {

    var event_id = req.params.id || '';

    var userids = new Array;
    var users = new Array;
    var hsids = new Array;
    var handshakes = new Array;

    if (event_id == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID is missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){
          
          async.series([
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM handshake WHERE event_id=$1 AND from_user=$2 AND recipient_type=2", [event_id, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              userids.push(result.rows[i].to_user);
                              //hsids[result.rows[i].to_user] = result.rows[i].handshake_id;
                              hsids.push(result.rows[i].handshake_id);
                            }
                        }

                      client.end();
                      done();
                    });
                  },


                  function(callback){
                    var client = new pg.Client(connectionString);
                    
                    //console.log("email is: "+key);
                    client.connect();
                    client.query("SELECT users.*, profile.* FROM users INNER JOIN profile ON users.u_id = profile.profile_user WHERE profile.profile_user = ANY($1)", [userids], function(err, result){
                        //console.log(err);
                        if(err){
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              //eventids.push(result.rows[i].event_id);
                              //console.log(result.rows[i].profile_fname);
                              users.push({  'user_id':result.rows[i].profile_user, 
                                                'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                                'photo':result.rows[i].profile_photo,
                                                'email':result.rows[i].email,
                                                'saidhello': 0,
                                                //'handshake_id':hsids[result.rows[i].profile_user]
                                                });
                            }
                            
                        }

                        client.end();
                        callback();
                      
                    });
                  },
                  function(callback){
                    hsidslen = hsids.length;
                    usrlen = users.length;
                    for(i=0; i<hsidslen; i++){

                      for(j=0; j<usrlen; j++){
                        if(userids[i] == users[j].user_id){
                          handshakes.push({  'user_id':userids[i], 
                                            'name':users[j].name, 
                                            'photo':users[j].photo,
                                            'email':users[j].email,
                                            'saidhello': 0,
                                            'handshake_id':hsids[i]
                                            });
                        }
                        
                      }
                      
                    }
                    callback();
                  },

                  /*function(callback){
                    reslen = users.length;
                    for(i=0; i<reslen; i++){
                      users[i].handshake_id = h
                    }

                  },*/
                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "",
                            "data" : handshakes
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
          
        }
        
    });
  },


  getCompanyHandshakes: function(req, res) {

    var event_id = req.params.id || '';

    var userids = new Array;
    var users = new Array;
    var hsids = new Array;
    var companyids = new Array;
    var companies = new Array;
    var handshakes = new Array;
    var hsfiltered = new Array;

    if (event_id == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID is missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){
          
          async.series([
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    //client.query("select distinct on (date) * from handshake order by date, from_user", null, function(err, result){
                    client.query("SELECT * FROM handshake WHERE event_id=$1 AND from_user=$2 AND recipient_type=1", [event_id, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              userids.push(result.rows[i].to_user);
                              companyids.push(result.rows[i].to_company);
                              hsids[result.rows[i].to_user] = result.rows[i].handshake_id;
                              handshakes.push({ 'handshake_id':result.rows[i].handshake_id, 
                                                'to_company':result.rows[i].to_company, 
                                                'to_user':result.rows[i].to_user,
                                                'date':result.rows[i].date,
                                                'unique': 1
                                                });
                            }
                        }

                      client.end();
                      done();
                    });
                  },

                  /*function(callback){
                    hslen = handshakes.length;
                    for(i=0; i<hslen; i++){
                      hsdate = handshakes[i].date;
                      hscompany = handshakes[i].to_company;

                      thiscount = 0;

                      hsflen = hsfiltered.length;
                      console.log("length: "+hsflen);
                      if(hsflen == 0){
                        hsfiltered.push({   'handshake_id':handshakes[i].handshake_id, 
                                            'to_company':handshakes[i].to_company, 
                                            'to_user':handshakes[i].to_user,
                                            'date':handshakes[i].date,
                                            });
                        thiscount++;
                      }
                      else{
                        for(j=0; j<hsflen; j++){
                          console.log(hsfiltered[j].to_company);
                          console.log(hsfiltered[j].date);
                          if(hsfiltered[j].to_company != hscompany && hsfiltered[j].date != hsdate && thiscount == 0){
                            
                            thiscount = 1;

                          }


                        }
                        if(thiscount){
                            hsfiltered.push({ 'handshake_id':handshakes[i].handshake_id, 
                                              'to_company':handshakes[i].to_company, 
                                              'to_user':handshakes[i].to_user,
                                              'date':handshakes[i].date,
                                              });
                          }
                      }
                      

                    }
                    callback();
                  },*/

                  function(callback){
                    hslen = handshakes.length;
                    for(i=0; i<hslen; i++){

                      hsdate = new Date(handshakes[i].date);
                      hsdate = months[hsdate.getMonth()] + ' ' + hsdate.getDate() + ',' +  hsdate.getFullYear();
                      hscompany = handshakes[i].to_company;
                      hsunique = handshakes[i].unique;

                      if(hsunique){
                        for(j=0; j<hslen; j++){
                          jdate = new Date(handshakes[j].date);
                          jdate = months[jdate.getMonth()] + ' ' + jdate.getDate() + ',' +  jdate.getFullYear();

                          //console.log("---------");
                          //console.log("i: "+i+"; j: "+j+"; company i: "+handshakes[i].to_company+"; company j: "+handshakes[j].to_company+"; date i: "+hsdate+"; date j: "+jdate);
                          //console.log(i!=j && handshakes[i].to_company==handshakes[j].to_company && hsdate==jdate );
                          //console.log("---------");
                          if(i!=j && handshakes[i].to_company==handshakes[j].to_company && hsdate==jdate ){// && handshakes[i].date==handshakes[j].date
                            handshakes[j].unique = 0;
                          }
                        }
                      }
                      
                      
                      
                      

                    }
                    callback();
                  },




                  function(callback){
                    var client = new pg.Client(connectionString);
            
                    //console.log("email is: "+key);
                    client.connect();
                    client.query("SELECT * FROM company WHERE company_id=ANY($1)",  [companyids], function(err, result){
                        //console.log(err);
                        if(err){
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              //eventids.push(result.rows[i].event_id);
                              //console.log(result.rows[i].profile_fname);
                              companies.push({  'company_id':result.rows[i].company_id, 
                                                'name':result.rows[i].company_name, 
                                                'photo':result.rows[i].company_image
                                                });
                            }
                            
                        }

                      client.end();
                      callback();
                      
                    });
                  },


                  function(callback){
                    hslen = handshakes.length;
                    for(i=0; i<hslen; i++){
                      if(handshakes[i].unique){
                        hsfiltered.push({ 'handshake_id':handshakes[i].handshake_id, 
                                          'to_company':handshakes[i].to_company, 
                                          'to_user':handshakes[i].to_user,
                                          'date':handshakes[i].date,
                                          });
                      }
                    }

                    hslen = hsfiltered.length;
                    clen = companies.length;
                    for(i=0; i<hslen; i++){
                      for(j=0; j<clen; j++){
                        if(hsfiltered[i].to_company == companies[j].company_id){
                          hsfiltered[i].name = companies[j].name;
                          hsfiltered[i].photo = companies[j].photo;
                        }
                      }
                    }


                    callback();
                  },




                  /*function(callback){
                    var client = new pg.Client(connectionString);
                    
                    //console.log("email is: "+key);
                    client.connect();
                    client.query("SELECT users.*, profile.* FROM users INNER JOIN profile ON users.u_id = profile.profile_user WHERE profile.profile_user = ANY($1)", [userids], function(err, result){
                        //console.log(err);
                        if(err){
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              //eventids.push(result.rows[i].event_id);
                              //console.log(result.rows[i].profile_fname);
                              users.push({  'user_id':result.rows[i].profile_user, 
                                                'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                                'photo':result.rows[i].profile_photo,
                                                'email':result.rows[i].email,
                                                'saidhello': 0,
                                                'handshake_id':hsids[result.rows[i].profile_user]
                                                });
                            }
                            
                        }

                        client.end();
                        callback();
                      
                    });
                  },*/
                  /*function(callback){
                    reslen = users.length;
                    for(i=0; i<reslen; i++){
                      users[i].handshake_id = h
                    }

                  },*/
                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "",
                            "data" : hsfiltered
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
          
        }
        
    });
  },


  getHandshake: function(req, res) {

    var handshake_id = req.params.id || '';

    var userids = new Array;
    var users = new Array;
    var hsids = new Array;

    var date;
    var handshake;

    if (handshake_id == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Handshake ID is missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){
          
          async.series([
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM handshake WHERE handshake_id=$1", [handshake_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              userids.push(result.rows[i].to_user);
                              hsids[result.rows[i].to_user] = result.rows[i].handshake_id;

                              date = new Date(result.rows[i].date);

                              handshake = [{    
                                    'handshake_id':result.rows[i].handshake_id, 
                                    'recipient_type':result.rows[i].recipient_type, 
                                    'to_company':result.rows[i].to_company,
                                    'to_user':result.rows[i].to_user,
                                    'note': result.rows[i].notes,
                                    'action' : result.rows[i].action,
                                    'date' : days[date.getDay()] + ', ' + months[date.getMonth()] + ' ' + date.getDate() + ',' +  date.getFullYear(),
                                    'users': [],
                                    'company_name':'',
                                    'company_photo': ''
                                    }];

                              userids.push(handshake[0].to_user);

                            }
                        }

                      client.end();
                      done();
                    });
                  },

                  function(done){

                    if(handshake[0].recipient_type == 1){
                      console.log(handshake[0].to_company, date);
                      var client = new pg.Client(connectionString);
                        client.connect();
                        client.query("SELECT * FROM handshake WHERE to_company=$1 AND date=$2", [handshake[0].to_company, date], function(err, result){
                            //console.log(err);
                            if(err){
                              console.log(err);
                              return ;
                            }
                            //if no rows were returned from query, then new user
                            if (result.rows.length > 0){
                                //console.log(result.rows.length);
                                reslen = result.rows.length;
                                //console.log(result.rows);
                                for(i=0; i<reslen; i++){
                                  userids.push(result.rows[i].to_user);
                                }
                            }
                          client.end();
                          done();
                        });
                      }else{
                        done();
                      }
                  },

                  function(done){
                    //if(handshake[0].recipient_type == 2){
                        //userid = handshake[0].to_user;
                        thisusers = new Array;
                        var client = new pg.Client(connectionString);
  
                        //console.log("email is: "+key);
                        client.connect();
                        client.query("SELECT users.*, profile.* FROM users INNER JOIN profile ON users.u_id = profile.profile_user WHERE profile.profile_user = ANY($1)", [userids], function(err, result){
                            //console.log(err);
                            if(err){
                                return ;
                            }

                            //if no rows were returned from query, then new user
                            if (result.rows.length > 0){
                                //console.log(result.rows.length);
                                reslen = result.rows.length;
                                
                                //console.log(result.rows);
                                for(i=0; i<reslen; i++){
                                  //eventids.push(result.rows[i].event_id);
                                  //console.log(result.rows[i].profile_fname);
                                  handshake[0].users.push({    
                                                  'user_id':result.rows[i].profile_user, 
                                                  'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                                  'photo':result.rows[i].profile_photo
                                                  });


                                }
                                
                            }
                            

                          client.end();
                          done();
                          
                        });
                  

                    

                  },

                  function(callback){

                    companyid = handshake[0].to_company;
                    //console.log("companyid: "+handshake[0].to_company);

                    var client = new pg.Client(connectionString);
            
                    //console.log("email is: "+key);
                    client.connect();
                    client.query("SELECT * FROM company WHERE company_id=$1",  [companyid], function(err, result){
                        //console.log(err);
                        if(err){
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              //eventids.push(result.rows[i].event_id);
                              //console.log(result.rows[i].profile_fname);

                              handshake[0].company_name = result.rows[i].company_name;
                              handshake[0].company_photo = result.rows[i].company_image;
                              
                            }
                            
                        }

                      client.end();
                      callback();
                      
                    });
                  },


                 
                  /*function(callback){
                    reslen = users.length;
                    for(i=0; i<reslen; i++){
                      users[i].handshake_id = h
                    }

                  },*/
                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "",
                            "data" : handshake
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
          
        }
        
    });
  },


  checkIfHandshakedUser: function(req, res) {

    var status = 0;

    var event_id = req.params.id || '';
    var user_id = req.params.userid || '';

    var userids = new Array;
    var users = new Array;
    var hsids = new Array;

    if (event_id == '' || user_id == '') {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID and/or User ID are missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){
          
          async.series([
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM handshake WHERE event_id=$1 AND from_user=$2 AND to_user=$3 AND recipient_type=2", [event_id, user.u_id, user_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            status = result.rows[0].handshake_id;
                        }

                      client.end();
                      done();
                    });
                  },

                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "",
                            "data" : status
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
          
        }
        
    });
  },

  checkIfHandshakedCompany: function(req, res) {

    var status = 0;

    var event_id = req.params.id || '';
    var company_id = req.params.companyid || '';

    var userids = new Array;
    var users = new Array;
    var hsids = new Array;

    if (event_id == '' || company_id == '') {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID and/or Company ID are missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    User.findOne( username , function(err, user) {
        if(user){
          
          async.series([
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM handshake WHERE event_id=$1 AND from_user=$2 AND to_company=$3 AND recipient_type=1", [event_id, user.u_id, company_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            status = result.rows[0].handshake_id;
                        }

                      client.end();
                      done();
                    });
                  },

                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "",
                            "data" : status
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
          
        }
        
    });
  },




  getOne: function(req, res) {
    var id = req.params.id;
    var user = data[0]; // Spoof a DB call
    res.json(user);
  },

  create: function(req, res) {
    var newuser = req.body;
    data.push(newuser); // Spoof a DB call
    res.json(newuser);
  },

  update: function(req, res) {
    var updateuser = req.body;
    var id = req.params.id;
    data[id] = updateuser // Spoof a DB call
    res.json(updateuser);
  },

  delete: function(req, res) {
    var id = req.params.id;
    data.splice(id, 1) // Spoof a DB call
    res.json(true);
  },

  uploadFile : function(req, res) {
    // We are able to access req.files.file thanks to 
    // the multiparty middleware
    var file = req.files.file;
    console.log(file.name);
    console.log(file.type);
  }
};


/*users.prototype.uploadFile = function(req, res) {
    // We are able to access req.files.file thanks to 
    // the multiparty middleware
    var file = req.files.file;
    console.log(file.name);
    console.log(file.type);
}*/


var data = [{
  name: 'user 1',
  id: '1'
}, {
  name: 'user 2',
  id: '2'
}, {
  name: 'user 3',
  id: '3'
}];

module.exports = users;
