

var g = require('ger')
var esm = new g.MemESM()
var ger = new g.GER(esm);

var pg = require('pg');
var connectionString = require('../config/database');

var imagesurl = require('../config/variables');

var async = require('async');

var User = require('../models/user');


var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

var events = {




  getSuggested:  function(req, res, next) {

    var industryfound = 0;
    var industryid = 0;

    var eventids = [];
    var events = new Array;


    var topics = [];
    var lutopics = [];
    var topicbranches = [];
    

    var analyzed = new Array;



    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){

          async.series([
              //Load user to get `userId` first
              function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("SELECT * from user_linkedin where user_id=$1", [user_id], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }
                    
                    text1 = result.rows[0].linkedin_headline;
                    text2 = result.rows[0].linkedin_industry;
                    //text3 = result.rows[0].linkedin_summary;

                    analyzed1 = extractWords(text1);
                    analyzed2 = extractWords(text2);

                    //console.log(analyzed[0].text, analyzed[1].text, analyzed[2].text);
                    console.log(text2);

                    client.end();
                    callback();
                });
              },
              function(callback) {

                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query('SELECT * FROM linkedin_industry WHERE industry_name=$1', [text2], function(err, result){
                                if(err){
                                    return ;
                                }
                                if (result.rows.length > 0){
                                  //console.log(result.rows.length);
                                  industryfound = 1;
                                  industryid = result.rows[0].industry_id;
                                  //console.log(industryid);
                                }

                                client.end();
                                callback();
                            });
                  
              },
              
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM event_industry WHERE industry_id=$1", [industryid], function(err, result){
                    //console.log(err);
                    if(err){
                      console.log(err);
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){
                          eventids.push(result.rows[i].event_id);
                        }
                  }

                  client.end();
                  callback();
                  
                });


        },
        function(callback) {
          //console.log(eventids);
          
          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id = ANY($1)", [eventids], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){
                          //eventids.push(result.rows[i].event_id);
                          //console.log(result.rows[i]);
                          events.push({
                            'event_id':result.rows[i].event_id, 
                            'event_name':result.rows[i].event_name, 
                            'event_thumb':imagesurl+result.rows[i].event_thumb, 
                            'event_location':result.rows[i].event_location, 
                            'event_start':result.rows[i].event_start, 
                            'event_end':result.rows[i].event_end});

                        }
                        
                      //res.json(events);
                      

                      //callback();
                  }

                  client.end();

                  res.status(200);
                      res.json({
                        "status": 200,
                        "message": "",
                        "data" : events
                      });
                });


          


        }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


        }
        
    });





   
},

  /*getSubscribed : function(req, res){
    eventids = new Array;
    events = new Array;

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){

          async.series([
              //Load user to get `userId` first
              function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("SELECT * from subscription where user_id=$1", [user_id], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }
                    if(result.rows.length > 0){
                       //console.log(result.rows);
                        reslen = result.rows.length;
                        
                        for(i=0; i<reslen; i++){
                          eventids.push(result.rows[i].event_id);
                          //console.log(result.rows[i].topic_branch);
                        }
                    }
                    //console.log(eventids);

                    client.end();
                    callback();
                });
              },
              function(callback) {
                //console.log(allEvents);
                
                var client = new pg.Client(connectionString);
        
                //console.log("email is: "+key);
                client.connect();
                client.query("SELECT * FROM events WHERE event_id = ANY($1)", [eventids], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }

                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){
                          //eventids.push(result.rows[i].event_id);
                          events.push({'event_id':result.rows[i].event_id, 'event_name':result.rows[i].event_name, 'event_thumb':imagesurl+result.rows[i].event_thumb, 'event_location':result.rows[i].event_location, 'event_start':result.rows[i].event_start, 'event_end':result.rows[i].event_end});
                        }

                  }

                  client.end();

                  res.status(200);
                  res.json({
                  "status": 200,
                  "message": "",
                  "data" : events
                  });
                  
                });

                


              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


        }
        
    });
  },*/

  getPlanned : function(req, res){
    eventids = new Array;
    events = new Array;

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){

          async.series([
              //Load user to get `userId` first
              function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("SELECT * from answer_three where user_id=$1", [user_id], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }
                    if(result.rows.length > 0){
                       //console.log(result.rows);
                        reslen = result.rows.length;
                        
                        for(i=0; i<reslen; i++){
                          eventids.push(result.rows[i].event_id);
                          //console.log(result.rows[i].topic_branch);
                        }
                    }
                    //console.log(eventids);

                    client.end();
                    callback();
                });
              },
              function(callback) {
                //console.log(allEvents);
                
                var client = new pg.Client(connectionString);
        
                //console.log("email is: "+key);
                client.connect();
                client.query("SELECT * FROM events WHERE event_id = ANY($1)", [eventids], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }

                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){
                          //eventids.push(result.rows[i].event_id);

                          var startdate = new Date(result.rows[i].event_start);
                          var enddate = new Date(result.rows[i].event_end);
                          
                          events.push({
                                        'event_id':result.rows[i].event_id, 
                                        'event_name':result.rows[i].event_name, 
                                        'event_thumb':imagesurl+result.rows[i].event_thumb, 
                                        'event_location':result.rows[i].event_location, 
                                        'event_start':months[startdate.getMonth()] + ' ' + (startdate.getDate()+1), 
                                        'event_end':months[enddate.getMonth()] + ' ' + (enddate.getDate()+1)
                                      });
                        }

                  }

                  client.end();

                  res.status(200);
                  res.json({
                  "status": 200,
                  "message": "",
                  "data" : events
                  });
                  
                });

                


              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


        }
        
    });
  },
  
  getAll: function(req, res) {


 /*   var nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport('smtps://info@monica.events:monica123@mail.monica.events');

// setup e-mail data with unicode symbols
var mailOptions = {
    from: '"Fred Foo 👥" <foo@blurdybloop.com>', // sender address
    to: 'bar@blurdybloop.com, baz@blurdybloop.com', // list of receivers
    subject: 'Hello ✔', // Subject line
    text: 'Hello world 🐴', // plaintext body
    html: '<b>Hello world 🐴</b>' // html body
};

// send mail with defined transport object
transporter.sendMail(mailOptions, function(error, info){
    if(error){
        return console.log(error);
    }
    console.log('Message sent: ' + info.response);
});
*/





    var allEvents = new Array;

    var from = req.params.skip || '';

    async.series([
        //Load user to get `userId` first
        function(callback) {
          //console.log(topicbranches);

          //console.log(from, to);

          if(from==''){
            from = 0;
            //console.log(from, to);
          }

          to = 10;

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events ORDER BY event_id OFFSET $1 LIMIT $2", [from, to], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){

                          

                          var startdate = new Date(result.rows[i].event_start);
                          var enddate = new Date(result.rows[i].event_end);

                          allEvents.push({
                                          'event_id':result.rows[i].event_id, 
                                          'event_name':result.rows[i].event_name, 
                                          'event_thumb':imagesurl+result.rows[i].event_thumb, 
                                          'event_location':result.rows[i].event_location, 
                                          'event_start':months[startdate.getMonth()] + ' ' + (startdate.getDate()+1), 
                                          'event_end':months[enddate.getMonth()] + ' ' + (enddate.getDate()+1), 
                                          'event_day': days[startdate.getDay()] + ', ' + months[startdate.getMonth()] + ' ' + startdate.getDate() + ',' +  startdate.getFullYear(),
                                          'sortdate' : result.rows[i].event_start
                                        });
                          //console.log(result.rows[i].topic_branch);
                        }
                        
                        
                  }

                  client.end();
                  callback();
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(allEvents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : allEvents
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },

  getSearched : function(req, res){
    eventids = new Array;
    events = new Array;

    var searchstr = req.body.searchstr || '';
    var search_sec = req.body.search_sec || ''; //all, my, suggested


    //console.log(username);

          async.series([
              //Load user to get `userId` first
              function(callback) {
                //console.log(allEvents);
                
                if(search_sec == 'my'){
                  var token = req.headers['x-access-token'];
                  var user = new User;
                  username = user.getFromToken(token);
                  
                  User.findOne( username , function(err, user) {
                      if(user){
                          async.series([
                              function(done){
                                //console.log(user);
                                var client = new pg.Client(connectionString);
                                client.connect();
                                client.query("SELECT * FROM answer_three WHERE user_id=$1", [user.u_id], function(err, result){
                                    //console.log(err);
                                    if(err){
                                      console.log(err);
                                        return ;
                                    }

                                    //if no rows were returned from query, then new user
                                    if (result.rows.length > 0){
                                        //console.log(result.rows.length);
                                        reslen = result.rows.length;
                                        
                                        //console.log(result.rows);
                                        for(i=0; i<reslen; i++){
                                          //eventids.push(result.rows[i].event_id);
                                          eventids.push(result.rows[i].event_id);
                                        }

                                  }

                                  client.end();
                                  done();
                                });
                              },
                              function(done){
                                var client = new pg.Client(connectionString);
        
                                //console.log("email is: "+key);
                                //console.log(searchstr);
                                msearch = '%'+searchstr.toLowerCase()+'%';
                                client.connect();
                                client.query("SELECT * FROM events WHERE LOWER(event_name) LIKE $1 AND event_id=ANY($2)", [msearch, eventids], function(err, result){
                                    //console.log(err);
                                    if(err){
                                      console.log(err);
                                        return ;
                                    }

                                    //if no rows were returned from query, then new user
                                    if (result.rows.length > 0){
                                        //console.log(result.rows.length);
                                        reslen = result.rows.length;
                                        
                                        //console.log(result.rows);
                                        for(i=0; i<reslen; i++){
                                          //eventids.push(result.rows[i].event_id);
                                          events.push({'event_id':result.rows[i].event_id, 'event_name':result.rows[i].event_name, 'event_thumb':imagesurl+result.rows[i].event_thumb, 'event_location':result.rows[i].event_location, 'event_start':result.rows[i].event_start, 'event_end':result.rows[i].event_end});
                                        }

                                  }

                                  client.end();

                                  callback();
                                  
                                });
                              }
                            ],
                             function(err) { //This function gets called after the two tasks have called their "task callbacks"
                                if (err) return next(err);
                                //Here locals will be populated with `user` and `posts`
                                //Just like in the previous example
                                //res.render('user-profile', locals);
                            }
                          );
                        
                      }
                      
                  });

                }else{
                  callback();
                }

   

              },
              function(callback) {
                //console.log(allEvents);

                if(search_sec == 'all'){
                
                var client = new pg.Client(connectionString);
        
                //console.log("email is: "+key);
                //console.log(searchstr);
                msearch = '%'+searchstr.toLowerCase()+'%';
                client.connect();
                client.query("SELECT * FROM events WHERE LOWER(event_name) LIKE $1", [msearch], function(err, result){
                    //console.log(err);
                    if(err){
                      console.log(err);
                        return ;
                    }

                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){
                          //eventids.push(result.rows[i].event_id);
                          events.push({'event_id':result.rows[i].event_id, 'event_name':result.rows[i].event_name, 'event_thumb':imagesurl+result.rows[i].event_thumb, 'event_location':result.rows[i].event_location, 'event_start':result.rows[i].event_start, 'event_end':result.rows[i].event_end});
                        }

                  }

                  client.end();
                  callback();
                  
                  
                });
              }else{
                callback();
              }

                


              },
              function(callback){
                  res.status(200);
                  res.json({
                  "status": 200,
                  "message": "",
                  "data" : events
                  });
              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


        
  },

  getOne: function(req, res) {
    var id = req.params.id;
    var product; // Spoof a DB call

    var exhibitorids = new Array;
    var exhibitorloc = new Array;
    var exhibitors = new Array;

    var subevents = new Array;
    var subeventscat = new Array;
    var sortsubevents = new Array;

    var speakerids = new Array;
    var speakers = new Array;

    var talks = new Array;
    var workshops = new Array;

    async.series([
        //Load user to get `userId` first
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM event_company WHERE event_id=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          console.log(result.rows.length);
                          reslen = result.rows.length;
                        
                          for(i=0; i<reslen; i++){
                            exhibitorids.push(result.rows[i].company_id);
                            exhibitorloc[result.rows[i].company_id] = result.rows[i].company_location;
                            //console.log(result.rows[i].topic_branch);
                          }

                      }
                      client.end();
                      callback();
                  });
        },
        function(callback) {
                //console.log(allEvents);
          
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM company WHERE company_id = ANY($1)", [exhibitorids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    exhibitors.push({ 'company_id':result.rows[i].company_id, 
                                      'company_name':result.rows[i].company_name, 
                                      'company_image':result.rows[i].company_image, 
                                      'company_location':exhibitorloc[result.rows[i].company_id]
                                      });
                  }
                  
              }
              client.end();
              callback();
            
            
          });

          


        },
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM subevents WHERE subevent_event=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                        
                          for(i=0; i<reslen; i++){
                            subtype = ["","talk","workshop"];
                            if(typeof result.rows[i].subevent_type === undefined){
                              subevent_type = "";
                            }else{
                              subevent_type = subtype[result.rows[i].subevent_type];
                            }
                            

                            

                            var startdate = new Date(result.rows[i].subevent_start);
                            var enddate = new Date(result.rows[i].subevent_end);

                            subeventid = result.rows[i].subevent_id;
                            subeventuserid = result.rows[i].subevent_user;
                            speakerids[subeventid] = subeventuserid;

                            subevents.push({ 
                                      'subevent_id':subeventid, 
                                      'subevent_name':result.rows[i].subevent_name, 
                                      'subevent_image':result.rows[i].subevent_image, 
                                      'subevent_description':result.rows[i].subevent_description,
                                      'subevent_type':subevent_type,
                                      'subevent_provider_id':subeventuserid,
                                      'subevent_provider':"Test Test",
                                      'subevent_start':addZero(startdate.getHours()) + ':' + addZero(startdate.getMinutes()), 
                                      'subevent_end':addZero(enddate.getHours()) + ':' + addZero(enddate.getMinutes()), 
                                      'subevent_day': days[startdate.getDay()] + ', ' + months[startdate.getMonth()] + ' ' + startdate.getDate() + ',' +  startdate.getFullYear(),
                                      'sortdate' : result.rows[i].subevent_start
                                      });
                            
                          }

                        
                      }
                      client.end();
                      callback();
                  });
        },
        
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM profile WHERE profile_user = ANY($1)", [speakerids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    speakers.push({   'user_id':result.rows[i].profile_user, 
                                      'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                      'photo':result.rows[i].profile_photo
                                      });
                  }
                  
              }
              client.end();
              callback();
            
          });
        },
        function(callback){
            function compare(a,b) {
              if (a.sortdate < b.sortdate)
                return -1;
              if (a.sortdate > b.sortdate)
                return 1;
              return 0;
            }

            subevents.sort(compare);

            reslen = subevents.length;
            spelen = speakers.length;

            var groups = [];
            date = '';
            date_label = 'Day ';
            date_count = 1;


            for(i=0; i<reslen; i++){
              seid = subevents[i].subevent_id;
              for(j=0; j<spelen; j++){
                if(speakers[j].user_id == speakerids[seid]){
                  subevents[i].subevent_provider = speakers[j].name;
                }           
              }

              if(subevents[i].subevent_type == 'workshop'){
                workshops.push(subevents[i]);
              }

              if(subevents[i].subevent_type == 'talk'){
                talks.push(subevents[i]);
              }


               prevdate = date;
                var item = subevents[i];
                newdate = item.subevent_day.toString();
                //console.log(newdate);
                date = newdate;

                if(prevdate == newdate){
                    

                }else if(prevdate != ""){
                  //sortsubevents.push([{"date":prevdate, "subevents":groups}]);
                  sortsubevents.push([{"date":prevdate, "date_label":date_label+date_count, "subevents":groups}]);
                  date_count++;
                  groups = [];
                }
                

                groups.push({
                      subevent_id: item.subevent_id,
                      subevent_name: item.subevent_name,
                      subevent_image: item.subevent_image,
                      subevent_description: item.subevent_description,
                      subevent_type: item.subevent_type,
                      subevent_provider_id: item.subevent_provider_id,
                      subevent_provider: item.subevent_provider,
                      subevent_start: item.subevent_start,
                      subevent_end: item.subevent_end
                  });

                

                if(i==(reslen-1)){
                  //sortsubevents.push([{"date":prevdate, "subevents":groups}]);
                  sortsubevents.push([{"date":prevdate, "date_label":date_label+date_count, "subevents":groups}]);
                  date_count++;
                }


            }

            //console.log(sortsubevents);
            
            callback();


        },
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);

                        

                        i = 0;

                        var startdate = new Date(result.rows[i].event_start);
                        var enddate = new Date(result.rows[i].event_end);
                        
                        product = {
                                    'event_id':result.rows[i].event_id, 
                                    'event_name':result.rows[i].event_name, 
                                    'event_thumb':imagesurl+result.rows[i].event_image, 
                                    'event_location':result.rows[i].event_location, 
                                    'event_start':months[startdate.getMonth()] + ' ' + startdate.getDate(), 
                                    'event_end':months[enddate.getMonth()] + ' ' + enddate.getDate(), 
                                    'event_day': days[startdate.getDay()] + ', ' + months[startdate.getMonth()] + ' ' + startdate.getDate() + ',' +  startdate.getFullYear(),
                                    'sortdate' : result.rows[i].event_start,
                                    'event_description':result.rows[i].event_description,
                                    'exhibitors' : [{
                                                      "count":exhibitors.length,
                                                      "data":exhibitors
                                                    }],
                                    'agenda' : [{
                                                      "count":sortsubevents.length,
                                                      "data":sortsubevents
                                                    }],
                                    'speakers' : [{
                                                      "count":speakers.length,
                                                      "data":speakers
                                                    }],
                                    'talks' : [{
                                                      "count":talks.length,
                                                      "data":talks
                                                    }],
                                    'workshops' : [{
                                                      "count":workshops.length,
                                                      "data":workshops
                                                    }],

                                  };
                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : product
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },


  getAgenda: function(req, res) {
    var id = req.params.id;
    var product; // Spoof a DB call


    var subevents = new Array;
    var subeventscat = new Array;
    var sortsubevents = new Array;

    var speakerids = new Array;
    var speakers = new Array;

    var talks = new Array;
    var workshops = new Array;

    async.series([
        //Load user to get `userId` first
        
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM subevents WHERE subevent_event=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                        
                          for(i=0; i<reslen; i++){
                            subtype = ["","talk","workshop"];
                            if(typeof result.rows[i].subevent_type === undefined){
                              subevent_type = "";
                            }else{
                              subevent_type = subtype[result.rows[i].subevent_type];
                            }
                            
                            var startdate = new Date(result.rows[i].subevent_start);
                            var enddate = new Date(result.rows[i].subevent_end);

                            subeventid = result.rows[i].subevent_id;
                            subeventuserid = result.rows[i].subevent_user;
                            speakerids[subeventid] = subeventuserid;

                            subevents.push({ 
                                      'subevent_id':subeventid, 
                                      'subevent_name':result.rows[i].subevent_name, 
                                      'subevent_image':result.rows[i].subevent_image, 
                                      'subevent_description':result.rows[i].subevent_description,
                                      'subevent_type':subevent_type,
                                      'subevent_provider_id':subeventuserid,
                                      'subevent_provider':"Test Test",
                                      'subevent_start':addZero(startdate.getHours()) + ':' + addZero(startdate.getMinutes()), 
                                      'subevent_end':addZero(enddate.getHours()) + ':' + addZero(enddate.getMinutes()), 
                                      'subevent_day': days[startdate.getDay()] + ', ' + months[startdate.getMonth()] + ' ' + startdate.getDate() + ',' +  startdate.getFullYear(),
                                      'sortdate' : result.rows[i].subevent_start
                                      });

                          }

                        
                      }

                      client.end();
                      callback();
                  });
        },
        
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM profile WHERE profile_user = ANY($1)", [speakerids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    speakers.push({   'user_id':result.rows[i].profile_user, 
                                      'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                      'photo':result.rows[i].profile_photo
                                      });
                  }
                  
              }

              client.end();
              callback();
            
          });
        },
        function(callback){
            function compare(a,b) {
              if (a.sortdate < b.sortdate)
                return -1;
              if (a.sortdate > b.sortdate)
                return 1;
              return 0;
            }

            subevents.sort(compare);

            reslen = subevents.length;
            spelen = speakers.length;
            ndate = '';
            numdays = 0;


            var groups = [];
            date = '';

            date_label = 'Day ';
            date_count = 1;

            for(i=0; i<reslen; i++){
              seid = subevents[i].subevent_id;
              for(j=0; j<spelen; j++){
                if(speakers[j].user_id == speakerids[seid]){
                  subevents[i].subevent_provider = speakers[j].name;
                }
              }

              if(subevents[i].subevent_type == 'workshop'){
                workshops.push(subevents[i]);
              }

              if(subevents[i].subevent_type == 'talk'){
                talks.push(subevents[i]);
              }



                prevdate = date;
                var item = subevents[i];
                newdate = item.subevent_day.toString();
                //console.log(newdate);
                date = newdate;

                if(prevdate == newdate){
                    

                }else if(prevdate != ""){
                  sortsubevents.push([{"date":prevdate, "date_label":date_label+date_count, "subevents":groups}]);
                  date_count++;
                  groups = [];
                }


                groups.push({
                      subevent_id: item.subevent_id,
                      subevent_name: item.subevent_name,
                      subevent_image: item.subevent_image,
                      subevent_description: item.subevent_description,
                      subevent_type: item.subevent_type,
                      subevent_provider_id: item.subevent_provider_id,
                      subevent_provider: item.subevent_provider,
                      subevent_start: item.subevent_start,
                      subevent_end: item.subevent_end
                  });

                

                if(i==(reslen-1)){
                  sortsubevents.push([{"date":prevdate, "date_label":date_label+date_count, "subevents":groups}]);
                  date_count++;
                }


            }





            //console.log(sortsubevents);
             callback();

        },

        function(callback) {
          //console.log(topicbranches);
          //console.log(1);
          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);

                        

                        
                        product = [{
                                    "count":sortsubevents.length,
                                    "data":sortsubevents
                                  }];

                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          //console.log(sortsubevents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : product
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },


  getPrivateAgenda: function(req, res) {
    var id = req.params.id;
    var product; // Spoof a DB call


    var subevents = new Array;
    var mysubevents = new Array;
    var subeventscat = new Array;
    var sortsubevents = new Array;

    var speakerids = new Array;
    var speakers = new Array;

    var talks = new Array;
    var workshops = new Array;




    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){
            async.series([
        //Load user to get `userId` first
        
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM subevents WHERE subevent_event=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                        
                          for(i=0; i<reslen; i++){
                            subtype = ["","talk","workshop"];
                            if(typeof result.rows[i].subevent_type === undefined){
                              subevent_type = "";
                            }else{
                              subevent_type = subtype[result.rows[i].subevent_type];
                            }
                            
                            var startdate = new Date(result.rows[i].subevent_start);
                            var enddate = new Date(result.rows[i].subevent_end);

                            subeventid = result.rows[i].subevent_id;
                            subeventuserid = result.rows[i].subevent_user;
                            speakerids[subeventid] = subeventuserid;

                            subevents.push({ 
                                      'subevent_id':subeventid, 
                                      'subevent_name':result.rows[i].subevent_name, 
                                      'subevent_image':result.rows[i].subevent_image, 
                                      'subevent_description':result.rows[i].subevent_description,
                                      'subevent_type':subevent_type,
                                      'subevent_provider_id':subeventuserid,
                                      'subevent_provider':"Test Test",
                                      'subevent_start':addZero(startdate.getHours()) + ':' + addZero(startdate.getMinutes()), 
                                      'subevent_end':addZero(enddate.getHours()) + ':' + addZero(enddate.getMinutes()), 
                                      'subevent_day': days[startdate.getDay()] + ', ' + months[startdate.getMonth()] + ' ' + startdate.getDate() + ',' +  startdate.getFullYear(),
                                      'sortdate' : result.rows[i].subevent_start,
                                      'isSelected': false
                                      });

                          }

                        
                      }

                      client.end();
                      callback();
                  });
        },
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM mysubevents WHERE event_id=$1 AND user_id=$2", [id, user.u_id], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(1);
                   reslen = result.rows.length;
                      
                  for(i=0; i<reslen; i++){
                    mysubevents.push(result.rows[i].subevent_id);
                  }

              }

              client.end();
              callback();
              
            
          });
          
          //console.log(subevents);
          //subevents[i].isSelected = true;
          
        },
        function(callback){
          sublen = subevents.length;
          //console.log(sublen);
          //console.log(mysubevents);
          for(i=0; i<sublen; i++){
            found = 0;
            subeventid = subevents[i].subevent_id;
            if(mysubevents.indexOf(subeventid) > -1){
              found=1;
              subevents[i].isSelected = true;
            }
            //console.log(found);
          }

          
          callback();
        },
        
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM profile WHERE profile_user = ANY($1)", [speakerids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    speakers.push({   'user_id':result.rows[i].profile_user, 
                                      'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                      'photo':result.rows[i].profile_photo
                                      });
                  }
                  
              }

              client.end();
              callback();
            
          });
        },
        function(callback){
            function compare(a,b) {
              if (a.sortdate < b.sortdate)
                return -1;
              if (a.sortdate > b.sortdate)
                return 1;
              return 0;
            }

            subevents.sort(compare);

            reslen = subevents.length;
            spelen = speakers.length;
            ndate = '';
            numdays = 0;


            var groups = [];
            date = '';

            date_label = 'Day ';
            date_count = 1;

            for(i=0; i<reslen; i++){
              seid = subevents[i].subevent_id;
              for(j=0; j<spelen; j++){
                if(speakers[j].user_id == speakerids[seid]){
                  subevents[i].subevent_provider = speakers[j].name;
                }else if(subevents[i].subevent_type == 0){
                  subevents[i].subevent_provider = "";
                }
              }

              if(subevents[i].subevent_type == 'workshop'){
                workshops.push(subevents[i]);
              }

              if(subevents[i].subevent_type == 'talk'){
                talks.push(subevents[i]);
              }



                prevdate = date;
                var item = subevents[i];
                newdate = item.subevent_day.toString();
                //console.log(newdate);
                date = newdate;

                if(prevdate == newdate){
                    

                }else if(prevdate != ""){
                  //sortsubevents.push([{"date":prevdate, "subevents":groups}]);
                  sortsubevents.push([{"date":prevdate, "date_label":date_label+date_count, "subevents":groups}]);
                  date_count++;
                  groups = [];
                }


                groups.push({
                      subevent_id: item.subevent_id,
                      subevent_name: item.subevent_name,
                      subevent_image: item.subevent_image,
                      subevent_description: item.subevent_description,
                      subevent_type: item.subevent_type,
                      subevent_provider_id: item.subevent_provider_id,
                      subevent_provider: item.subevent_provider,
                      subevent_start: item.subevent_start,
                      subevent_end: item.subevent_end,
                      isSelected: item.isSelected
                  });

                

                if(i==(reslen-1)){
                  //sortsubevents.push([{"date":prevdate, "subevents":groups}]);
                  sortsubevents.push([{"date":prevdate, "date_label":date_label+date_count, "subevents":groups}]);
                  date_count++;
                }

            }

            //console.log(sortsubevents);
             callback();

        },

        function(callback) {
          //console.log(topicbranches);
          //console.log(1);
          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);

                        

                        
                        product = [{
                                    "count":sortsubevents.length,
                                    "data":sortsubevents
                                  }];

                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          //console.log(sortsubevents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : product
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });
        
        }
    }
  );

    

    
  },


   getMyAgenda: function(req, res) {
    var id = req.params.id;
    var product; // Spoof a DB call
    
    var subeventids = new Array;
    var subevents = new Array;
    var subeventscat = new Array;
    var sortsubevents = new Array;
    var sortfinalsub = new Array;

    var speakerids = new Array;
    var speakers = new Array;

    var talks = new Array;
    var workshops = new Array;

    var industryfound = 0;
    var industryid = 0;

    var companyids = new Array;
    var exhibitors = new Array;

    var subeventdates = new Array;
    var subeventdatesa = new Array;

    var topicids = new Array;
    var topics = new Array;
    var userids = new Array;
    var users = new Array;
    var waveduserids = new Array;
    

    
    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){
          user_id = user.u_id;

    async.series([
        
        function(callback) {
                      subeventids = [];
                      //console.log(user_id);
                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query('SELECT * FROM mysubevents WHERE event_id=$1 AND user_id=$2', [id, user_id], function(err, result){
                                if(err){
                                    return ;
                                }
                                if (result.rows.length > 0){
                                  //console.log(result.rows.length);
                                  reslen = result.rows.length;
                  
                                  //console.log(result.rows);
                                  for(i=0; i<reslen; i++){
                                    subeventids.push(result.rows[i].subevent_id);
                                  }
                                  
                                }
                                //console.log(result.rows.length);
                                //console.log(subeventids);

                                client.end();
                                callback();
                            });
                  
              },
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM subevents WHERE subevent_id=ANY($1)", [subeventids], function(err, result){// WHERE subevent_id=ANY($1)[subeventids]
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                          
                          for(i=0; i<reslen; i++){
                            subtype = ["","talk","workshop"];
                            if(typeof result.rows[i].subevent_type === undefined){
                              subevent_type = "";
                            }else{
                              subevent_type = subtype[result.rows[i].subevent_type];
                            }


                            
                            var nstartdate = '';
                            var nenddate = '';
                            //console.log("["+i+"]: "+result.rows[i].subevent_start);
                            if(result.rows[i].subevent_start !== null){
                              startdate = new Date(result.rows[i].subevent_start);
                              nstartdate = addZero(startdate.getHours()) + ':' + addZero(startdate.getMinutes());
                            }
                            
                            if(result.rows[i].subevent_end !== null){
                              enddate = new Date(result.rows[i].subevent_end);
                              nenddate = addZero(enddate.getHours()) + ':' + addZero(enddate.getMinutes());
                            }

                            subeventid = result.rows[i].subevent_id;
                            subeventuserid = result.rows[i].subevent_user;
                            speakerids[subeventid] = subeventuserid;

                            subevents.push({
                                      'subevent_id':subeventid, 
                                      'subevent_name':result.rows[i].subevent_name, 
                                      'subevent_image':result.rows[i].subevent_image, 
                                      'subevent_description':result.rows[i].subevent_description,
                                      'subevent_type':subevent_type,
                                      'subevent_provider_id':subeventuserid,
                                      'subevent_provider':"Test Test",
                                      'subevent_start':nstartdate, 
                                      'subevent_end':nenddate, 
                                      'subevent_day': days[startdate.getDay()] + ', ' + months[startdate.getMonth()] + ' ' + startdate.getDate() + ',' +  startdate.getFullYear(),
                                      'sortdate' : result.rows[i].subevent_start,
                                      'isSelected': true
                                      });
                            //console.log(result.rows[i]);
                            subeventdate = result.rows[i].subevent_start.toString();
                            if(subeventdates.indexOf(subeventdate) == -1){
                              subeventdates.push(subeventdate);
                              subeventdatesa.push(result.rows[i].subevent_start);
                            }
                          }
                          //console.log(subevents);
                          //console.log(subeventdates);

                        
                      }

                      client.end();
                      callback();
                  });
        },

        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM subevents WHERE subevent_event=$1 AND subevent_start=ANY($2)", [id, subeventdatesa], function(err, result){// WHERE subevent_id=ANY($1)[subeventids]
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                          
                          for(i=0; i<reslen; i++){
                            subtype = ["","talk","workshop"];
                            if(typeof result.rows[i].subevent_type === undefined){
                              subevent_type = "";
                            }else{
                              subevent_type = subtype[result.rows[i].subevent_type];
                            }

                            
                            var nstartdate = '';
                            var nenddate = '';
                            //console.log("["+i+"]: "+result.rows[i].subevent_start);
                            if(result.rows[i].subevent_start !== null){
                              startdate = new Date(result.rows[i].subevent_start);
                              nstartdate = addZero(startdate.getHours()) + ':' + addZero(startdate.getMinutes());
                            }
                            
                            if(result.rows[i].subevent_end !== null){
                              enddate = new Date(result.rows[i].subevent_end);
                              nenddate = addZero(enddate.getHours()) + ':' + addZero(enddate.getMinutes());
                            }

                            subeventid = result.rows[i].subevent_id;
                            subeventuserid = result.rows[i].subevent_user;
                            speakerids[subeventid] = subeventuserid;

                            subevents.push({
                                      'subevent_id':subeventid, 
                                      'subevent_name':result.rows[i].subevent_name, 
                                      'subevent_image':result.rows[i].subevent_image, 
                                      'subevent_description':result.rows[i].subevent_description,
                                      'subevent_type':subevent_type,
                                      'subevent_provider_id':subeventuserid,
                                      'subevent_provider':"Test Test",
                                      'subevent_start':nstartdate, 
                                      'subevent_end':nenddate, 
                                      'subevent_day': days[startdate.getDay()] + ', ' + months[startdate.getMonth()] + ' ' + startdate.getDate() + ',' +  startdate.getFullYear(),
                                      'sortdate' : result.rows[i].subevent_start,
                                      'isSelected': false
                                      });
                            //console.log(result.rows[i]);
                          }
                          //console.log(subevents);

                        
                      }

                      client.end();
                      callback();
                  });
        },

        
        function(callback) {
          //console.log(industryid);
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM myexhibitors WHERE event_id=$1 AND user_id=$2", [id, user_id], function(err, result){
                      //console.log(err);
                      if(err){
                          //console.log(err);
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                          
                          for(i=0; i<reslen; i++){
                           //console.log(result.rows[i]);
                           companyids.push(result.rows[i].company_id);
                          }
                      }
                      //console.log(companyids);
                      client.end();
                      callback();
                  });
        },
        function(callback) {
          //console.log(industryid);
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM company WHERE company_id=ANY($1)", [companyids], function(err, result){
                      //console.log(err);
                      if(err){
                          //console.log(err);
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                          
                          for(i=0; i<reslen; i++){
                           //console.log(result.rows[i]);
                           exhibitors.push({ 
                                      'company_id':result.rows[i].company_id, 
                                      'company_name':result.rows[i].company_name, 
                                      'company_image':result.rows[i].company_image
                                      });
                          }
                      }
                      //console.log(exhibitors);
                      client.end();
                      callback();
                  });
        },
        function(callback){
          var client = new pg.Client(connectionString);
          
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM profile WHERE profile_user = ANY($1)", [speakerids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    // consle.log.  quick question
                    //the client wants to know more details about the timeline, can you please tell me how much time each of these comments will take: client comments, business comments, and Jira bugs
                    //console.log(result.rows[i].profile_fname);
                    speakers.push({   'user_id':result.rows[i].profile_user, 
                                      'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                      'photo':result.rows[i].profile_photo
                                      });
                  }
                  
              }

              client.end();
              callback();
            
          });
        },

        //suggested attendees
        function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM answer_three WHERE user_id=$1 AND event_id=$2", [user.u_id, id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              topicids.push(result.rows[i].topic_subsubcat);
                            }

                      }

                      client.end();
                      done();
                    });
                  },
                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM linkedin_industry WHERE industry_id=ANY($1)", [topicids], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              topics.push(result.rows[i].industry_name);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                    
                  },
                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM user_linkedin WHERE linkedin_industry=ANY($1)", [topics], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              userids.push(result.rows[i].user_id);
                            }
                            //console.log(userids);
                      }

                      client.end();
                      done();
                    });
                    
                  },
                  function(done){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND to_user=ANY($2) AND from_user=$3", [id, userids, user.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              waveduserids.push(result.rows[i].to_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                    
                  },
                  function(callback){
                    var client = new pg.Client(connectionString);
            
                    //console.log("email is: "+key);
                    client.connect();
                    client.query("SELECT users.*, profile.* FROM users INNER JOIN profile ON users.u_id = profile.profile_user WHERE profile.profile_user = ANY($1)", [userids], function(err, result){
                        //console.log(err);
                        if(err){
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              //eventids.push(result.rows[i].event_id);
                              //console.log(result.rows[i].profile_fname);
                              users.push({  'user_id':result.rows[i].profile_user, 
                                                'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                                'photo':result.rows[i].profile_photo,
                                                'email':result.rows[i].email,
                                                'saidhello': false
                                                });
                            }
                            
                        }

                        client.end();
                        callback();
                      
                    });
                  },
                  function(callback) {
                    reslen = users.length;
                    console.log(waveduserids);
                    for(i=0; i<reslen; i++){
                      if(waveduserids.indexOf(users[i].user_id) != -1)
                      users[i].saidhello = true;
                    }
                    callback();
                  },
        //suggested attendees
        function(callback){

            function compareSelected(a,b) {
              if (a.isSelected == true || b.isSelected == false)
                return -1;
              if (a.isSelected == false || b.isSelected == true)
                return 1;
              return 0;
            }

            

            function compare(a,b) {
              if (a.sortdate < b.sortdate)
                return -1;
              if (a.sortdate > b.sortdate)
                return 1;
              return 0;
            }

            subevents.sort(compare);



            //console.log(subevents);

            reslen = subevents.length;
            spelen = speakers.length;
            ndate = '';
            numdays = 0;

            var timegroup = [];
            time = '';
            nexttime = '';
            newtime = '';
            newday = '';
            day = '';

            for(i=0; i<reslen; i++){



              seid = subevents[i].subevent_id;
              for(j=0; j<spelen; j++){
                if(speakers[j].user_id == speakerids[seid] ){
                  subevents[i].subevent_provider = speakers[j].name;
                }else if(subevents[i].subevent_type == 0){
                  subevents[i].subevent_provider = "";
                }
              }

              if(subevents[i].subevent_type == 'workshop'){
                workshops.push(subevents[i]);
              }

              if(subevents[i].subevent_type == 'talk'){
                talks.push(subevents[i]);
              }




              var item = subevents[i];

              oldtime = time;
              newtime = item.subevent_start.toString();
              time = newtime;


              oldday = day;
              newday = item.subevent_day.toString();
              day = newday;


              if(typeof subevents[i+1] === 'undefined') {
                nexttime = '';
              }else{
                nexttime = subevents[i+1].subevent_start;
              }


              //console.log(newday);
              if( nexttime == '' || (newtime != nexttime) ){
                timegroup.push({
                              subevent_id: item.subevent_id,
                              subevent_name: item.subevent_name,
                              subevent_image: item.subevent_image,
                              subevent_description: item.subevent_description,
                              subevent_type: item.subevent_type,
                              subevent_provider_id: item.subevent_provider_id,
                              subevent_provider: item.subevent_provider,
                              subevent_start: item.subevent_start,
                              subevent_end: item.subevent_end,
                              isSelected: item.isSelected
                          });

                sortsubevents.push({
                                    "time":newtime,
                                    "date":newday,
                                    "type":"subevents",
                                    "subevents":timegroup
                                  });

                timegroup.sort(compareSelected);

                timegroup = [];
              }else{
                timegroup.push({
                              subevent_id: item.subevent_id,
                              subevent_name: item.subevent_name,
                              subevent_image: item.subevent_image,
                              subevent_description: item.subevent_description,
                              subevent_type: item.subevent_type,
                              subevent_provider_id: item.subevent_provider_id,
                              subevent_provider: item.subevent_provider,
                              subevent_start: item.subevent_start,
                              subevent_end: item.subevent_end,
                              isSelected: item.isSelected
                          });
              }


              
            }
            


            //console.log(sortsubevents);

            sortsubevents.push({
                                    "time":newtime,
                                    "date":newday,
                                    "type":"exhibitors",
                                    "exhibitors":exhibitors
                                  });

            sortsubevents.push({
                                    "time":newtime,
                                    "date":newday,
                                    "type":"attendees",
                                    "attendees":users
                                  });


           


            //console.log(sortfinalsub);


            tglen = sortsubevents.length;
            daygroup = [];

            date = '';

            date_label = 'Day ';
            date_count = 1;

            for(i=0; i<tglen; i++){
              //console.log(sortsubevents[i].date);

              curdate = sortsubevents[i].date;
              if ( sortsubevents[i+1] !== undefined ) {
                nextdate = sortsubevents[i+1].date;
              }else{
                nextdate = '';
              }

              if(curdate != nextdate ){
                daygroup.push(sortsubevents[i]);
                sortfinalsub.push({"date":curdate, "date_label":date_label+date_count, "times":daygroup});
                date_count++;
                daygroup = [];
              }else{
                daygroup.push(sortsubevents[i]);
              }
              //console.log(curdate, nextdate);
              
            }
            

            //console.log(sortsubevents);
             callback();

        },

        function(callback) {
          //console.log(topicbranches);
          //console.log(1);
          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);

                        

                        
                        product = [{
                                    "count":sortfinalsub.length,
                                    "data":sortfinalsub
                                  }];

                      
                  }

                    client.end();
                    callback();
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          //console.log(sortsubevents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : product
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });
      }
    }
  );

    
  },


  swapSubevents: function(req, res) {
    var oldsub = req.body.oldsubevent_id;
    var newsub = req.body.newsubevent_id;
    var event_id;
    var product; // Spoof a DB call
    
   
    //console.log(oldsub, newsub);

    
    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){

    async.series([
        //Load user to get `userId` first
        function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("SELECT * from mysubevents WHERE user_id=$1 AND subevent_id=$2", [user_id, oldsub], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                        console.log(err);
                    }

                    //console.log(result.rows.length );

                    if(result.rows.length == 0){
                      res.status(200);
                      res.json({
                              "status": 200,
                              "message": "Subevent not found.",
                              "data" : []
                              });
                    }

                    //console.log(analyzed[0].text, analyzed[1].text, analyzed[2].text);
                    //console.log(text2);

                    client.end();
                    callback();
                });
              },
        function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("DELETE from mysubevents WHERE user_id=$1 AND subevent_id=$2", [user_id, oldsub], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                        //console.log(err);
                    }


                    //console.log(analyzed[0].text, analyzed[1].text, analyzed[2].text);
                    //console.log(text2);

                    client.end();
                    callback();
                });
              },
        function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("SELECT * from subevents WHERE subevent_id=$1", [newsub], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }

                    if(result.rows.length > 0){
                      event_id = result.rows[0].subevent_event;
                    }

                    //console.log(analyzed[0].text, analyzed[1].text, analyzed[2].text);
                    //console.log("eventid: "+event_id);

                    client.end();
                    callback();
                });
              },
        function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("INSERT INTO mysubevents(event_id,user_id,subevent_id) VALUES($1,$2,$3)", [event_id, user_id, newsub], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }

                    //console.log(analyzed[0].text, analyzed[1].text, analyzed[2].text);
                    //console.log(text2);

                    client.end();
                    callback();
                });
              },

              
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          //console.log(sortsubevents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "Subevent swapped successfully",
                  "data" : []
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });
      }
    }
  );

    
  },

addSubevent: function(req, res) {
    var subeventid = req.body.subevent_id || "";
    
    var event_id;
    var product; // Spoof a DB call
    
   if (subeventid == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Subevent ID is required",
        "data" : []
      });
      return;
    }
    

    
    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){

    async.series([
        //Load user to get `userId` first
        
        
        function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("SELECT * from subevents WHERE subevent_id=$1", [subeventid], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }

                    if(result.rows.length > 0){
                      event_id = result.rows[0].subevent_event;
                    }

                    //console.log(analyzed[0].text, analyzed[1].text, analyzed[2].text);
                    //console.log("eventid: "+event_id);

                    client.end();
                    callback();
                });
              },

        function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("INSERT INTO mysubevents(event_id,user_id,subevent_id) VALUES($1,$2,$3)", [event_id, user_id, subeventid], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }

                    //console.log(analyzed[0].text, analyzed[1].text, analyzed[2].text);
                    //console.log(text2);

                    client.end();
                    callback();
                });
              },

              
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          //console.log(sortsubevents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "Subevent added successfully",
                  "data" : []
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });
      }
    }
  );

    
  },

deleteSubevent: function(req, res) {
    var subeventid = req.body.subevent_id || "";
    
    var event_id;
    var product; // Spoof a DB call
    
   if (subeventid == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Subevent ID is required",
        "data" : []
      });
      return;
    }
    

    
    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){

    async.series([
        //Load user to get `userId` first
        
        
        function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("SELECT * from subevents WHERE subevent_id=$1", [subeventid], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }

                    if(result.rows.length > 0){
                      event_id = result.rows[0].subevent_event;
                    }

                    //console.log(analyzed[0].text, analyzed[1].text, analyzed[2].text);
                    //console.log("eventid: "+event_id);

                    client.end();
                    callback();
                });
              },
        
        function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("DELETE FROM mysubevents WHERE user_id=$1 AND subevent_id=$2", [user_id, subeventid], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }

                    //console.log(analyzed[0].text, analyzed[1].text, analyzed[2].text);
                    //console.log(text2);

                    client.end();
                    callback();
                });
              },

              
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          //console.log(sortsubevents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "Subevent deleted successfully",
                  "data" : []
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });
      }
    }
  );

    
  },

  addExhibitor: function(req, res) {
    var exhibitorid = req.body.exhibitor_id || "";
    var eventid = req.body.event_id || "";

    var product; // Spoof a DB call
    
   if (exhibitorid == '' || eventid == '') {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID and/or Exhibitor ID are required",
        "data" : []
      });
      return;
    }
    

    
    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){

    async.series([
        //Load user to get `userId` first
        


        function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("INSERT INTO myexhibitors(event_id,user_id,company_id) VALUES($1,$2,$3)", [eventid, user_id, exhibitorid], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }

                    //console.log(analyzed[0].text, analyzed[1].text, analyzed[2].text);
                    //console.log(text2);

                    client.end();
                    callback();
                });
              },

              
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          //console.log(sortsubevents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "Exhibitor added successfully",
                  "data" : []
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });
      }
    }
  );

    
  },

  deleteExhibitor: function(req, res) {
    var exhibitorid = req.body.exhibitor_id || "";
    var eventid = req.body.event_id || "";

    var product; // Spoof a DB call
    
   if (exhibitorid == '' || eventid == '') {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID and/or Exhibitor ID are required",
        "data" : []
      });
      return;
    }
    

    
    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){

    async.series([
        //Load user to get `userId` first
        
        
        function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("DELETE FROM myexhibitors WHERE event_id=$1 AND user_id=$2 AND company_id=$3", [eventid, user_id, exhibitorid], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }

                    //console.log(analyzed[0].text, analyzed[1].text, analyzed[2].text);
                    //console.log(text2);

                    client.end();
                    callback();
                });
              },

              
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          //console.log(sortsubevents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "Exhibitor deleted successfully",
                  "data" : []
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });
      }
    }
  );

    
  },


  getSpeakers: function(req, res) {
    var id = req.params.id;
    var product; // Spoof a DB call


    var subevents = new Array;
    var subeventscat = new Array;

    var speakerids = new Array;
    var speakers = new Array;

    var exhibitorids = new Array;
    var exhibitorloc = new Array;


    async.series([
        //Load user to get `userId` first
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM event_company WHERE event_id=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                        
                          for(i=0; i<reslen; i++){
                            exhibitorids.push(result.rows[i].company_id);
                            exhibitorloc[result.rows[i].company_id] = result.rows[i].company_location;
                            //console.log(result.rows[i].topic_branch);
                          }

                      }
                      client.end();
                      callback();
                  });
        },
        
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM subevents WHERE subevent_event=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                        
                          for(i=0; i<reslen; i++){
                            subtype = ["","talk","workshop"];
                            if(typeof result.rows[i].subevent_type === undefined){
                              subevent_type = "";
                            }else{
                              subevent_type = subtype[result.rows[i].subevent_type];
                            }
                            

                            

                            var startdate = new Date(result.rows[i].subevent_start);
                            var enddate = new Date(result.rows[i].subevent_end);

                            subeventid = result.rows[i].subevent_id;
                            subeventuserid = result.rows[i].subevent_user;
                            speakerids[subeventid] = subeventuserid;

                            
                            
                          }

                        
                      }

                      client.end();
                      callback();
                  });
        },
        
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM profile WHERE profile_user = ANY($1)", [speakerids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    speakers.push({   'user_id':result.rows[i].profile_user, 
                                      'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                      'photo':result.rows[i].profile_photo
                                      });
                  }
                  
              }

              client.end();
              callback();
            
          });
        },
        
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);

                        
                        product = [{
                                    "count":speakers.length,
                                    "data":speakers
                                  }];

                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : product
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },


  getAttendees: function(req, res) {
    var id = req.params.id;
    var product; // Spoof a DB call

    var attendeeids = new Array;
    var attendees = new Array;


    var subevents = new Array;
    var subeventscat = new Array;

    var speakerids = new Array;
    var speakers = new Array;

    var exhibitorids = new Array;
    var exhibitorloc = new Array;


    var waveduserids = new Array;
    var wavedforuserids = new Array;
    var accepteduserids = new Array;


    var userexist = 0;
    var guser;

    async.series([
        //Load user to get `userId` first
        
        function(callback){
          var token = req.headers['x-access-token'] || '';

          if (token != '' ) {
            var user = new User;
            username = user.getFromToken(token);
            
            User.findOne( username , function(err, user) {
                if(user){
                  guser = user;
                  userexist = 1;
                  //console.log("hey:");
                  //console.log(user);
                  
                }
                
            });
          }
          callback();
          
        },
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM subscription WHERE event_id=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                        
                          for(i=0; i<reslen; i++){
                            
                            attendeeids.push(result.rows[i].user_id);
                            
                          }

                        
                      }

                      client.end();
                      callback();
                  });
        },
        function(done){
          if(userexist){
            var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND to_user=ANY($2) AND from_user=$3", [id, attendeeids, guser.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              wavedforuserids.push(result.rows[i].to_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                  }else{
                    done();
                  }
                    
                    
        },

        function(done){
          if(userexist){
            var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND to_user=$3 AND from_user=ANY($2)", [id, attendeeids, guser.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              waveduserids.push(result.rows[i].from_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                  }else{
                    done();
                  }
                    
                    
        },

        function(done){
          if(userexist){
            var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND status=1 AND to_user=$3 AND from_user=ANY($2)", [id, attendeeids, guser.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              accepteduserids.push(result.rows[i].from_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                  }else{
                    done();
                  }       
        },
        function(done){
          if(userexist){
            var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM waves WHERE event_id=$1 AND status=1 AND to_user=ANY($2) AND from_user=$3", [id, attendeeids, guser.u_id], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                            return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            reslen = result.rows.length;
                            
                            //console.log(result.rows);
                            for(i=0; i<reslen; i++){
                              accepteduserids.push(result.rows[i].to_user);
                            }
                            //console.log(topics);
                      }

                      client.end();
                      done();
                    });
                  }else{
                    done();
                  }       
        },
        
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT users.*, profile.* FROM users INNER JOIN profile ON users.u_id = profile.profile_user WHERE profile.profile_user = ANY($1)", [attendeeids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    attendees.push({  'user_id':result.rows[i].profile_user, 
                                      'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                      'photo':result.rows[i].profile_photo,
                                      'email':result.rows[i].email,
                                      'saidhello': 0
                                      });
                  }
                  
              }

              client.end();
              callback();
            
          });
        },
        function(callback) {
          if(userexist){
            reslen = attendees.length;
            console.log(waveduserids);
            for(i=0; i<reslen; i++){
              if(waveduserids.indexOf(attendees[i].user_id) != -1)
              attendees[i].saidhello = 2;
            }
          }
          
          callback();
        },

        function(callback) {
          if(userexist){
            reslen = attendees.length;
            console.log(waveduserids);
            for(i=0; i<reslen; i++){
              if(wavedforuserids.indexOf(attendees[i].user_id) != -1)
              attendees[i].saidhello = 3;
            }
          }
          
          callback();
        },

        function(callback) {
          if(userexist){
            reslen = attendees.length;
            console.log(waveduserids);
            for(i=0; i<reslen; i++){
              if(accepteduserids.indexOf(attendees[i].user_id) != -1)
              attendees[i].saidhello = 1;
            }
          }
          
          callback();
        },
        
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);

                        
                        product = [{
                                    "count":attendees.length,
                                    "data":attendees
                                  }];

                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : product
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },





  getExhibitors: function(req, res) {
    var id = req.params.id;
    var product; // Spoof a DB call

    var exhibitorids = new Array;
    var exhibitorloc = new Array;
    var exhibitors = new Array;

    var subevents = new Array;
    var subeventscat = new Array;


    async.series([
        //Load user to get `userId` first
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM event_company WHERE event_id=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                        
                          for(i=0; i<reslen; i++){
                            exhibitorids.push(result.rows[i].company_id);
                            exhibitorloc[result.rows[i].company_id] = result.rows[i].company_location;
                            //console.log(result.rows[i].topic_branch);
                          }

                        
                      }

                      client.end();
                      callback();
                  });
        },
        function(callback) {
                //console.log(allEvents);
          
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM company WHERE company_id = ANY($1)", [exhibitorids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    exhibitors.push({ 'company_id':result.rows[i].company_id, 
                                      'company_name':result.rows[i].company_name, 
                                      'company_image':result.rows[i].company_image, 
                                      'company_location':exhibitorloc[result.rows[i].company_id]
                                      });
                  }
                  
              }
              client.end();
              callback();
            
            
          });

          


        },
        
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);

                        i = 0;

                        var startdate = new Date(result.rows[i].event_start);
                        var enddate = new Date(result.rows[i].event_end);
                        
                        product = [{
                                    "count":exhibitors.length,
                                    "data":exhibitors
                                  }];

                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : product
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },


  getPrivateExhibitors: function(req, res) {
    var id = req.params.id;
    var product; // Spoof a DB call

    var exhibitorids = new Array;
    var exhibitorloc = new Array;
    var exhibitors = new Array;
    var myexhibitors = new Array;

    var subevents = new Array;
    var subeventscat = new Array;


    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){

          async.series([
        //Load user to get `userId` first

        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM myexhibitors WHERE event_id=$1 AND user_id=$2", [id, user.u_id], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(1);
                   reslen = result.rows.length;
                      
                  for(i=0; i<reslen; i++){
                    myexhibitors.push(result.rows[i].company_id);
                  }

              }

              client.end();
              callback();
              
            
          });
          
          //console.log(subevents);
          //subevents[i].isSelected = true;
          
        },

        function(callback) {
          //console.log(myexhibitors);
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM event_company WHERE event_id=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                        
                          for(i=0; i<reslen; i++){
                            exhibitorids.push(result.rows[i].company_id);
                            exhibitorloc[result.rows[i].company_id] = result.rows[i].company_location;
                            //console.log(result.rows[i].topic_branch);
                          }

                        
                      }

                      client.end();
                      callback();
                  });
        },
        function(callback) {
                //console.log(allEvents);
          
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM company WHERE company_id = ANY($1)", [exhibitorids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    isSelected = false;
                    //console.log(myexhibitors.indexOf(result.rows[i].company_id));
                    if(myexhibitors.indexOf(result.rows[i].company_id) > -1){
                      
                      isSelected = true;
                    }
                    exhibitors.push({ 'company_id':result.rows[i].company_id, 
                                      'company_name':result.rows[i].company_name, 
                                      'company_image':result.rows[i].company_image, 
                                      'company_location':exhibitorloc[result.rows[i].company_id],
                                      'isSelected': isSelected
                                      });
                  }
                  
              }

              client.end();
              callback();
            
            
          });

          


        },
        
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);

                        i = 0;

                        var startdate = new Date(result.rows[i].event_start);
                        var enddate = new Date(result.rows[i].event_end);
                        
                        product = [{
                                    "count":exhibitors.length,
                                    "data":exhibitors
                                  }];

                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : product
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });
    

        }
  });

    
  },

  getTalks: function(req, res) {
    var id = req.params.id;
    var product; // Spoof a DB call


    var subevents = new Array;
    var subeventscat = new Array;

    var speakerids = new Array;
    var speakers = new Array;

    var talks = new Array;
    var workshops = new Array;

    async.series([
        //Load user to get `userId` first
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM event_company WHERE event_id=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                          
                          for(i=0; i<reslen; i++){
                            exhibitorids.push(result.rows[i].company_id);
                            exhibitorloc[result.rows[i].company_id] = result.rows[i].company_location;
                            //console.log(result.rows[i].topic_branch);
                          }

                        
                      }

                      client.end();
                      callback();
                  });
        },
        
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM subevents WHERE subevent_event=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                        
                          for(i=0; i<reslen; i++){
                            subtype = ["","talk","workshop"];
                            if(typeof result.rows[i].subevent_type === undefined){
                              subevent_type = "";
                            }else{
                              subevent_type = subtype[result.rows[i].subevent_type];
                            }
                            

                            

                            var startdate = new Date(result.rows[i].subevent_start);
                            var enddate = new Date(result.rows[i].subevent_end);

                            subeventid = result.rows[i].subevent_id;
                            subeventuserid = result.rows[i].subevent_user;
                            speakerids[subeventid] = subeventuserid;

                            subevents.push({ 
                                      'subevent_id':subeventid, 
                                      'subevent_name':result.rows[i].subevent_name, 
                                      'subevent_image':result.rows[i].subevent_image, 
                                      'subevent_description':result.rows[i].subevent_description,
                                      'subevent_type':subevent_type,
                                      'subevent_provider_id':subeventuserid,
                                      'subevent_provider':"Test Test",
                                      'subevent_start':addZero(startdate.getHours()) + ':' + addZero(startdate.getMinutes()), 
                                      'subevent_end':addZero(enddate.getHours()) + ':' + addZero(enddate.getMinutes()), 
                                      'subevent_day': days[startdate.getDay()] + ', ' + months[startdate.getMonth()] + ' ' + startdate.getDate() + ',' +  startdate.getFullYear(),
                                      'sortdate' : result.rows[i].subevent_start
                                      });
                            
                          }

                        
                      }

                      client.end();
                      callback();
                  });
        },
        
        function(callback){
            function compare(a,b) {
              if (a.sortdate < b.sortdate)
                return -1;
              if (a.sortdate > b.sortdate)
                return 1;
              return 0;
            }

            subevents.sort(compare);

            reslen = subevents.length;
            spelen = speakers.length;
            for(i=0; i<reslen; i++){
              seid = subevents[i].subevent_id;
              for(j=0; j<spelen; j++){
                if(speakers[j].user_id == speakerids[seid]){
                  subevents[i].subevent_provider = speakers[j].name;
                }           
              }

              if(subevents[i].subevent_type == 'workshop'){
                workshops.push(subevents[i]);
              }

              if(subevents[i].subevent_type == 'talk'){
                talks.push(subevents[i]);
              }

            }

            client.end();
            callback();

        },
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);

                        

                        i = 0;

                        var startdate = new Date(result.rows[i].event_start);
                        var enddate = new Date(result.rows[i].event_end);
                        
                        product = [{
                                    "count":talks.length,
                                    "data":talks
                                  }];


                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : product
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },


  getWorkshops: function(req, res) {
    var id = req.params.id;
    var product; // Spoof a DB call
    


    var subevents = new Array;
    var subeventscat = new Array;

    var speakerids = new Array;
    var speakers = new Array;

    var talks = new Array;
    var workshops = new Array;

    async.series([
        //Load user to get `userId` first
        
        
        function(callback) {
            var client = new pg.Client(connectionString);
            client.connect();
            client.query("SELECT * FROM subevents WHERE subevent_event=$1", [id], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      //if no rows were returned from query, then new user
                      if (result.rows.length > 0){
                          //console.log(result.rows.length);
                          reslen = result.rows.length;
                        
                          for(i=0; i<reslen; i++){
                            subtype = ["","talk","workshop"];
                            if(typeof result.rows[i].subevent_type === undefined){
                              subevent_type = "";
                            }else{
                              subevent_type = subtype[result.rows[i].subevent_type];
                            }
                            

                            

                            var startdate = new Date(result.rows[i].subevent_start);
                            var enddate = new Date(result.rows[i].subevent_end);

                            subeventid = result.rows[i].subevent_id;
                            subeventuserid = result.rows[i].subevent_user;
                            speakerids[subeventid] = subeventuserid;

                            subevents.push({ 
                                      'subevent_id':subeventid, 
                                      'subevent_name':result.rows[i].subevent_name, 
                                      'subevent_image':result.rows[i].subevent_image, 
                                      'subevent_description':result.rows[i].subevent_description,
                                      'subevent_type':subevent_type,
                                      'subevent_provider_id':subeventuserid,
                                      'subevent_provider':"Test Test",
                                      'subevent_start':addZero(startdate.getHours()) + ':' + addZero(startdate.getMinutes()), 
                                      'subevent_end':addZero(enddate.getHours()) + ':' + addZero(enddate.getMinutes()), 
                                      'subevent_day': days[startdate.getDay()] + ', ' + months[startdate.getMonth()] + ' ' + startdate.getDate() + ',' +  startdate.getFullYear(),
                                      'sortdate' : result.rows[i].subevent_start
                                      });
                            
                          }

                        
                      }

                      client.end();
                      callback();
                  });
        },
        
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM profile WHERE profile_user = ANY($1)", [speakerids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    speakers.push({   'user_id':result.rows[i].profile_user, 
                                      'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                      'photo':result.rows[i].profile_photo
                                      });
                  }
                  
              }

              client.end();
              callback();
            
          });
        },
        function(callback){
            function compare(a,b) {
              if (a.sortdate < b.sortdate)
                return -1;
              if (a.sortdate > b.sortdate)
                return 1;
              return 0;
            }

            subevents.sort(compare);

            reslen = subevents.length;
            spelen = speakers.length;
            for(i=0; i<reslen; i++){
              seid = subevents[i].subevent_id;
              for(j=0; j<spelen; j++){
                if(speakers[j].user_id == speakerids[seid]){
                  subevents[i].subevent_provider = speakers[j].name;
                }           
              }

              if(subevents[i].subevent_type == 'workshop'){
                workshops.push(subevents[i]);
              }

              if(subevents[i].subevent_type == 'talk'){
                talks.push(subevents[i]);
              }

            }


            client.end();
            callback();

        },
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        
                        product = [{
                                    "count":workshops.length,
                                    "data":workshops
                                  }];

                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : product
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },


  create: function(req, res) {
    var event_image = req.body.image || '';
    var event_name = req.body.name || '';
    var event_description = req.body.description || '';
    var event_location = req.body.location || '';
    var event_start = req.body.start || '';
    var event_end = req.body.end || '';

    if (event_image == ''  || event_name == ''  || event_description == ''  || event_location == '' ||  event_start == ''  || event_end == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "All fields are required.",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);

    /*subevent_start = new Date().getTime();
    subevent_end = new Date().getTime();*/

    //var now = new Date();
    //var mydate = new Date(now.getFullYear(), now.getMonth() , now.getDate(), now.getHours(), now.getMinutes());

    
    //subevent_start = mydate;
    //subevent_end = mydate;

    User.findOne( username , function(err, user) {
        if(user){

          alreadysub = 0;
          user_id = user.u_id;
          subsid = "";
          msg = '';

          async.series([
              
              //Load user to get `userId` first
              function(callback) {
                //console.log(topicbranches);
                if(!alreadysub){
                  var client = new pg.Client(connectionString);
                  client.connect();
                  client.query("INSERT INTO events(event_name, event_description, event_image, event_location, event_start, event_end) VALUES($1, $2, $3, $4, $5, $6) RETURNING event_id", [event_name, event_description, event_image, event_location, event_start, event_end], function(err, result){
                      //console.log(result.rows[0]);
                      if(err){
                        console.log(err);
                          return ;
                      }


                      client.end();
                      //console.log(result);
                      //subsid = result.rows[0].subs_id;
                      msg = "Event created successfully.";
                      res.status(200);
                      res.json({
                        "status": 200,
                        "message": msg,
                        "data" : ""
                      });
                  });
                }else{
                  msg = "";
                  res.status(401);
                  res.json({
                    "status": 401,
                    "message": msg,
                    "data" : ""
                  });
                }

                

              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });




           //console.log(user);
            

        }
        
    });
  },

  update: function(req, res) {
    var updateProduct = req.body;
    var id = req.params.id;
    data[id] = updateProduct // Spoof a DB call
    res.json(updateProduct);
  },

  delete: function(req, res) {
    var id = req.params.id;
    data.splice(id, 1) // Spoof a DB call
    res.json(true);
  }
};

var data = [{
  name: 'event 1',
  id: '1'
}, {
  name: 'event 2',
  id: '2'
}, {
  name: 'event 3',
  id: '3'
}];


function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}


function extractWords(text){
  var sWords = text.toLowerCase().trim().replace(/[,;.]/g,'').split(/[\s\/]+/g).sort();
  var iWordsCount = sWords.length; // count w/ duplicates

  // array of words to ignore
  var ignore = ['and','the','to','a','of','for','as','i','with','it','is','on','that','this','can','in','be','has','if','at'];
  ignore = (function(){
    var o = {}; // object prop checking > in array checking
    var iCount = ignore.length;
    for (var i=0;i<iCount;i++){
      o[ignore[i]] = true;
    }
    return o;
  }());

  var counts = {}; // object for math
  for (var i=0; i<iWordsCount; i++) {
    var sWord = sWords[i];
    if (!ignore[sWord]) {
      counts[sWord] = counts[sWord] || 0;
      counts[sWord]++;
    }
  }

  var arr = []; // an array of objects to return
  for (sWord in counts) {
    arr.push({
      text: sWord,
      frequency: counts[sWord]
    });
  }

  // sort array by descending frequency | http://stackoverflow.com/a/8837505
  return arr.sort(function(a,b){
    return (a.frequency > b.frequency) ? -1 : ((a.frequency < b.frequency) ? 1 : 0);
  });
}





module.exports = events;
