


var pg = require('pg');
var connectionString = require('../config/database');

var async = require('async');

var User = require('../models/user');




var speaker = {

 

  getOne: function(req, res) {
    var id = req.params.id;

    var linkedin_headline;
    var linkedin_industry;
    var linkedin_summary;
    var speaker = new Array;
    var subeventids = new Array;
    var subevents = new Array;


    async.series([
        //Load user to get `userId` first
        function(callback){
          var client = new pg.Client(connectionString);
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM subevents WHERE subevent_user = $1", [id], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    subevents.push({
                                    'subevent_id':result.rows[i].subevent_id,
                                    'subevent_name':result.rows[i].subevent_name,
                                    'subevent_image':result.rows[i].subevent_image,
                                    'subevent_description':result.rows[i].subevent_description,
                    });
                    //console.log(result.rows[i].profile_fname);
                    
                  }
                  
              }
            client.end();
            callback();
            
          });
        },
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM user_linkedin WHERE user_id = $1", [id], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    linkedin_headline = result.rows[i].linkedin_headline;
                    linkedin_industry = result.rows[i].linkedin_industry;
                    linkedin_summary = result.rows[i].linkedin_summary;
                  }
                  
              }

            client.end();
            callback();
            
          });
        },
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM profile WHERE profile_user = $1", [id], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    speaker = [{    
                                    'user_id':result.rows[i].profile_user, 
                                    'name':result.rows[i].profile_fname + " " + result.rows[i].profile_lname, 
                                    'photo':result.rows[i].profile_photo,
                                    'headline': linkedin_headline,
                                    'industry' : linkedin_industry,
                                    'summary' : linkedin_summary,
                                    'subevents': subevents
                                    }];
                  }
                  
              }

            client.end();
            callback();
            
          });
        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(allEvents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : speaker
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },

  update: function(req, res) {
    var updateProduct = req.body;
    var id = req.params.id;
    data[id] = updateProduct // Spoof a DB call
    res.json(updateProduct);
  },

  delete: function(req, res) {
    var id = req.params.id;
    data.splice(id, 1) // Spoof a DB call
    res.json(true);
  }
};

var data = [{
  name: 'event 1',
  id: '1'
}, {
  name: 'event 2',
  id: '2'
}, {
  name: 'event 3',
  id: '3'
}];







module.exports = speaker;
