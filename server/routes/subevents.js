

var g = require('ger')
var esm = new g.MemESM()
var ger = new g.GER(esm);

var pg = require('pg');
var connectionString = require('../config/database');

var async = require('async');

var User = require('../models/user');



var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];



var subevents = {




  getSuggested:  function(req, res, next) {

    var topics = [];
    var lutopics = [];
    var topicbranches = [];
    var eventids = [];
    var events = new Array;

    async.series([
        //Load user to get `userId` first
        function(callback) {

          var client = new pg.Client(connectionString);
          client.connect();
          client.query('SELECT * FROM topics', null, function(err, result){

                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){

                      


                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        for(i=0; i<reslen; i++){
                          
                          var mytopic = {
                            namespace: 'events',
                            person: 'bob-'+result.rows[i].topic_branch,
                            action: 'likes',
                            thing: result.rows[i].topic_related,
                            expires_at: '2025-06-06'
                          };
                          topics.push(mytopic);
                        }

                        var mytopic = {
                            namespace: 'events',
                            person: 'alice',
                            action: 'likes',
                            thing: 'photoshop',
                            expires_at: '2025-06-06'
                          };
                          topics.push(mytopic);
                          //console.log(1);
                          
                          callback();
                        //console.log(topics);
                        
                    }
                });
            
        },
        //Load posts (won't be called before task 1's "task callback" has been called)
        function(callback) {
          //console.log(topics);
          ger.initialize_namespace('events')
            .then( function() {

              return ger.events(topics);
            })
            .then( function() {
              // What things might alice like? 
              return ger.recommendations_for_person('events', 'alice', {actions: {likes: 1}})
            })
            .then( function(recommendations) {
              //console.log("\nRecommendations For 'alice'")
              //console.log(JSON.stringify(recommendations,null,2))
              //console.log(recommendations.recommendations[0].thing);
              recomm = recommendations.recommendations;
              recomm.forEach(function(item){
                  //console.log(item.thing);
                  lutopics.push(item.thing);
              });
              callback();
            })
            /*.then( function() {
              // What things are similar to xmen? 
              return ger.recommendations_for_thing('events', 'green_energy', {actions: {likes: 1}})
            })
            .then( function(recommendations) {
              console.log("\nRecommendations Like 'green_energy'")
              console.log(JSON.stringify(recommendations,null,2))
            });*/
            
        },
        function(callback) {
          
          var combarray = new Array;
          var brancharr = new Array;

          lutopics.map(function(lutopic){
              // This will wrap each element of the dates array with quotes
              combarray.push( lutopic );
          });
          
          //console.log(combarray);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM topics WHERE topic_related = ANY($1)", [combarray], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        for(i=0; i<reslen; i++){
                          brancharr.push(result.rows[i].topic_id);
                          //console.log(result.rows[i].topic_branch);
                        }
                      
                      /*var uniqueArray = brancharr.filter(function(elem, pos) {
                        return brancharr.indexOf(elem) == pos;
                      });*/ 
                      topicbranches = brancharr;
                      //console.log(uniqueArray);

                      client.end();
                      callback();
                  }
                });
        },
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM topic_event WHERE topic_id = ANY($1)", [topicbranches], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){
                          eventids.push(result.rows[i].event_id);
                          //console.log(result.rows[i].topic_branch);
                        }
                      
                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(eventids);
          
          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id = ANY($1)", [eventids], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){
                          //eventids.push(result.rows[i].event_id);
                          //console.log(result.rows[i]);
                          events.push({'event_id':result.rows[i].event_id, 'event_name':result.rows[i].event_name, 'event_thumb':result.rows[i].event_thumb, 'event_location':result.rows[i].event_location, 'event_start':result.rows[i].event_start, 'event_end':result.rows[i].event_end});

                        }

                      client.end();
                        
                      //res.json(events);
                      res.status(200);
                      res.json({
                        "status": 200,
                        "message": "",
                        "data" : events
                      });
                      //callback();
                  }
                });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });
},

  getSubscribed : function(req, res){
    eventids = new Array;
    events = new Array;

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){

          async.series([
              //Load user to get `userId` first
              function(callback) {
                //console.log(topicbranches);

                user_id = user.u_id;
                //console.log(user_id);
                var client = new pg.Client(connectionString);
                
                //console.log("email is: "+key);
                client.connect();
                client.query("SELECT * from subscription where user_id=$1", [user_id], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return;
                    }
                    if(result.rows.length > 0){
                       //console.log(result.rows);
                        reslen = result.rows.length;
                        
                        for(i=0; i<reslen; i++){
                          eventids.push(result.rows[i].event_id);
                          //console.log(result.rows[i].topic_branch);
                        }
                    }
                    //console.log(eventids);

                    client.end();
                    callback();
                });
              },
              function(callback) {
                //console.log(allEvents);
                
                var client = new pg.Client(connectionString);
        
                //console.log("email is: "+key);
                client.connect();
                client.query("SELECT * FROM events WHERE event_id = ANY($1)", [eventids], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }

                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){
                          //eventids.push(result.rows[i].event_id);
                          events.push({'event_id':result.rows[i].event_id, 'event_name':result.rows[i].event_name, 'event_thumb':result.rows[i].event_thumb, 'event_location':result.rows[i].event_location, 'event_start':result.rows[i].event_start, 'event_end':result.rows[i].event_end});
                        }

                  }

                  client.end();

                  res.status(200);
                  res.json({
                  "status": 200,
                  "message": "",
                  "data" : events
                  });
                  
                });

                


              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


        }
        
    });
  },
  
  getAll: function(req, res) {
    var allEvents = new Array;

    var id = req.params.id;
    console.log(id);

    async.series([
        //Load user to get `userId` first
        function(callback) {
          //console.log(topicbranches);

          /*var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM subevents WHERE subevent_event=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){
                          allEvents.push({'subevent_id':result.rows[i].subevent_id, 'subevent_name':result.rows[i].subevent_name, 'subevent_image':result.rows[i].subevent_image, 'subevent_start':result.rows[i].subevent_start, 'subevent_end':result.rows[i].subevent_end});
                          //console.log(result.rows[i].topic_branch);
                        }
                        client.end();
                        callback();
                    }else{
                      res.json({
                        "status": 200,
                        "message": "No subevents were found.",
                        "data" : []
                      });
                    }
                });*/


          pg.connect(connectionString, function(err, client, done) {
              client.query("SELECT * FROM subevents WHERE subevent_event=$1 ORDER BY subevent_start ASC", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){
                          allEvents.push({'subevent_id':result.rows[i].subevent_id, 'subevent_name':result.rows[i].subevent_name, 'subevent_image':result.rows[i].subevent_image, 'subevent_start':result.rows[i].subevent_start, 'subevent_end':result.rows[i].subevent_end});
                          //console.log(result.rows[i].topic_branch);
                        }
                        done();
                        callback();
                    }else{
                      res.json({
                        "status": 200,
                        "message": "No subevents were found.",
                        "data" : []
                      });
                    }
                });
          });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(allEvents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : allEvents
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },

  getOne: function(req, res) {
    var id = req.params.id;

    var event_id;
    var product; // Spoof a DB call


    async.series([
        //Load user to get `userId` first
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM subevents WHERE subevent_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        i = 0;

                        var startdate = new Date(result.rows[i].subevent_start);
                        var enddate = new Date(result.rows[i].subevent_end);

                        event_id = result.rows[i].subevent_event;

                        type = 'Workshop';
                        if(result.rows[i].subevent_type == 1){
                          type = 'Talk';
                        }

                        product = {
                                    'subevent_id':result.rows[i].subevent_id, 
                                    'subevent_event':result.rows[i].subevent_event, 
                                    'subevent_image':result.rows[i].subevent_image, 
                                    'subevent_name':result.rows[i].subevent_name,
                                    'subevent_description':result.rows[i].subevent_description,
                                    'subevent_day': days[startdate.getDay()] + ', ' + months[startdate.getMonth()] + ' ' + startdate.getDate() + ',' +  startdate.getFullYear(),
                                    'subevent_start':addZero(startdate.getHours()) + ':' + addZero(startdate.getMinutes()) ,
                                    'subevent_end':addZero(enddate.getHours()) + ':' + addZero(enddate.getMinutes()) ,
                                    'subevent_type':type,
                                  };
                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM events WHERE event_id=$1", [event_id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                       
                      product.subevent_event = result.rows[0].event_name;

                      client.end();
                      callback();
                  }
                });


        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(product);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : product
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },

  create: function(req, res) {
    var subevent_event = req.body.event || '';
    var subevent_name = req.body.name || '';
    var subevent_description = req.body.description || '';
    var subevent_start = req.body.start || '';
    var subevent_end = req.body.end || '';

    if (subevent_event == '' || subevent_name == ''  || subevent_description == ''  || subevent_start == ''  || subevent_end == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "All fields are required.",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);

    /*subevent_start = new Date().getTime();
    subevent_end = new Date().getTime();*/

    //var now = new Date();
    //var mydate = new Date(now.getFullYear(), now.getMonth() , now.getDate(), now.getHours(), now.getMinutes());

    
    //subevent_start = mydate;
    //subevent_end = mydate;

    User.findOne( username , function(err, user) {
        if(user){

          alreadysub = 0;
          user_id = user.u_id;
          subsid = "";
          msg = '';

          async.series([
              function(callback) {
                /*var client = new pg.Client(connectionString);
                client.connect();
                client.query("select * from subscription where user_id=$1 and event_id=$2", [user_id, event_id], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return ;
                    }
                    if(result.rows.length > 0){
                      alreadysub = 1;
                    }
                    callback();
                });*/
                callback();
              },
              //Load user to get `userId` first
              function(callback) {
                //console.log(topicbranches);
                if(!alreadysub){
                  var client = new pg.Client(connectionString);
                  client.connect();
                  client.query("INSERT INTO subevents(subevent_event, subevent_name, subevent_description, subevent_start, subevent_end) VALUES($1, $2, $3, $4, $5) RETURNING subevent_id", [subevent_event, subevent_name, subevent_description, subevent_start, subevent_end], function(err, result){
                      //console.log(result.rows[0]);
                      if(err){
                        console.log(err);
                          return ;
                      }
                      //console.log(result);
                      //subsid = result.rows[0].subs_id;

                      client.end();
                      
                      msg = "Subevent created successfully.";
                      res.status(200);
                      res.json({
                        "status": 200,
                        "message": msg,
                        "data" : ""
                      });
                  });
                }else{
                  msg = "";
                  res.status(401);
                  res.json({
                    "status": 401,
                    "message": msg,
                    "data" : ""
                  });
                }

                

              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });




           //console.log(user);
            

        }
        
    });

  },

  update: function(req, res) {
    var updateProduct = req.body;
    var id = req.params.id;
    data[id] = updateProduct // Spoof a DB call
    res.json(updateProduct);
  },

  delete: function(req, res) {
    var id = req.params.id;
    data.splice(id, 1) // Spoof a DB call
    res.json(true);
  }
};

var data = [{
  name: 'event 1',
  id: '1'
}, {
  name: 'event 2',
  id: '2'
}, {
  name: 'event 3',
  id: '3'
}];


function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}





module.exports = subevents;
