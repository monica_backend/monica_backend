
var pg = require('pg');

var connectionString = require('../config/database');

var async = require('async');

var User = require('../models/user');



var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];


var installation = {

  saveToken: function(req, res) {

    var pushtoken = req.body.pushtoken || '';

    var tokenfound = 0;
    var status = 0;


    if (pushtoken == ''  ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Push Token is missing",
        "data" : []
      });
      return;
    }


          
          async.series([
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM installation WHERE install_token=$1", [pushtoken], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            tokenfound = 1;
                            
                        }

                      client.end();
                      done();
                    });
                  },
                  
                  function(done){
                    //console.log(user);

                    if(!tokenfound){
                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query("INSERT INTO installation(install_token) VALUES($1)", [pushtoken], function(err, result){
                          //console.log(err);
                          if(err){
                            console.log(err);
                              return ;
                          }else{
                            status = 1;
                          }

                        client.end();
                        done();
                      });
                    }else{
                      status = 1;
                      done();
                    }
                    
                  },
                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "Success",
                            "data" : status
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
       

  },


  setUser: function(req, res) {

    var pushtoken = req.body.pushtoken || '';
    //var user_id =  req.body.userid || '';

    var tokenfound = 0;
    var status = 0;


    if (pushtoken == '') {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Push Token is missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);

    User.findOne( username , function(err, user) {
        if(user){
          
          async.series([
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM installation WHERE install_token=$1", [pushtoken], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            tokenfound = 1;
                            
                        }

                      client.end();
                      done();
                    });
                  },
                  
                  function(done){
                    //console.log(user);

                    if(tokenfound){
                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query("UPDATE installation SET install_user=$2 WHERE install_token=$1", [pushtoken, user.u_id], function(err, result){
                          //console.log(err);
                          if(err){
                            console.log(err);
                              return ;
                          }else{
                            status = 1;
                          }

                        client.end();
                        done();
                      });
                    }else{
                      
                      done();
                    }
                    
                  },
                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "Success",
                            "data" : status
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
        }
        
    });
    
  },


  unsetUser: function(req, res) {

    var pushtoken = req.body.pushtoken || '';
    //var user_id =  req.body.userid || '';

    var tokenfound = 0;
    var status = 0;


    if (pushtoken == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Push Token is missing",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);

    User.findOne( username , function(err, user) {
        if(user){
          
          async.series([
                  function(done){
                    //console.log(user);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("SELECT * FROM installation WHERE install_token=$1", [pushtoken], function(err, result){
                        //console.log(err);
                        if(err){
                          console.log(err);
                          return ;
                        }

                        //if no rows were returned from query, then new user
                        if (result.rows.length > 0){
                            //console.log(result.rows.length);
                            tokenfound = 1;
                            
                        }

                      client.end();
                      done();
                    });
                  },
                  
                  function(done){
                    //console.log(user);

                    if(tokenfound){
                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query("UPDATE installation SET install_user=NULL WHERE install_token=$1", [pushtoken], function(err, result){
                          //console.log(err);
                          if(err){
                            console.log(err);
                              return ;
                          }else{
                            status = 1;
                          }

                        client.end();
                        done();
                      });
                    }else{
                      
                      done();
                    }
                    
                  },
                  function(callback) {
                    //console.log(allEvents);
                    
                    //res.json(product);
                    res.status(200);
                    res.json({
                            "status": 200,
                            "message": "Success",
                            "data" : status
                            });

                  }
                ],
                 function(err) { //This function gets called after the two tasks have called their "task callbacks"
                    if (err) return next(err);
                    //Here locals will be populated with `user` and `posts`
                    //Just like in the previous example
                    //res.render('user-profile', locals);
                }
              );
        }
        
    });
    
  },

  getOne: function(req, res) {
    var id = req.params.id;
    var user = data[0]; // Spoof a DB call
    res.json(user);
  },

  create: function(req, res) {
    var newuser = req.body;
    data.push(newuser); // Spoof a DB call
    res.json(newuser);
  },

  update: function(req, res) {
    var updateuser = req.body;
    var id = req.params.id;
    data[id] = updateuser // Spoof a DB call
    res.json(updateuser);
  },

  delete: function(req, res) {
    var id = req.params.id;
    data.splice(id, 1) // Spoof a DB call
    res.json(true);
  },

  uploadFile : function(req, res) {
    // We are able to access req.files.file thanks to 
    // the multiparty middleware
    var file = req.files.file;
    console.log(file.name);
    console.log(file.type);
  }
};


/*users.prototype.uploadFile = function(req, res) {
    // We are able to access req.files.file thanks to 
    // the multiparty middleware
    var file = req.files.file;
    console.log(file.name);
    console.log(file.type);
}*/


var data = [{
  name: 'user 1',
  id: '1'
}, {
  name: 'user 2',
  id: '2'
}, {
  name: 'user 3',
  id: '3'
}];

module.exports = installation;
