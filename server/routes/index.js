var express = require('express');
var router = express.Router();

var auth = require('./auth.js');
var products = require('./products.js');
var events = require('./events.js');
var user = require('./users.js');
var subs = require('./subs.js');
var substopics = require('./substopics.js');
var subevents = require('./subevents.js');

var company = require('./company.js');
var speaker = require('./speaker.js');

var news = require('./news.js');
var installation = require('./installation.js');

//var documents = require('./documents.js');  
var elastic = require('../models/elasticsearch.js');


var formidable = require('formidable');


var path = require('path');





var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();


var uploadedfilename = "";

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

var multer  =   require('multer');
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, '../../html/monica/uploads');
  },
  filename: function (req, file, callback) {
  	//console.log(file);
    ext = path.extname(file.originalname);
    uploadedfilename = guid()+ext; //Date.now() + '-' + file.originalname;
    callback(null, uploadedfilename);
  }
});

var upload = multer({ storage : storage}).single('file');

router.get('/',function(req,res){
      res.sendFile(__dirname + "/index.html");
});

//router.post('/api/user/uploads', upload.single(), user.uploadFile);

router.post('/api/user/uploads',function(req,res){
    upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        console.log(uploadedfilename);
        res.end(uploadedfilename);
    });
});







var https = require('https');
 
function getCall() {
    //initialize options values, the value of the method can be changed to POST to make https post calls
    //var key = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxZmFiOTc0NS0xMDVkLTQ3NTUtOGZkNy1lNDAxM2IzODQ4YmQifQ.ZxG23UooTjoUSQb-xXew4B5QQjH5v5oSaUUKjosn9n8';
    //var regtoken = '5cb8aea4ae046eb686ce482d60aec551fc9d1a459ca30319638631e2b7430473';
    var options = {
        host :  'api.ionic.io',
        //url :  'api.ionic.io/push/notifications',
        //port : 443,
        path : '/push/notifications',
        //tokens: ["5cb8aea4ae046eb686ce482d60aec551fc9d1a459ca30319638631e2b7430473"],
        method : 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxZmFiOTc0NS0xMDVkLTQ3NTUtOGZkNy1lNDAxM2IzODQ4YmQifQ.ZxG23UooTjoUSQb-xXew4B5QQjH5v5oSaUUKjosn9n8'
        },
        data: {
          "tokens": ["5cb8aea4ae046eb686ce482d60aec551fc9d1a459ca30319638631e2b7430473", "26d3b655a3504fac9e712a40ebd89c5ccf3a5cef2d976e842924b54864a21469", "dshHVwBNfUg:APA91bEIb8nmrGH0yOZk1-aO1r0y6_yxSDYcXyFZLFP5xaxw2uNwDs7tIeV_OU72NHFZ5MvC62IvAfXrGgYBOktubXZbQX4lSMYxlMrGsWAnTgbB0OnYKI6W3-rM5smJrFbWaSOBeFvl","dVsZ8wHOlOI:APA91bFo3OFufbuWx0I3XcyiuPI8LyYJEXrlaiRJKexkMVw10wr_3WPNStFypzHTMxweLaWWoBUg7Pby0js7Zujvjkx_E5D0EpIDYFdTrQTv0nw6AyunWEBBkno14wFhRlkOrvqcy-bo"],
          "profile": "ionic_security_profile",
          "notification": {
            "title": "title",
            "message": "message",
            "android": {
              "title": "title",
              "message": "message"
            },
            "ios": {
              "title": "title",
              "message": "message"
            }
          }
        }
    }
 
    //making the https get call
    var getReq = https.request(options, function(res) {
        console.log("\nstatus code: ", res.statusCode);
        res.on('data', function(data) {
            console.log( JSON.parse(data) );
        });
    });
 
    //end the request
    getReq.end();
    getReq.on('error', function(err){
        console.log("Error: ", err);
    }); 
}
 
//getCall();


var querystring = require('querystring');
var http = require('http');

function escapeJson(json) {
   return JSON.parse(JSON.stringify(json));
}

title = escapeJson('title');
message = escapeJson('message');

var post_data = querystring.stringify({
    "tokens": ["5cb8aea4ae046eb686ce482d60aec551fc9d1a459ca30319638631e2b7430473", "26d3b655a3504fac9e712a40ebd89c5ccf3a5cef2d976e842924b54864a21469", "dshHVwBNfUg:APA91bEIb8nmrGH0yOZk1-aO1r0y6_yxSDYcXyFZLFP5xaxw2uNwDs7tIeV_OU72NHFZ5MvC62IvAfXrGgYBOktubXZbQX4lSMYxlMrGsWAnTgbB0OnYKI6W3-rM5smJrFbWaSOBeFvl","dVsZ8wHOlOI:APA91bFo3OFufbuWx0I3XcyiuPI8LyYJEXrlaiRJKexkMVw10wr_3WPNStFypzHTMxweLaWWoBUg7Pby0js7Zujvjkx_E5D0EpIDYFdTrQTv0nw6AyunWEBBkno14wFhRlkOrvqcy-bo"],
    "profile": "prod",
    "notification": {
      "title": title,
      "message": message,
      "android": {
        "title": title,
        "message": message
      },
      "ios": {
        "title": title,
        "message": message
      }
    }
  });

var req = {
  method: 'POST',
  host :  'api.ionic.io',
  //url :  'api.ionic.io/push/notifications',
        //port : 443,
  path : '/push/notifications',
  //url: 'https://api.ionic.io/push/notifications',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxZmFiOTc0NS0xMDVkLTQ3NTUtOGZkNy1lNDAxM2IzODQ4YmQifQ.ZxG23UooTjoUSQb-xXew4B5QQjH5v5oSaUUKjosn9n8',
    'Content-Length': Buffer.byteLength(post_data),
  },
  //data: post_data
};


/*myreq = https.request(req, function(res) {
      console.log("\nstatus code: ", res.statusCode);
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
          console.log('Response: ' + chunk);
      });
  });

myreq.write(post_data);
myreq.end();
myreq.on('error', function(err){
    console.log("Error: ", err);
});*/







function PostCode() {
  // Build the post string from an object
  var post_data = querystring.stringify({
      'compilation_level' : 'ADVANCED_OPTIMIZATIONS',
      'output_format': 'json',
      'output_info': 'compiled_code',
        'warning_level' : 'QUIET',
      'tokens': ["5cb8aea4ae046eb686ce482d60aec551fc9d1a459ca30319638631e2b7430473", "26d3b655a3504fac9e712a40ebd89c5ccf3a5cef2d976e842924b54864a21469", "dshHVwBNfUg:APA91bEIb8nmrGH0yOZk1-aO1r0y6_yxSDYcXyFZLFP5xaxw2uNwDs7tIeV_OU72NHFZ5MvC62IvAfXrGgYBOktubXZbQX4lSMYxlMrGsWAnTgbB0OnYKI6W3-rM5smJrFbWaSOBeFvl","dVsZ8wHOlOI:APA91bFo3OFufbuWx0I3XcyiuPI8LyYJEXrlaiRJKexkMVw10wr_3WPNStFypzHTMxweLaWWoBUg7Pby0js7Zujvjkx_E5D0EpIDYFdTrQTv0nw6AyunWEBBkno14wFhRlkOrvqcy-bo"],
  });

  // An object of options to indicate where to post to
  var post_options = {
      host: 'api.ionic.io',
      //port: '443',
      path: '/push/notifications',
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(post_data),
          'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxZmFiOTc0NS0xMDVkLTQ3NTUtOGZkNy1lNDAxM2IzODQ4YmQifQ.ZxG23UooTjoUSQb-xXew4B5QQjH5v5oSaUUKjosn9n8'
      }
  };

  // Set up the request
  var post_req = https.request(post_options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
          console.log('Response: ' + chunk);
      });
  });

  // post the data
  post_req.write(post_data);
  post_req.end();

}

//PostCode();

/*var request = require('request');

//Lets configure and request
request({
    url: 'https://api.ionic.io/push/notifications', //URL to hit
    //qs: '{}', //Query string data
    method: 'POST', //Specify the method
    body: '{"tokens": ["cYnxxq-HNfY:APA91bHZszS47Q27VmVQ6XleXfIc1GKReJx_961JwHITapcsoX0E1CeUeyEA0yOLhMzPO27TsgttzZiEE5lrKGFtvlEbi8XJjXculnZe0I9aFS52PyHReoqlxizqcTNU8S6S4KuMqETI"], "profile": "prod",  "notification": {"title": "title", "message": "message"}}',
    headers: { //We can define headers too
        'Content-Type': 'application/json',
        //'Custom-Header': 'Custom Value',
        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxZmFiOTc0NS0xMDVkLTQ3NTUtOGZkNy1lNDAxM2IzODQ4YmQifQ.ZxG23UooTjoUSQb-xXew4B5QQjH5v5oSaUUKjosn9n8'

    }
}, function(error, response, body){
    if(error) {
        console.log(error);
    } else {
        console.log(response.statusCode, body);
    }
});*/

/*
router.post('/api/user/uploads', function (req, res, next) {

  var form = new formidable.IncomingForm();
    //Formidable uploads to operating systems tmp dir by default
    form.uploadDir = "./img";       //set upload directory
    form.keepExtensions = true;     //keep file extension

    form.parse(req, function(err, fields, files) {
        res.writeHead(200, {'content-type': 'text/plain'});
        res.write('received upload:\n\n');
        console.log("form.bytesReceived");
        //TESTING
        console.log("file size: "+JSON.stringify(files.fileUploaded.size));
        console.log("file path: "+JSON.stringify(files.fileUploaded.path));
        console.log("file name: "+JSON.stringify(files.fileUploaded.name));
        console.log("file type: "+JSON.stringify(files.fileUploaded.type));
        console.log("astModifiedDate: "+JSON.stringify(files.fileUploaded.lastModifiedDate));

        //Formidable changes the name of the uploaded file
        //Rename the file to its original name
        fs.rename(files.fileUploaded.path, './img/'+files.fileUploaded.name, function(err) {
        if (err)
            throw err;
          console.log('renamed complete');  
        });
          res.end();
    });
});
*/


/*var multer  =   require('multer');
var upload = multer({ dest: 'uploads/' });
router.post('/profile', upload.single('avatar'), function (req, res, next) {
  // req.file is the `avatar` file
  // req.body will hold the text fields, if there were any
  console.log(req.body, req.file);
});*/



// Requires controller
//UserController = require('./users,');


/*
 * Routes that can be accessed by any one
 */
router.post('/signup', auth.signup);
router.post('/login', auth.login);
router.post('/login/linkedin', auth.loginlinkedin);

/*
 * Routes that can be accessed only by autheticated users
 */
router.get('/api/v1/products', products.getAll);
router.get('/api/v1/product/:id', products.getOne);
router.post('/api/v1/product/', products.create);
router.put('/api/v1/product/:id', products.update);
router.delete('/api/v1/product/:id', products.delete);


/*
	Event
*/
router.get('/api/v1/public/events/', events.getAll);
router.get('/api/v1/public/events/:skip', events.getAll);

router.get('/api/v1/events/subscribed', events.getPlanned); //getSubscribed
router.get('/api/v1/events/suggested', events.getSuggested);
router.get('/api/v1/event/agenda/:id', events.getMyAgenda);
router.get('/api/v1/public/event/:id', events.getOne);
router.get('/api/v1/public/event/agenda/:id', events.getAgenda);
router.get('/api/v1/event/privateagenda/:id', events.getPrivateAgenda);
router.get('/api/v1/public/event/speakers/:id', events.getSpeakers);
router.get('/api/v1/public/event/exhibitors/:id', events.getExhibitors);
router.get('/api/v1/event/privateexhibitors/:id', events.getPrivateExhibitors);
router.get('/api/v1/public/event/talks/:id', events.getTalks);
router.get('/api/v1/public/event/workshops/:id', events.getWorkshops);

router.get('/api/v1/public/event/attendees/:id', events.getAttendees);

router.post('/api/v1/event/', events.create);
router.post('/api/v1/public/events/search', events.getSearched);

router.post('/api/v1/event/agenda/subevent', events.addSubevent);
router.put('/api/v1/event/agenda/subevent', events.swapSubevents);
router.delete('/api/v1/event/agenda/subevent', events.deleteSubevent);

router.post('/api/v1/event/agenda/exhibitor', events.addExhibitor);
router.delete('/api/v1/event/agenda/exhibitor', events.deleteExhibitor);

//router.put('/api/v1/event/:id', events.update);
//router.delete('/api/v1/event/:id', events.delete);


//Subevents
router.get('/api/v1/public/subevents/:id', subevents.getAll);
router.get('/api/v1/public/subevent/:id', subevents.getOne);
router.post('/api/v1/subevent/', subevents.create);

/*
	Subcscribe
*/
//router.get('/api/v1/subs', subs.getAll);
router.get('/api/v1/subs/:id', subs.checkSubscribed);
router.post('/api/v1/subs/', subs.create);
//router.put('/api/v1/subs/:id', subs.update);
router.delete('/api/v1/subs/:id', subs.delete);


/*
  Sponsors
*/
router.get('/api/v1/public/company', company.getAll);
router.get('/api/v1/public/company/sponsors/:id', company.getSponsors);
router.get('/api/v1/public/company/:id', company.getOne);
router.post('/api/v1/company', company.create);


/* Subscription Topics */
//router.get('/api/v1/substopics', substopics.getAll);
//router.get('/api/v1/subs/:id', subs.getOne);

router.get('/api/v1/public/substopics', substopics.getAll);
router.get('/api/v1/substopics/answered/:id', substopics.hasAnswered);
router.get('/api/v1/substopics/skip/:id', substopics.skipQuestions);
router.get('/api/v1/substopics/:id', substopics.getTopics);
router.post('/api/v1/substopics1', substopics.getTopics1);
router.post('/api/v1/substopics2', substopics.getTopics2);
router.post('/api/v1/substopics3', substopics.getTopics3);


router.post('/api/v1/substopics/', substopics.create);
//router.put('/api/v1/subs/:id', subs.update);
router.delete('/api/v1/substopics/:id', substopics.delete);







/*
  News
*/
router.get('/api/v1/event/news/:id', news.getAll);
router.post('/api/v1/event/news/add', news.addNews);

/*
  Speakers
*/
router.get('/api/v1/public/speaker/:id', speaker.getOne);










/* GET suggestions */
router.get('/documents/suggest/:input', function (req, res, next) {  
  elastic.getSuggestions(req.params.input).then(function (result) { res.json(result) });
});

router.get('/documents/indextopics', substopics.indexTopics);

/* POST document to be indexed */
router.post('/documents/', function (req, res, next) {  
  elastic.addDocument(req.body).then(function (result) { res.json(result) });
});





/*
 * Routes that can be accessed only by authenticated & authorized users
 */

router.get('/api/v1/public/user/:id', user.viewOne);

router.get('/api/v1/admin/users', user.getAll);
router.get('/api/v1/admin/user/:id', user.getOne);
router.post('/api/v1/admin/user/', user.create);
router.put('/api/v1/admin/user/:id', user.update);
router.delete('/api/v1/admin/user/:id', user.delete);


//
router.post('/api/v1/user/sendmessage', user.sendMessage);
router.get('/api/v1/user/suggestedusers/:id', user.getSuggestedUsers);
router.get('/api/v1/user/sayhello/:id/:userid', user.sayHello);
router.get('/api/v1/user/gethellos/:id', user.getHellos);
router.get('/api/v1/user/checkhello/:id', user.checkHello);
router.post('/api/v1/user/handshakedo', user.handshakeDo);
router.post('/api/v1/user/handshakeupdate', user.handshakeUpdate);
router.get('/api/v1/user/handshake/:id', user.getHandshake);
router.get('/api/v1/user/handshakes/user/:id', user.getUserHandshakes);
router.get('/api/v1/user/handshakes/company/:id', user.getCompanyHandshakes);
router.get('/api/v1/user/chkhandshakeusr/:id/:userid', user.checkIfHandshakedUser);
router.get('/api/v1/user/chkhandshakecomp/:id/:companyid', user.checkIfHandshakedCompany);




router.post('/api/v1/public/installation/savetoken', installation.saveToken);
router.post('/api/v1/installation/setuser', installation.setUser);
router.post('/api/v1/installation/unsetuser', installation.unsetUser);




module.exports = router;
