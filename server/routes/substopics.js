
var pg = require('pg');

var connectionString = require('../config/database');

var async = require('async');

var User = require('../models/user');


var substopics = {

  //get all subscriptions of a user
  getTopics: function(req, res) {
    //var allProducts = data; // Spoof a DB call
    //res.json(allProducts);

    var id = req.params.id;

    var industryids = new Array;
    var subcatids = new Array;
    var catids = new Array;

    var topics = new Array;

    var question = '';

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){
          //console.log(id);
          //console.log(user);

          async.series([
              //Load user to get `userId` first
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM subevent_industry WHERE event_id=$1", [id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            reslen = result.rows.length;
                            for(i=0; i<reslen; i++){
                              industryids.push(result.rows[i].industry_id);
                            }
                          }
                          //console.log(industryids);
                          client.end();
                          callback();
                      });
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM linkedin_industry WHERE industry_id=ANY($1)", [industryids], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            reslen = result.rows.length;
                            for(i=0; i<reslen; i++){
                              //console.log(result.rows[i].industry_name);
                              subcatids.push(result.rows[i].industry_subcat);
                            }
                          }
                          //console.log(subcatids);
                          client.end();
                          callback();
                      });
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM topic_subcat WHERE subcat_id=ANY($1)", [subcatids], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            reslen = result.rows.length;
                            for(i=0; i<reslen; i++){
                              //console.log(result.rows[i].industry_name);
                              catids.push(result.rows[i].subcat_category);
                            }
                          }
                          //console.log(subcatids);
                          client.end();
                          callback();
                      });
              },
              function(callback){ //temporary block
                if(id == 14 || id == 15){
                  catids.push(10);
                  catids.push(11);
                  callback();
                }else{
                  callback();
                }
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM topic_category WHERE category_id=ANY($1)", [catids], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }

                          reslen = result.rows.length;

                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){

                            for(i=0; i<reslen; i++){
                   
                              topics.push({     
                                            'category_id':result.rows[i].category_id, 
                                            'category_name':result.rows[i].category_name
                                            });
                            }
                          }

                          client.end();

                          callback();
                          
                      });
              },

              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM question_one WHERE event_id=$1", [id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            question = result.rows[0].question_text;
                          }
                          //console.log(industryids);
                          client.end();
                          callback();
                      });
              },
              
              
              

              function(callback){
                res.status(200);
                res.json({
                        "status": 200,
                        "message": "",
                        "data" : {
                                  'question': question,
                                  'answers':topics
                                }
                        });
              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


        }
        
    });

  },

  skipQuestions: function(req, res) {
    //var allProducts = data; // Spoof a DB call
    //res.json(allProducts);

    var event_id = req.params.id;


    var subsubtopics = new Array;

    var topic = '';
    var subsubcat_ids = new Array;


    var subeventids = new Array;
    var filsubeventids = new Array;

    var companyids = new Array;
    var filcompanyids = new Array;


    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){
          //console.log(id);
          //console.log(user);
          user_id = user.u_id;

          async.series([
              //Load user to get `userId` first
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM user_linkedin WHERE user_id=$1", [user_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            reslen = result.rows.length;
                            topic = result.rows[0].linkedin_industry;
                          }
                          //console.log(industryids);
                          client.end();
                          callback();
                      });
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM linkedin_industry WHERE industry_name=$1", [topic], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            reslen = result.rows.length;
                            subsubcat_ids.push(result.rows[0].industry_id);
                          }
                          //console.log(subcatids);
                          client.end();
                          callback();
                      });
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();

                sizecat = subsubcat_ids.length;
                iter = 0;

                function insertData(item,callback) {
                  
                  client.query('INSERT INTO answer_three(event_id, user_id, topic_subsubcat) VALUES ($1,$2,$3)', [
                        event_id,
                        user.u_id,
                        item
                       ], 
                  function(err,result) {
                    // return any err to async.each iterator
                    //callback(err);
                    iter++;
                    
                    /*if(iter > (sizecat-1)){
                      
                    }*/
                    //console.log(iter);

                    //client.end();
                    callback();

                  })
                }

                
                async.each(subsubcat_ids,insertData,function(err) {
                  // Release the client to the pg module
                  //done();
                  if (err) {
                    return console.error('error running query', err);
                  }
                  client.end();
                  callback();
                  //console.log(sizecat);

                })
               
              },
              

              function(callback) {

                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query('SELECT * FROM subevent_industry WHERE industry_id=ANY($1) AND event_id=$2', [subsubcat_ids, event_id], function(err, result){
                                if(err){
                                    return ;
                                }
                                if (result.rows.length > 0){
                                  //console.log(result.rows.length);
                                  reslen = result.rows.length;
                                  
                                  //console.log(result.rows);
                                  for(i=0; i<reslen; i++){
                                    subeventids.push(result.rows[i].subevent_id);
                                  }
                                  
                                }
                                //console.log(result.rows.length);
                                //console.log("start");
                                //console.log(subeventids);

                                client.end();
                                callback();
                            });
                  
              },
              function(callback){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query('SELECT * FROM subevents WHERE subevent_id=ANY($1) OR ( subevent_event=$2 AND subevent_type=0)', [subeventids, event_id], function(err, result){
                              if(err){
                                  return ;
                              }
                              if (result.rows.length > 0){
                                //console.log(result.rows.length);
                                reslen = result.rows.length;
                                
                                startarr = new Array;
                                //console.log(result.rows);
                                for(i=0; i<reslen; i++){
                                  starttime = result.rows[i].subevent_start.toString();
                                  //console.log(startarr.indexOf(starttime));
                                  if(startarr.indexOf(starttime) == -1){
                                    filsubeventids.push(result.rows[i].subevent_id);
                                    startarr.push(starttime);
                                  }
                                }
                                
                              }
                              //console.log(result.rows.length);
                              //console.log("start");
                              //console.log(filsubeventids);
                              //console.log(startarr);

                              client.end();
                              callback();
                          });
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("DELETE FROM mysubevents WHERE event_id=$1 AND user_id=$2", [event_id, user_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }

                          client.end();
                          callback();
                          
                      });
              },
              function(callback){
                var client = new pg.Client(connectionString);
                client.connect();

                sizecat = subsubcat_ids.length;
                iter = 0;

                function insertData(item,callback) {
                  
                  client.query('INSERT INTO mysubevents(event_id, user_id, subevent_id) VALUES ($1,$2,$3)', [
                        event_id,
                        user_id,
                        item
                       ], 
                  function(err,result) {
                    // return any err to async.each iterator
                    //callback(err);
                    iter++;
                    
                    /*if(iter > (sizecat-1)){
                      
                    }*/
                    //console.log(iter);

                    //client.end();
                    callback();

                  })
                }

                
                async.each(filsubeventids,insertData,function(err) {
                  // Release the client to the pg module
                  //done();
                  if (err) {
                    return console.error('error running query', err);
                  }
                  client.end();
                  callback();
                  //console.log(sizecat);

                });
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("DELETE FROM myexhibitors WHERE event_id=$1 AND user_id=$2", [event_id, user_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }

                          client.end();
                          callback();
                          
                      });
              },
              function(callback) {

                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query('SELECT * FROM event_company WHERE event_id=$1', [event_id], function(err, result){
                                if(err){
                                    return ;
                                }
                                if (result.rows.length > 0){
                                  //console.log(result.rows.length);
                                  reslen = result.rows.length;
                                  
                                  //console.log(result.rows);
                                  for(i=0; i<reslen; i++){
                                    companyids.push(result.rows[i].company_id);
                                  }
                                  
                                }
                                //console.log(result.rows.length);
                                //console.log("start");
                                //console.log(subeventids);

                                client.end();
                                callback();
                            });
                  
              },
              function(callback) {

                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query('SELECT * FROM company_industry WHERE industry_id=ANY($1) AND company_id=ANY($2)', [subsubcat_ids, companyids], function(err, result){
                                if(err){
                                  console.log(err);
                                    return ;
                                }
                                if (result.rows.length > 0){
                                  //console.log(result.rows.length);
                                  reslen = result.rows.length;
                                  
                                  //console.log(result.rows);
                                  for(i=0; i<reslen; i++){
                                    filcompanyids.push(result.rows[i].company_id);
                                  }
                                  
                                }
                                //console.log(result.rows.length);
                                //console.log("start");
                                //console.log(subeventids);

                                client.end();
                                callback();
                            });
                  
              },
              function(callback){
                //console.log(filcompanyids);
                var client = new pg.Client(connectionString);
                client.connect();

                sizecat = subsubcat_ids.length;
                iter = 0;

                function insertData(item,callback) {
                  
                  client.query('INSERT INTO myexhibitors(event_id, user_id, company_id) VALUES ($1,$2,$3)', [
                        event_id,
                        user_id,
                        item
                       ], 
                  function(err,result) {
                    // return any err to async.each iterator
                    //callback(err);
                    iter++;
                    
                    /*if(iter > (sizecat-1)){
                      
                    }*/
                    //console.log(iter);

                    //client.end();
                    callback();

                  })
                }

                
                async.each(filcompanyids,insertData,function(err) {
                  // Release the client to the pg module
                  //done();
                  if (err) {
                    return console.error('error running query', err);
                  }
                  client.end();
                  callback();
                  //console.log(sizecat);

                });
              },

              function(callback){
                res.status(200);
                res.json({
                        "status": 200,
                        "message": "Successfully submitted.",
                        "data" : subsubtopics
                        });
              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


        }
        
    });

  },

  getTopics1: function(req, res) {
    //var allProducts = data; // Spoof a DB call
    //res.json(allProducts);

    var industryids = new Array;
    var subcatids = new Array;
    var catids = new Array;

    var subsid;
    var subtopics = new Array;

    var question = '';

    var event_id = req.body.event_id;
    var category_ids = req.body.category_ids;

    console.log("eventid: "+event_id);

    if (event_id == '' || category_ids == '' || !(category_ids instanceof Array) ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID and/or Topic Categories are missing",
        "data" : []
      });
      return;
    }

    

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){
          //console.log(id);
          //console.log(user);

          async.series([
              //Load user to get `userId` first
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("DELETE FROM answer_one WHERE event_id=$1 AND user_id=$2", [event_id, user.u_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }

                          client.end();
                          callback();
                          
                      });
              },
              function(callback){
                if(event_id == 14 || event_id == 15){
                  category_ids.push(9);
                  callback();
                }else{
                  callback();
                }
              },
              
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();

                sizecat = category_ids.length;
                iter = 0;

                function insertData(item,callback) {
                  
                  client.query('INSERT INTO answer_one(event_id, user_id, topic_category) VALUES ($1,$2,$3)', [
                        event_id, 
                        user.u_id,
                        item
                       ], 
                  function(err,result) {
                    // return any err to async.each iterator
                    //callback(err);
                    iter++;
                    
                    /*if(iter > (sizecat-1)){
                      
                    }*/
                    console.log(iter);

                    //client.end();
                    callback();

                  })
                }
                async.each(category_ids,insertData,function(err) {
                  // Release the client to the pg module
                  //done();
                  if (err) {
                    return console.error('error running query', err);
                  }
                  client.end();
                  callback();
                  //console.log(sizecat);

                })


                
               
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM subevent_industry WHERE event_id=$1", [event_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            reslen = result.rows.length;
                            for(i=0; i<reslen; i++){
                              industryids.push(result.rows[i].industry_id);
                            }
                          }
                          //console.log(industryids);
                          client.end();
                          callback();
                      });
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM linkedin_industry WHERE industry_id=ANY($1)", [industryids], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            reslen = result.rows.length;
                            for(i=0; i<reslen; i++){
                              //console.log(result.rows[i].industry_name);
                              subcatids.push(result.rows[i].industry_subcat);
                            }
                          }
                          //console.log(subcatids);
                          client.end();
                          callback();
                      });
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM topic_subcat WHERE subcat_id=ANY($1) AND  subcat_category=ANY($2)", [subcatids, category_ids], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            reslen = result.rows.length;
                            for(i=0; i<reslen; i++){
                              //console.log(result.rows[i].industry_name);
                              subtopics.push({     
                                              'subcat_id':result.rows[i].subcat_id, 
                                              'subcat_name':result.rows[i].subcat_name
                                              });
                            }
                          }
                          //console.log(subcatids);
                          client.end();
                          callback();
                      });
              },

              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM question_two WHERE event_id=$1", [event_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            question = result.rows[0].question_text;
                          }
                          //console.log(industryids);
                          client.end();
                          callback();
                      });
              },
              
              function(callback){
                res.status(200);
                res.json({
                        "status": 200,
                        "message": "",
                        "data" : {
                                  'question':question,
                                  'answers':subtopics
                                }
                        });
              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


        }
        
    });

  },

  getTopics2: function(req, res) {
    //var allProducts = data; // Spoof a DB call
    //res.json(allProducts);

    var industryids = new Array;
    var subcatids = new Array;
    var catids = new Array;

    var subsid;
    var subsubtopics = new Array;

    var question = '';

    var event_id = req.body.event_id;
    var subcat_ids = req.body.subcat_ids;

    if (event_id == '' || subcat_ids == '' || !(subcat_ids instanceof Array) ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID and/or Topic Sub Categories are missing",
        "data" : []
      });
      return;
    }

    

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){
          //console.log(id);
          //console.log(user);

          async.series([
              //Load user to get `userId` first
              
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("DELETE FROM answer_two WHERE event_id=$1 AND user_id=$2", [event_id, user.u_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }

                          client.end();
                          callback();
                          
                      });
              },
              
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();

                sizecat = subcat_ids.length;
                iter = 0;

                function insertData(item,callback) {
                  
                  client.query('INSERT INTO answer_two( event_id, user_id, topic_subcat) VALUES ($1,$2,$3)', [
                         event_id,
                         user.u_id,
                        item
                       ], 
                  function(err,result) {
                    // return any err to async.each iterator
                    //callback(err);
                    iter++;
                    
                    /*if(iter > (sizecat-1)){
                      
                    }*/
                    console.log(item);

                    //client.end();
                    callback();

                  })

                }

                
                async.each(subcat_ids,insertData,function(err) {
                  // Release the client to the pg module
                  //done();
                  if (err) {
                    return console.error('error running query', err);
                  }
                  client.end();
                  callback();

                  //console.log(sizecat);

                })


                
               
              },


              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM subevent_industry WHERE event_id=$1", [event_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            reslen = result.rows.length;
                            for(i=0; i<reslen; i++){
                              industryids.push(result.rows[i].industry_id);
                            }
                          }

                          //console.log(industryids);
                          client.end();
                          callback();
                      });
              },
              
             


              function(callback){
                //console.log('pass');
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM linkedin_industry WHERE industry_subcat=ANY($1) AND industry_id=ANY($2)", [subcat_ids, industryids], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){

                            reslen = result.rows.length;
                            
                            for(i=0; i<reslen; i++){
                              subsubtopics.push({     
                                              'subsubcat_id':result.rows[i].industry_id, 
                                              'subsubcat_name':result.rows[i].industry_name
                                              });
                              
                            }
                          }

                          client.end();
                          callback();
                          
                      });
              },

              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM question_three WHERE event_id=$1", [event_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            question = result.rows[0].question_text;
                          }
                          //console.log(industryids);
                          client.end();
                          callback();
                      });
              },

              function(callback){
                res.status(200);
                res.json({
                        "status": 200,
                        "message": "",
                        "data" : {
                                  'question':question,
                                  'answers':subsubtopics
                                }
                        });
              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


        }
        
    });

  },


  getTopics3: function(req, res) {
    //var allProducts = data; // Spoof a DB call
    //res.json(allProducts);

    var subsid;
    var subsubtopics = new Array;

    var event_id = req.body.event_id;
    var subsubcat_ids = req.body.subsubcat_ids;

    var subeventids = new Array;
    var filsubeventids = new Array;

    var companyids = new Array;
    var filcompanyids = new Array;

    var alreadysub = 0;

    if (event_id == '' || subsubcat_ids == '' || !(subsubcat_ids instanceof Array) ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Event ID and/or Topic Sub Categories are missing",
        "data" : []
      });
      return;
    }


    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){
          //console.log(id);
          //console.log(user);
          user_id = user.u_id;

          async.series([
              //Load user to get `userId` first
              /*function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM subscription WHERE event_id=$1 AND user_id=$2", [event_id, user_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            subsid = result.rows[0].subs_id;

                            client.end();
                            callback();
                          }else{

                            client.end();
                            res.status(401);
                            res.json({
                              "status": 401,
                              "message": "Please subscribe first.",
                              "data" : ""
                            });
                          }
                          
                      });
              },*/
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("DELETE FROM answer_three WHERE event_id=$1 AND user_id=$2", [event_id, user_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }

                          client.end();
                          callback();
                          
                      });
              },
              /*function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM answer_two WHERE subs_id=$1", [subsid], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }

                          reslen = result.rows.length;

                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            res.status(401);
                            res.json({
                              "status": 401,
                              "message": "An answer has already been submitted.",
                              "data" : ""
                            });
                            
                          }else{
                            callback();
                          }
                          
                          
                      });
              },*/
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();

                sizecat = subsubcat_ids.length;
                iter = 0;

                function insertData(item,callback) {
                  
                  client.query('INSERT INTO answer_three(event_id, user_id, topic_subsubcat) VALUES ($1,$2,$3)', [
                        event_id,
                        user.u_id,
                        item
                       ], 
                  function(err,result) {
                    // return any err to async.each iterator
                    //callback(err);
                    iter++;
                    
                    /*if(iter > (sizecat-1)){
                      
                    }*/
                    //console.log(iter);

                    //client.end();
                    callback();

                  })
                }

                
                async.each(subsubcat_ids,insertData,function(err) {
                  // Release the client to the pg module
                  //done();
                  if (err) {
                    return console.error('error running query', err);
                  }
                  client.end();
                  callback();
                  //console.log(sizecat);

                })


                
               
              },
              function(callback) {

                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query('SELECT * FROM subevent_industry WHERE industry_id=ANY($1) AND event_id=$2', [subsubcat_ids, event_id], function(err, result){
                                if(err){
                                    return ;
                                }
                                if (result.rows.length > 0){
                                  //console.log(result.rows.length);
                                  reslen = result.rows.length;
                                  
                                  //console.log(result.rows);
                                  for(i=0; i<reslen; i++){
                                    subeventids.push(result.rows[i].subevent_id);
                                  }
                                  
                                }
                                //console.log(result.rows.length);
                                //console.log("start");
                                //console.log(subeventids);

                                client.end();
                                callback();
                            });
                  
              },
              function(callback){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query('SELECT * FROM subevents WHERE subevent_id=ANY($1) OR ( subevent_event=$2 AND subevent_type=0)', [subeventids, event_id], function(err, result){
                              if(err){
                                  return ;
                              }
                              if (result.rows.length > 0){
                                //console.log(result.rows.length);
                                reslen = result.rows.length;
                                
                                startarr = new Array;
                                //console.log(result.rows);
                                for(i=0; i<reslen; i++){
                                  starttime = result.rows[i].subevent_start.toString();
                                  //console.log(startarr.indexOf(starttime));
                                  if(startarr.indexOf(starttime) == -1){
                                    filsubeventids.push(result.rows[i].subevent_id);
                                    startarr.push(starttime);
                                  }
                                }
                                
                              }
                              //console.log(result.rows.length);
                              //console.log("start");
                              //console.log(filsubeventids);
                              //console.log(startarr);

                              client.end();
                              callback();
                          });
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("DELETE FROM mysubevents WHERE event_id=$1 AND user_id=$2", [event_id, user_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }

                          client.end();
                          callback();
                          
                      });
              },
              function(callback){
                var client = new pg.Client(connectionString);
                client.connect();

                sizecat = subsubcat_ids.length;
                iter = 0;

                function insertData(item,callback) {
                  
                  client.query('INSERT INTO mysubevents(event_id, user_id, subevent_id) VALUES ($1,$2,$3)', [
                        event_id,
                        user_id,
                        item
                       ], 
                  function(err,result) {
                    // return any err to async.each iterator
                    //callback(err);
                    iter++;
                    
                    /*if(iter > (sizecat-1)){
                      
                    }*/
                    //console.log(iter);

                    //client.end();
                    callback();

                  })
                }

                
                async.each(filsubeventids,insertData,function(err) {
                  // Release the client to the pg module
                  //done();
                  if (err) {
                    return console.error('error running query', err);
                  }
                  client.end();
                  callback();
                  //console.log(sizecat);

                });
              },
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("DELETE FROM myexhibitors WHERE event_id=$1 AND user_id=$2", [event_id, user_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }

                          client.end();
                          callback();
                          
                      });
              },
              function(callback) {

                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query('SELECT * FROM event_company WHERE event_id=$1', [event_id], function(err, result){
                                if(err){
                                    return ;
                                }
                                if (result.rows.length > 0){
                                  //console.log(result.rows.length);
                                  reslen = result.rows.length;
                                  
                                  //console.log(result.rows);
                                  for(i=0; i<reslen; i++){
                                    companyids.push(result.rows[i].company_id);
                                  }
                                  
                                }
                                //console.log(result.rows.length);
                                //console.log("start");
                                //console.log(subeventids);

                                client.end();
                                callback();
                            });
                  
              },
              function(callback) {

                      var client = new pg.Client(connectionString);
                      client.connect();
                      client.query('SELECT * FROM company_industry WHERE industry_id=ANY($1) AND company_id=ANY($2)', [subsubcat_ids, companyids], function(err, result){
                                if(err){
                                  console.log(err);
                                    return ;
                                }
                                if (result.rows.length > 0){
                                  //console.log(result.rows.length);
                                  reslen = result.rows.length;
                                  
                                  //console.log(result.rows);
                                  for(i=0; i<reslen; i++){
                                    filcompanyids.push(result.rows[i].company_id);
                                  }
                                  
                                }
                                //console.log(result.rows.length);
                                //console.log("start");
                                //console.log(subeventids);

                                client.end();
                                callback();
                            });
                  
              },
              function(callback){
                //console.log(filcompanyids);
                var client = new pg.Client(connectionString);
                client.connect();

                sizecat = subsubcat_ids.length;
                iter = 0;

                function insertData(item,callback) {
                  
                  client.query('INSERT INTO myexhibitors(event_id, user_id, company_id) VALUES ($1,$2,$3)', [
                        event_id,
                        user_id,
                        item
                       ], 
                  function(err,result) {
                    // return any err to async.each iterator
                    //callback(err);
                    iter++;
                    
                    /*if(iter > (sizecat-1)){
                      
                    }*/
                    //console.log(iter);

                    //client.end();
                    callback();

                  })
                }

                
                async.each(filcompanyids,insertData,function(err) {
                  // Release the client to the pg module
                  //done();
                  if (err) {
                    return console.error('error running query', err);
                  }
                  client.end();
                  callback();
                  //console.log(sizecat);

                });
              },

              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("select * from subscription where user_id=$1 and event_id=$2", [user_id, event_id], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return ;
                    }
                    if(result.rows.length > 0){
                      alreadysub = 1;
                    }

                    client.end();
                    callback();
                });
              },
              //Load user to get `userId` first
              function(callback) {
                //console.log(topicbranches);

                if(!alreadysub){
                  var client = new pg.Client(connectionString);
                  client.connect();
                  client.query("INSERT INTO subscription(user_id, event_id) VALUES($1, $2) RETURNING subs_id", [user_id, event_id], function(err, result){
                      //console.log(result.rows[0]);
                      if(err){
                          return ;
                      }
                      //console.log(result);
                      client.end();
                      callback();
                  });
                }else{
                  callback();
                }

              },

              function(callback){
                res.status(200);
                res.json({
                        "status": 200,
                        "message": "Successfully submitted.",
                        "data" : subsubtopics
                        });
              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


        }
        
    });


    


  },


  getOne: function(req, res) {
    var id = req.params.id;
    var product = data[0]; // Spoof a DB call
    res.json(product);
  },

  create: function(req, res) {
    //var newProduct = req.body;
    //console.log(req.params);

    var subscription_id = req.body.subscription_id || '';
    var topics = req.body.topics || '';

    if (subscription_id == '' || topics == '' || !(topics instanceof Array) ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Subscription ID and/or Topics are missing",
        "data" : []
      });
      return;
    }

    toplength = topics.length;
    if (toplength > 3 ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "You can submit 3 topics maximum.",
        "data" : []
      });
      return;
    }



    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    
    
    User.findOne( username , function(err, user) {
        if(user){
          
          done = 0;
          funcs = new Array;

            //console.log(funcs.toString());

          async.series(
              [
                function(callback) {
                  var client = new pg.Client(connectionString);
                  client.connect();
                  client.query("SELECT * FROM subscription_topic WHERE subscription_id=$1", [subscription_id], function(err, result){
                      //console.log(result.rows[0]);
                      if(err){
                          return ;
                      }
                      if(result.rows.length > 0){

                        client.end();

                        res.status(200);
                        res.json({
                          "status": 401,
                          "message": "This subcription already has topics.",
                          "data" : ""
                        });
                        //return;
                      }else{
                        client.end();
                        callback();
                      }
                      
                  });
                },
                function(callback) {
                  topic = topics[0] ;
                  if(topic != '' && (typeof topic != 'undefined')){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("INSERT INTO subscription_topic (subscription_id, topic_id) VALUES ($1, $2)", [subscription_id, topic], function(err, result){
                        //console.log(result.rows[0]);
                        if(err){
                            return ;
                        }
                        client.end();
                        callback();
                    });
                  }else{
                    
                    callback();
                  }
                },
                function(callback) {
                  topic = topics[1];
                  
                  if(topic != '' && (typeof topic != 'undefined')){
                    console.log(topic);
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("INSERT INTO subscription_topic (subscription_id, topic_id) VALUES ($1, $2)", [subscription_id, topic], function(err, result){
                        //console.log(result.rows[0]);
                        if(err){
                            return ;
                        }
                        client.end();
                        callback();
                    });
                  }else{
                    
                    callback();
                  }
                  
                },
                function(callback) {
                  topic = topics[2] ;
                  if(topic != '' && (typeof topic != 'undefined')){
                    var client = new pg.Client(connectionString);
                    client.connect();
                    client.query("INSERT INTO subscription_topic (subscription_id, topic_id) VALUES ($1, $2)", [subscription_id, topic], function(err, result){
                        //console.log(result.rows[0]);
                        if(err){
                            return ;
                        }
                        client.end();
                        callback();
                    });
                  }else{
                    
                    callback();
                  }
                }
              ,
              function(callback){
                msg = "Topics submitted Successfully.";
                res.status(200);
                res.json({
                  "status": 200,
                  "message": msg,
                  "data" : ""
                });
              }
            ]
          , function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


          //console.log(user);
          

        }
        
    });
  },



  hasAnswered: function(req, res) {
    //var allProducts = data; // Spoof a DB call
    //res.json(allProducts);

    var subsid;
    var subsubtopics = new Array;
    var subscribed = 0;
    var answered = 0;

    var event_id = req.params.id;
    

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);

    User.findOne( username , function(err, user) {
        if(user){
          //console.log(id);
          //console.log(user);

          async.series([
              //Load user to get `userId` first
              /*function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM subscription WHERE event_id=$1 AND user_id=$2", [event_id, user.u_id], function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            subsid = result.rows[0].subs_id;
                            subscribed = 1;
                          }
                          client.end();
                          callback();
                      });
              },*/
              function(callback) {
                
                  var client = new pg.Client(connectionString);
                  client.connect();
                  client.query("SELECT * FROM answer_three WHERE event_id=$1 AND user_id=$2", [event_id, user.u_id], function(err, result){
                            //console.log(err);
                            if(err){
                                return ;
                            }
                            reslen = result.rows.length;
                            console.log(reslen);
                            //if no rows were returned from query, then new user
                            if (result.rows.length > 0){
                              answered = 1;
                            }
                            client.end();
                            callback();

                        });
               
              },
              function(callback) {
                if(subscribed == 1 && answered == 0 ){

                  var client = new pg.Client(connectionString);
                  client.connect();

                  client.query("DELETE * FROM answer_two WHERE subs_id=$1", [subsid], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      client.end();
                  });

                  client.query("DELETE * FROM answer_one WHERE subs_id=$1", [subsid], function(err, result){
                      //console.log(err);
                      if(err){
                          return ;
                      }
                      client.end();
                  });
                }


                callback();
              },
              function(callback){
                console.log("answered: "+answered)
                res.status(200);
                res.json({
                        "status": 200,
                        "message": "",
                        "data" : [{"answered":answered}]
                        });
              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });


        }
        
    });

  },


  getAll: function(req, res) {
    //var allProducts = data; // Spoof a DB call
    //res.json(allProducts);



    var topics = new Array;

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);
    //console.log(username);


          //console.log(id);
          //console.log(user);

          async.series([
             
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM linkedin_industry", null, function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }
                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){
                            reslen = result.rows.length;
                            for(i=0; i<reslen; i++){
                              //console.log(result.rows[i].industry_name);
                              topics.push(result.rows[i].industry_name);
                            }
                          }
                          //console.log(subcatids);
                          client.end();
                          callback();
                      });
              },
              
              
              

              function(callback){
                res.status(200);
                res.json({
                        "status": 200,
                        "message": "",
                        "data" : {
                                  
                                  'topics':topics
                                }
                        });
              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });




  },

  


  update: function(req, res) {
    var updateProduct = req.body;
    var id = req.params.id;
    data[id] = updateProduct // Spoof a DB call
    res.json(updateProduct);
  },

  delete: function(req, res) {
    var subscription_id = req.params.id;

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);


    alreadysub = 0;

     User.findOne( username , function(err, user) {
        if(user){
          async.series([
              function(callback) {
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("select * from subscription_topic where subscription_id=$1", [subscription_id], function(err, result){
                    //console.log(result.rows[0]);
                    if(err){
                        return ;
                    }
                    if(result.rows.length > 0){
                      alreadysub = 1;
                    }
                    client.end();
                    callback();
                });
              },
              //Load user to get `userId` first
              function(callback) {
                //console.log(topicbranches);

                if(alreadysub){
                  var client = new pg.Client(connectionString);
                  client.connect();
                  client.query("DELETE FROM subscription_topic WHERE subscription_id=$1", [subscription_id], function(err, result){
                      //console.log(result.rows[0]);
                      if(err){
                          return ;
                      }
                      //console.log(result);
                      //subsid = result.rows[0].subs_id;
                      client.end();
                      msg = "Successfully removed topics for this subscription.";
                      res.status(200);
                      res.json({
                        "status": 200,
                        "message": msg,
                        "data" : ""
                      });
                  });
                }else{
                  msg = "No topics found for this subscription.";
                  res.status(401);
                  res.json({
                    "status": 401,
                    "message": msg,
                    "data" : ""
                  });
                }

                

              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });
        }
        
    });

  },


  indexTopics: function(){
    var subsubtopics = new Array;
    async.series([
              function(callback){
                var client = new pg.Client(connectionString);
                client.connect();
                client.query("SELECT * FROM topic_subsubcat", null, function(err, result){
                          //console.log(err);
                          if(err){
                              return ;
                          }

                         

                          //if no rows were returned from query, then new user
                          if (result.rows.length > 0){

                            reslen = result.rows.length;
                        
                            for(i=0; i<reslen; i++){
                              subsubtopics.push({     
                                              'subsubcat_id':result.rows[i].subsubcat_id, 
                                              'subsubcat_name':result.rows[i].subsubcat_name,
                                              'subsubcat_pkeywords':result.rows[i].subsubcat_pkeywords
                                              });
                            }
                            
                          }

                          client.end();
                          callback();
                          
                      });
              },
              function(callback){
                  var elastic = require('../models/elasticsearch.js');  
                  elastic.indexExists().then(function (exists) { 
                    if (exists) {
                      return elastic.deleteIndex();
                    }
                  }).then(function () {
                    return elastic.initIndex().then(elastic.initMapping).then(function () {
                      //Add a few titles for the autocomplete
                      //elasticsearch offers a bulk functionality as well, but this is for a different time
                      var promises = subsubtopics.map(function (bookTitle) {
                        return elastic.addDocument({
                          title: bookTitle.subsubcat_name,
                          content: bookTitle.subsubcat_pkeywords,
                          metadata: {
                            titleLength: bookTitle.subsubcat_name.length
                          }
                        });
                      });
                      //console.log('done');
                      return Promise.all(promises);
                    });
                  });
              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              
          });


    


  }


};

var data = [{
  name: 'product 1',
  id: '1'
}, {
  name: 'product 2',
  id: '2'
}, {
  name: 'product 3',
  id: '3'
}];

module.exports = substopics;
