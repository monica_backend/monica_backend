


var pg = require('pg');
var connectionString = require('../config/database');

var async = require('async');

var User = require('../models/user');




var company = {

  getSponsors: function(req, res) {
    var id = req.params.id;

    var sponsorids = new Array;
    var sponsors = new Array;


    async.series([
        //Load user to get `userId` first
        function(callback) {
          //console.log(topicbranches);

          var client = new pg.Client(connectionString);
          client.connect();
          client.query("SELECT * FROM event_sponsor WHERE event_id=$1", [id], function(err, result){
                    //console.log(err);
                    if(err){
                        return ;
                    }
                    //if no rows were returned from query, then new user
                    if (result.rows.length > 0){
                        //console.log(result.rows.length);
                        reslen = result.rows.length;
                        
                        //console.log(result.rows);
                        for(i=0; i<reslen; i++){
                          sponsorids.push(result.rows[i].company_id);
                          //console.log(result.rows[i].topic_branch);
                        }
                        
                        
                  }

                  client.end();
                  callback();
                });
        },
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM company WHERE company_id = ANY($1)", [sponsorids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    sponsors.push({   'company_id':result.rows[i].company_id, 
                                      'name':result.rows[i].company_name, 
                                      'photo':result.rows[i].company_image
                                      });
                  }
                  
              }

            client.end();
            callback();
            
          });
        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(allEvents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : sponsors
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },


  getOne: function(req, res) {
    var id = req.params.id;

   var company;
   var userids = new Array;
   var team = new Array;


    async.series([
        //Load user to get `userId` first
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM company_user WHERE company_id = $1", [id], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    userids.push(result.rows[i].user_id);
                    //console.log(result.rows[i].profile_fname);
                  }
                  
              }

            client.end();
            callback();
            
          });
        },
        function(callback){
          //console.log(userids);
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT user_linkedin.*, profile.* FROM user_linkedin INNER JOIN profile ON user_linkedin.user_id = profile.profile_user WHERE profile.profile_user = ANY($1)", [userids], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //console.log(result.rows[i].profile_fname);
                    team.push({
                                'userid': result.rows[i].user_id,
                                'name': result.rows[i].profile_fname + ' ' + result.rows[i].profile_lname,
                                'image': result.rows[i].profile_photo,
                                'title': result.rows[i].linkedin_headline
                              });
                    
                  }
                  
              }

            client.end();
            callback();
            
          });
        },
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM company WHERE company_id = $1", [id], function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname); 
                    //


                    company = [{      'company_id':result.rows[i].company_id, 
                                      'name':result.rows[i].company_name,
                                      'description':result.rows[i].company_description,
                                      'location':result.rows[i].company_location,
                                      'photo':result.rows[i].company_image,
                                      'email':result.rows[i].company_email,
                                      'phone':result.rows[i].company_phone,
                                      'website':result.rows[i].company_website,
                                      'team': team
                                      }];
                  }
                  
              }
              
            client.end();
            callback();
            
          });
        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(allEvents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : company
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },

  getAll: function(req, res) {
    var id = req.params.id;

    var sponsorids = new Array;
    var sponsors = new Array;

    async.series([
        //Load user to get `userId` first
        
        function(callback){
          var client = new pg.Client(connectionString);
  
          //console.log("email is: "+key);
          client.connect();
          client.query("SELECT * FROM company",  null, function(err, result){
              //console.log(err);
              if(err){
                  return ;
              }

              //if no rows were returned from query, then new user
              if (result.rows.length > 0){
                  //console.log(result.rows.length);
                  reslen = result.rows.length;
                  
                  //console.log(result.rows);
                  for(i=0; i<reslen; i++){
                    //eventids.push(result.rows[i].event_id);
                    //console.log(result.rows[i].profile_fname);
                    sponsors.push({   'company_id':result.rows[i].company_id, 
                                      'name':result.rows[i].company_name, 
                                      'photo':result.rows[i].company_image
                                      });
                  }
                  
              }

            client.end();
            callback();
            
          });
        },
        function(callback) {
          //console.log(allEvents);
          
          //res.json(allEvents);
          res.status(200);
          res.json({
                  "status": 200,
                  "message": "",
                  "data" : sponsors
                  });
          


        }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err);
        //Here locals will be populated with `user` and `posts`
        //Just like in the previous example
        //res.render('user-profile', locals);
    });

    
  },


  create: function(req, res) {
    var company_image = req.body.image || '';
    var company_name = req.body.name || '';
    var company_description = req.body.description || '';
    var company_location = req.body.location || '';


    if (company_image == ''  || company_name == ''  || company_description == ''  || company_location == '' ) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "All fields are required.",
        "data" : []
      });
      return;
    }

    var token = req.headers['x-access-token'];
    var user = new User;
    username = user.getFromToken(token);

    /*subevent_start = new Date().getTime();
    subevent_end = new Date().getTime();*/

    //var now = new Date();
    //var mydate = new Date(now.getFullYear(), now.getMonth() , now.getDate(), now.getHours(), now.getMinutes());

    
    //subevent_start = mydate;
    //subevent_end = mydate;

    User.findOne( username , function(err, user) {
        if(user){

          alreadysub = 0;
          user_id = user.u_id;
          subsid = "";
          msg = '';

          async.series([
              
              //Load user to get `userId` first
              function(callback) {
                //console.log(topicbranches);
                if(!alreadysub){
                  var client = new pg.Client(connectionString);
                  client.connect();
                  client.query("INSERT INTO company(company_name, company_description, company_image, company_location) VALUES($1, $2, $3, $4) RETURNING company_id", [company_name, company_description, company_image, company_location], function(err, result){
                      //console.log(result.rows[0]);
                      if(err){
                        //console.log(err);
                          return ;
                      }

                      client.end();
                      //console.log(result);
                      //subsid = result.rows[0].subs_id;
                      msg = "Company created successfully.";
                      res.status(200);
                      res.json({
                        "status": 200,
                        "message": msg,
                        "data" : ""
                      });
                  });
                }else{
                  msg = "";
                  res.status(401);
                  res.json({
                    "status": 401,
                    "message": msg,
                    "data" : ""
                  });
                }

                

              }
          ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
              if (err) return next(err);
              //Here locals will be populated with `user` and `posts`
              //Just like in the previous example
              //res.render('user-profile', locals);
          });




           //console.log(user);
            

        }
        
    });
  },



  update: function(req, res) {
    var updateProduct = req.body;
    var id = req.params.id;
    data[id] = updateProduct // Spoof a DB call
    res.json(updateProduct);
  },

  delete: function(req, res) {
    var id = req.params.id;
    data.splice(id, 1) // Spoof a DB call
    res.json(true);
  }
};

var data = [{
  name: 'event 1',
  id: '1'
}, {
  name: 'event 2',
  id: '2'
}, {
  name: 'event 3',
  id: '3'
}];







module.exports = company;
