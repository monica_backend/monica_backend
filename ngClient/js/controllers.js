


myApp.controller("HeaderCtrl", ['$scope', '$location', 'UserAuthFactory',
  function($scope, $location, UserAuthFactory) {

    $scope.isActive = function(route) {
      return route === $location.path();
    }

    $scope.logout = function () {
      UserAuthFactory.logout();
    }
  }
]);

myApp.controller("HomeCtrl", ['$scope',
  function($scope) {
    $scope.name = "Home Controller";
  }
]);


//var app = angular.module('myApp', ['ngFileUpload']);
myApp.controller("EventsCtrl", ['$scope', '$location', '$timeout', '$http', 'Upload', 'eventFactory',
  function($scope, $location, $timeout, $http, Upload, eventFactory) {
    $scope.events = [];

    // Access the factory and get the latest products list
    eventFactory.getEvents().then(function(data) {
      $scope.events = data.data.data;
    });

    $scope.create = function() { 

      if ($scope.imagelink !== undefined && $scope.event.name !== undefined && $scope.event.description !== undefined && $scope.event.location !== undefined 
        && $scope.event.start !== undefined && $scope.event.end !== undefined ) {

        var image = $scope.imagelink;
        var name = $scope.event.name;
        var description = $scope.event.description;
        var location = $scope.event.location;
        var start = $scope.event.start;
        var end = $scope.event.end;
        console.log(image, name, description, location, start, end);

        eventFactory.postEvent(image, name, description, location, start, end).then(function(data) {
          //console.log(data.data.status);
          if(data.data.status == 200){
            $location.path("/events");
          }
          
        });


      }
    };



    $scope.uploadFiles = function(file, errFiles) {
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: 'http://localhost:3000/api/user/uploads',
                data: {file: file}
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    console.log(response.data);
                     $scope.imagelink = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * 
                                         evt.loaded / evt.total));
            });
        }
    };



    $scope.uploadPic = function(file) {
      //inject angular file upload directives and services.
      
      //console.log(123);

      /*var fd = new FormData();
      fd.append('file', file);
      $http.post('http://localhost:3000/api/user/uploads', fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .success(function(){
        console.log('OK');
      })
      .error(function(){
        console.log('KO');
      });*/


      /*file.upload = Upload.upload({
        url: 'http://localhost:3000/api/user/uploads',//'http://localhost:3000/api/user/uploads', //https://angular-file-upload-cors-srv.appspot.com/upload',
        headers : {
          'Content-Type': file.type
        },
        method: 'POST',
        data: {
            username: $scope.username, userPhoto: file,
            //"Content-Type": file.type != '' ? file.type : 'application/octet-stream', // content type of the file (NotEmpty)
          },
        //headers : {
          //    'Content-Type': file.type
        //},
        //headers : {
          'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
        //'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
        //'Accept': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
        //'Access-Control-Allow-Origin': '*'
        //}
      });

      file.upload.then(function (response) {
        $timeout(function () {
          file.result = response.data;
        });
      }, function (response) {
        if (response.status > 0)
          $scope.errorMsg = response.status + ': ' + response.data;
      }, function (evt) {
        // Math.min is to fix IE which reports 200% sometimes
        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
      });*/

    }


  }
]);






myApp.controller("CompaniesCtrl", ['$scope', '$location', '$timeout', '$http', 'Upload', 'companiesFactory',
  function($scope, $location, $timeout, $http, Upload, companiesFactory) {
    $scope.companies = [];

    // Access the factory and get the latest products list
    companiesFactory.getCompanies().then(function(data) {
      $scope.companies = data.data.data;
      //console.log(data.data.data);
    });

    $scope.create = function() { 

      if ($scope.imagelink !== undefined && $scope.company.name !== undefined && $scope.company.description !== undefined && $scope.company.location !== undefined ) {

        var image = $scope.imagelink;
        var name = $scope.company.name;
        var description = $scope.company.description;
        var location = $scope.company.location;
        console.log(image, name, description);

        companiesFactory.postCompany(image, name, description, location).then(function(data) {
          //console.log(data.data.status);
          if(data.data.status == 200){
            $location.path("/companies");
          }
          
        });


      }
    };








$scope.availableColors = [];
  $http.get('http://localhost:3000/api/v1/public/substopics').then(
    function (response) {
      $scope.availableColors = response.data.data.topics;
      //$scope.multipleDemo.colors = ['Blue','Red'];
      console.log(response.data.data.topics)
    },
    function () {
      console.log('ERROR!!!');
    }
  );
  
  /*$scope.funcAsync = function (query) {
    var reqStr;
    reqStr = query == 'red' ? 'data1.json' : 'data.json';
    console.log(query);
    $http.get(reqStr).then(
    function (response) {
      $scope.availableColors = response.data;
      console.log(response)
    },
    function () {
      console.log('ERROR!!!');
    }
  );
  }*/

  //-------------------------------
  //-------------------------------
  
  //$scope.availableColors = ['Blue','Red','Green','Yellow']
  $scope.multipleDemo = {};
  //$scope.multipleDemo.colors = ['Blue','Red'];


  }
]);



/*myApp.controller('MyCtrl',['Upload','$window',function(Upload,$window){
    var vm = this;
    vm.submit = function(){ //function to call on form submit
        if (vm.upload_form.file.$valid && vm.file) { //check if from is valid
            vm.upload(vm.file); //call upload function
        }
    }
    
    vm.upload = function (file) {
        Upload.upload({
            url: 'http://localhost:3000/api/user/uploads', //webAPI exposed to upload the file
            data:{file:file} //pass file as data, should be user ng-model
        }).then(function (resp) { //upload function returns a promise
            if(resp.data.error_code === 0){ //validate success
                $window.alert('Success ' + resp.config.data.file.name + 'uploaded. Response: ');
            } else {
                $window.alert('an error occured');
            }
        }, function (resp) { //catch error
            console.log('Error status: ' + resp.status);
            $window.alert('Error status: ' + resp.status);
        }, function (evt) { 
            console.log(evt);
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            vm.progress = 'progress: ' + progressPercentage + '% '; // capture upload progress
        });
    };
}]);*/



/*
myApp.controller('MyCtrl', ['$scope', 'Upload', '$timeout', function ($scope, Upload, $timeout) {
    $scope.uploadFiles = function(file, errFiles) {
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: 'http://localhost:3000/api/user/uploads',
                data: {file: file}
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    console.log(response.data);
                    $scope.myForm.email2
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * 
                                         evt.loaded / evt.total));
            });
        }
    }
}]);
*/





myApp.controller("SubeventsCtrl", ['$scope', '$routeParams', '$location', 'subeventFactory',
  function($scope, $routeParams, $location, subeventFactory) {
    $scope.subevents = [];


    var eventid = $routeParams.id;
    $scope.eventid = eventid;
    
    // Access the factory and get the latest products list
    subeventFactory.getSubevents(eventid).then(function(data) {
      $scope.subevents = data.data.data;
    });

    $scope.create = function() {  

      if ($scope.subevent.name !== undefined && $scope.subevent.description !== undefined 
        && $scope.subevent.start !== undefined && $scope.subevent.end !== undefined ) {

        var event = eventid; //$scope.subevent.event;
        var name = $scope.subevent.name;
        var description = $scope.subevent.description;
        var start = $scope.subevent.start;
        var end = $scope.subevent.end;
        //console.log(event, name, description, start, end);

        subeventFactory.postSubevent(event, name, description, start, end).then(function(data) {
          //console.log(data.data.status);
          if(data.data.status == 200){
            $location.path("/subevents/"+eventid);
          }
          
        });


      }
    };
    
  }
]);

/*myApp.controller("Page1Ctrl", ['$scope',
  function($scope) {
    $scope.name = "Page1 Controller";
  }
]);

myApp.controller("Page2Ctrl", ['$scope',
  function($scope) {
    $scope.name = "Page2 Controller";
    // below data will be used by checkmark filter to show a ✓ or ✘ next to it
    $scope.list = ['yes', 'no', true, false, 1, 0];
  }
]);

myApp.controller("Page3Ctrl", ['$scope', 'dataFactory',
  function($scope, dataFactory) {
    $scope.products = [];

    // Access the factory and get the latest products list
    dataFactory.getProducts().then(function(data) {
      $scope.products = data.data;
    });

  }
]);*/
