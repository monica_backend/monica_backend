
myApp.factory('dataFactory', function($http) {
  /** https://docs.angularjs.org/guide/providers **/
  var urlBase = 'http://localhost:3000/api/v1/products';
  var _prodFactory = {};

  _prodFactory.getProducts = function() {
    return $http.get(urlBase);
  }; 

  return _prodFactory;
});



myApp.factory('eventFactory', function($http) {
  /** https://docs.angularjs.org/guide/providers **/
  var urlBase = 'http://localhost:3000/api/v1/public/events';
  var _eventFactory = {};

  _eventFactory.getEvents = function() {
    return $http.get(urlBase);
  };

  _eventFactory.postEvent = function(image, name, description, location, start, end){
    //console.log(event, name, description, start, end);
    //return $http.get(urlBase);
    return $http.post('http://localhost:3000/api/v1/event', {
        image: image,
        name: name,
        description: description,
        location: location,
        start: start,
        end: end
      });
  };

  return _eventFactory;
});


myApp.factory('companiesFactory', function($http) {
  /** https://docs.angularjs.org/guide/providers **/
  var urlBase = 'http://localhost:3000/api/v1/public/company';
  var _companiesFactory = {};

  _companiesFactory.getCompanies = function() {
    return $http.get(urlBase);
  };

  _companiesFactory.postCompany = function(image, name, description, location){
    //console.log(event, name, description, start, end);
    //return $http.get(urlBase);
    return $http.post('http://localhost:3000/api/v1/company', {
        image: image,
        name: name,
        description: description,
        location: location
      });
  };


  return _companiesFactory;
});


myApp.factory('subeventFactory', function($http) {
  /** https://docs.angularjs.org/guide/providers **/
  var urlBase = 'http://localhost:3000/api/v1/public/subevents';
  var _subeventFactory = {};

  _subeventFactory.getSubevents = function(eventid) {
    return $http.get(urlBase+'/'+eventid);
  }; 

  _subeventFactory.postSubevent = function(event, name, description, start, end){
    //console.log(event, name, description, start, end);
    //return $http.get(urlBase);
    return $http.post('http://localhost:3000/api/v1/subevent', {
        event: event,
        name: name,
        description: description,
        start: start,
        end: end
      });
  };

  return _subeventFactory;
});



